/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "terrificmodellingwindow.h"

namespace TerrificCoder{
	TerrificModellingWindow::TerrificModellingWindow(QWidget *) : KXmlGuiWindow()
{
	ui_terrificmodelling_window.setupUi(this);
	ui_terrificmodelling_window.retranslateUi(this);
	setAutoFillBackground(true);
//  	tabifyDockWidget(ui_terrificmodelling_window.dockScene, ui_terrificmodelling_window.dockActionList);
// 	tabifyDockWidget(ui_terrificmodelling_window.dockScene, ui_terrificmodelling_window.dockSelection);
// 	tabifyDockWidget(ui_terrificmodelling_window.objectEditorDock, ui_terrificmodelling_window.dockWidget_3);
// 	tabifyDockWidget(ui_terrificmodelling_window.objectEditorDock, ui_terrificmodelling_window.cameraEditorDock);
// 	tabifyDockWidget(ui_terrificmodelling_window.objectEditorDock, ui_terrificmodelling_window.materialEditorDock);
// 	tabifyDockWidget(ui_terrificmodelling_window.objectEditorDock, ui_terrificmodelling_window.lightEditorDock);
//	ui_terrificmodelling_window.models->set ui_terrificmodelling_window.toolBar_4);
	
}

TerrificModellingWindow::~TerrificModellingWindow()
{
	
}

};