add_subdirectory(terrificcoder)
add_subdirectory(test)

set(
	terrificmodelling_SRCS
	
	terrificmodellingwindow.cpp
	main.cpp
	
)

include (KDE4Defaults)
qt4_add_resources(terrificmodelling_RCS ../iconsToolbar.qrc)
kde4_add_ui_files(terrificmodelling_SRCS terrificmodelling_window.ui)
kde4_add_kcfg_files(terrificmodelling_SRCS)
kde4_add_executable(terrificmodelling ${terrificmodelling_SRCS} ${terrificmodelling_RCS})



target_link_libraries(terrificmodelling ${KDE4_KDEUI_LIBS} terrificcoder)





########### install files ###############
install(TARGETS terrificmodelling ${INSTALL_TARGETS_DEFAULT_ARGS} )

install(FILES terrificmodelling.desktop  DESTINATION ${XDG_APPS_INSTALL_DIR} )
install(FILES terrificmodelling.kcfg  DESTINATION  ${KCFG_INSTALL_DIR} )
install(FILES terrificmodellingui.rc  DESTINATION  ${DATA_INSTALL_DIR}/terrificmodelling )

