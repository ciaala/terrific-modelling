/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "isegment.h"

#include "math.h"
#include "../library/logging.h"

namespace TerrificCoder {

int ISegment::counter = 0;

ISegment::ISegment(Vector4 p1, Vector4 p2)
{
    this->id = counter++;
    this->p1 = Vector(p1.v[0],p1.v[1],p1.v[2]);
    this->p2 = Vector(p2.v[0],p2.v[1],p2.v[2]);
    this->u = this->p2 - this->p1;
    this->u.normalize();
}
ISegment::ISegment(Vector p1, Vector p2)
{
    this->id = counter++;
    this->p1 = Vector(p1.v[0],p1.v[1],p1.v[2]);
    this->p2 = Vector(p2.v[0],p2.v[1],p2.v[2]);
    this->u = this->p2 -this->p1;
    this->u.normalize();
}



/**
 *
 * c centre of the sphere
 * r radius of the sphere
 */
// std::list<Vector>* ISegment::intersectSphere(Vector c, GLdouble radius) {

/*
    GLdouble a = u.x * u.x + u.y * u.y + u.z * u.z;

    GLdouble b = 2*( u.x*(p1.x-c.x) +u.y*(p1.y-c.y) + u.z*(p1.z - c.z ) );
    GLdouble c1 = c.x*c.x + c.y*c.y + c.z*c.z + p1.x*p1.x + p1.y*p1.y + p1.z*p1.z;
    GLdouble c2 = 2*(c.x*p1.x + c.y*p1.y + c.z*p1.z);
    GLdouble c3 = c1 - c2 - radius*radius;

    GLdouble disc = b*b - 4*a *c3;
    if ( disc < 0 ) {
        return NULL;
    } else {
        std::list<Vector> * points = new std::list<Vector>();
        if ( disc > 0 )  {
            GLdouble sqrt_disc = sqrt( disc );
            GLdouble t1 = (-b + sqrt_disc) / ( 2 * a );
            GLdouble t2 = (-b - sqrt_disc) / ( 2 * a );
            points->push_back(pointAt(t1));

            points->push_back(pointAt(t2));
        } else if ( disc == 0 ) {
            GLdouble t = -b / (2 *a );
            points->push_back(pointAt(t));
        }
        return points;
    }*/
// }
Vector ISegment::point1()
{
    return p1;
}
Vector ISegment::point2()
{
    return p2;
}
Vector ISegment::versor()
{
	return u;
}

Vector ISegment::pointAt(GLdouble t)
{
    return Vector( u.x*t + p1.x,
                   u.y*t + p1.y,
                   u.z*t + p1.z );
}

GLdouble ISegment::intersectPlane( IPlane *plane ) {
	
    GLdouble num = -( plane->a()*p1.x + plane->b()*p1.y + plane->c()*p1.z + plane->d() );
    GLdouble denom = ( plane->a()*u.x + plane->b()*u.y + plane->c()*u.z );
    return num/denom;
}

bool ISegment::testPlane( IPlane *plane ) {



    //std::pair<Vector,GLdouble > r = pointOnPlane(u,p1,plane->plane_eq);
    Vector i = pointAt( intersectPlane(plane) );

    Vector p1_x = p1 - i;
    GLdouble t1 = p1_x.product_scalar(u);
    Vector p2_x = p2 - i;
    GLdouble t2 = p2_x.product_scalar(u);


    if ( (t2 > 0 && t1 < 0)  || (t2 < 0 && t1 > 0 ) ) {
        //test the intersection position in the surface limits
        if (plane->inPlane(i)) {
            TCDEBUG << "i: " << i << " u:" << u << " t1 :" << t1 << " t2 :" << t2;
            return true;
        }
    }
    return false;
}
bool ISegment::onSegment(Vector point)
{
	GLdouble test = ( point.x*u.x -p1.x + point.x*u.x - p1.x + point.z*u.z - p1.z);
	if (test == 0) {
		Vector p1_x = p1 - point;
		GLdouble t1 = p1_x.product_scalar(u);
		Vector p2_x = p2 - point;
		GLdouble t2 = p2_x.product_scalar(u);
		if ( (t1>0 && t2<0) || (t1<0 && t2>0) ) {
			return true;
		}
	}
	return false;
}

};
