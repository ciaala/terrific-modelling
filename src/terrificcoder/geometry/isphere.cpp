/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "isphere.h"
#include "../model/tcsimpleobjects.h"

namespace TerrificCoder{

ISphere::ISphere(TCSphere* sphere,Vector internal) : IObject(sphere)
{
	center = sphere->getPosition();
	radius = sphere->getWidth();
	quadRadius = radius*radius;
	Vector &p = center;
	Vector direction = internal-center;
	direction.normalize();
	direction.scale(radius);
	
	Vector *v[8];
// 	= new Vector[8];
	
	v[0] = new Vector(p.x-radius,p.y,p.z);
	v[1] = new Vector(p.x+radius,p.y,p.z);
	v[2] = new Vector(p.x,p.y-radius,p.z);
	v[3] = new Vector(p.x,p.y+radius,p.z);
	v[4] = new Vector(p.x,p.y,p.z-radius);
	v[5] = new Vector(p.x,p.y,p.z+radius);
	v[6] = new Vector(p+direction);
	v[7] = new Vector(p-direction);
	for( int i=0;i<8; i++){
		points.push_back(v[i]);
	}

}
ISphere::~ISphere()
{

}
std::list< Vector >* ISphere::intersect(ISegment* segment)
{
	Vector u = segment->versor();
	Vector p1 = segment->point1();
	
    GLdouble a = u.x * u.x + u.y * u.y + u.z * u.z;

    GLdouble b = 2*( u.x*(p1.x-center.x) +u.y*(p1.y-center.y) + u.z*(p1.z - center.z ) );
    GLdouble c1 = center.x*center.x + center.y*center.y + center.z*center.z + p1.x*p1.x + p1.y*p1.y + p1.z*p1.z;
    GLdouble c2 = 2*(center.x*p1.x + center.y*p1.y + center.z*p1.z);
    GLdouble c3 = c1 - c2 - radius*radius;

    GLdouble disc = b*b - 4*a *c3;
    if ( disc < 0 ) {
        return NULL;
    } else {
        std::list<Vector> * points = new std::list<Vector>();
        if ( disc > 0 )  {
            GLdouble sqrt_disc = sqrt( disc );
            GLdouble t1 = (-b + sqrt_disc) / ( 2 * a );
            GLdouble t2 = (-b - sqrt_disc) / ( 2 * a );
            points->push_back(segment->pointAt(t1));

            points->push_back(segment->pointAt(t2));
        } else if ( disc == 0 ) {
            GLdouble t = -b / (2 *a );
            points->push_back(segment->pointAt(t));
        }
        return points;
    }
}

// std::list< IPlane* >* ISphere::getPlanes()
// {
// 	return &planes;
// }
// std::list< Vector* >* ISphere::getPoints()
// {
// 	return &points;
// }
// std::list< ISegment* >* ISphere::getSegments()
// {
// 	return &segments;
// }
bool ISphere::contain(Vector point)
{
	Vector dir = point - center;
	if ( dir.quadLength() < quadRadius ) {
		return true;
	} else {
		return false;
	}
}

};