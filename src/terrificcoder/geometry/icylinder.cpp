/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "icylinder.h"

namespace TerrificCoder {
ICylinder::ICylinder(TCCylinder* e): IObject(e)
{
	Vector center = e->getPosition();
	GLdouble radius = e->getWidth();
	GLdouble height = e->getHeight();
	Matrix m(e->getRotationMatrix());
	
	Vector4 v[8];
	v[0] = Vector4(radius,0,0,0);
	v[1] = Vector4(-radius,0,0,0);
	v[2] = Vector4(0,radius,0,0);
	v[3] = Vector4(0,-radius,0,0);
	v[4] = Vector4(radius,0,height,0);
	v[5] = Vector4(-radius,0,height,0);
	v[6] = Vector4(0,radius,height,0);
	v[7] = Vector4(0,-radius,height,0);
	
	for( int i=0; i<4; i++ ){
		v[i] *= &m;
		Vector *p = new Vector(v[i]);
		*p += center;
		points.push_back(p);
	}
}

std::list<Vector>* ICylinder::intersect(ISegment* )
{
	return NULL;
}

bool ICylinder::contain(Vector vector)
{
	return false;
}

	
};