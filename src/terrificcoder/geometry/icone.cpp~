/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "icone.h"

namespace TerrificCoder {

ICone::ICone(TCCone *cone) : IObject(cone)
{
	Vector center = cone->getPosition();
	Vector4 tip(0, 0, cone->getHeight(),0);
	Matrix m(cone->getRotationMatrix());
	tip *= &m;
	
	Vector top(tip);
	top += center;

	GLdouble radius = cone->getWidth();
	this->setup(top ,center, radius);

	points.push_back( new Vector(center) );
	points.push_back( new Vector(top) );

	Vector4 v[4];
	
	v[0] = Vector4(radius,0,0,0);
	v[1] = Vector4(-radius,0,0,0);
	v[2] = Vector4(0,radius,0,0);
	v[3] = Vector4(0,-radius,0,0);
	
	for( int i=0; i<4; i++ ){
		v[i] *= &m;
		Vector *p = new Vector(v[i]);
		*p += center;
		points.push_back(p);
	}

	
}


void ICone::setup(Vector tip, Vector center, GLdouble radius)
{
	GLdouble qr = radius * radius;
	GLdouble S11 = tip.quadLength() - qr ;
	
	
	//	S11 * S = S1 ^ 2;
	
	
	xcoef[0] = 2* ( -S11*center.x + tip.x*qr);
	xcoef[1] = S11 - tip.x*tip.x;
	ycoef[0] = 2* ( -S11*center.y + tip.y*qr);
	ycoef[1] = S11 - tip.y*tip.y;
	zcoef[0] = 2* ( -S11*center.z + tip.z*qr);
	zcoef[1] = S11 - tip.z*tip.z;

	xy = -2*tip.x*tip.y;
	yz = -2*tip.y*tip.z;
	zx = -2*tip.x*tip.z;

	nat = S11* (center.quadLength() - qr)  - qr*qr;




}

std::list<Vector> *ICone::intersect(ISegment* segment)
{
	Vector d = segment->versor();
	Vector p = segment->point1();
	
	GLdouble A = xcoef[1]* d.x*d.x + ycoef[1]*d.y*d.y + zcoef[1]*d.z*d.z 
					+ xy* d.x *d.y + zx*d.z*d.x + yz*d.y*d.z;
	GLdouble B = 2*( xcoef[1]*d.x*p.x + ycoef[1]*d.y*p.y + zcoef[1]* d.y*p.y )
				+ xy* (p.x*d.y+ p.y*d.x) + zx*(p.z*d.x +p.x*d.z) + yz*(p.y*d.z + p.z*d.y)
				+ xcoef[0]*d.x + ycoef[0]*d.y + zcoef[0]*d.z;
				

	GLdouble C = xcoef[1]*p.x*p.x + ycoef[1]*p.y*p.y + zcoef[1]*p.z*p.z
				+ xy*p.x*p.y + zx*p.z*p.x + yz*p.y*p.z
				+ xcoef[0]*p.x + ycoef[0]*p.y + zcoef[0]*p.z + nat;

	GLdouble disc = sqrt(B*B -4*A*C);
	std::list<Vector> *points = new std::list<Vector>();
	if ( disc > 0 ) {
		disc = sqrt(disc);
		GLdouble t1 = (-B - disc) / (2 * A);
		GLdouble t2 = (-B + disc) / (2 * A);
		Vector p1 = segment->pointAt(t1);
		points->push_back(p1);
		Vector p2 = segment->pointAt(t2);
		points->push_back(p2);
	} else if ( disc == 0 ) {
		GLdouble t = -B / 2*A;
		points->push_back(segment->pointAt(t));
	}
	return points;
}

bool ICone::contain(Vector vector)
{

}

};