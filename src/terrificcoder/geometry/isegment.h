/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <list>

#include "../library/vector.h"
#include "iplane.h"

#ifndef TERRIFICCODER_ISEGMENT_H
#define TERRIFICCODER_ISEGMENT_H

namespace TerrificCoder {
class IPlane;

class ISegment
{
private:
	Vector p1;
	Vector p2;
	Vector u;
    int id;
	static int counter;
public :
	ISegment(Vector4 p1, Vector4 p2);
	ISegment(Vector p1, Vector p2);
	Vector point1( );
	Vector point2( );
	Vector versor();
	
	GLdouble intersectPlane( IPlane *plane );
	Vector pointAt(GLdouble t);
	std::list<Vector>* intersectSphere(Vector centre, GLdouble radius);
	bool testPlane( IPlane *plane );
    bool onSegment(Vector point);
    
};

}

#endif // TERRIFICCODER_ISEGMENT_H
