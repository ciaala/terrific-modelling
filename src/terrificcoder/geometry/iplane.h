/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "../library/vector.h"

#ifndef TERRIFICCODER_IPLANE_H
#define TERRIFICCODER_IPLANE_H

namespace TerrificCoder {

class IPlane
{
	Vector4 equation;

	GLdouble z_min, z_max;
	GLdouble y_min, y_max;
	GLdouble x_min, x_max;
public:
	
	IPlane(Vector normal, Vector point );
	static IPlane* ByCorners(Vector corner1, Vector corner2, Vector corner3);
	Vector normal;
	Vector point;
	GLdouble a();
	GLdouble b();
	GLdouble c();
	GLdouble d();
	
	bool inPlane(Vector point);
};



}

#endif // TERRIFICCODER_IPLANE_H
