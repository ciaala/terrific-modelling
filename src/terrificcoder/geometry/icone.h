/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_ICONE_H
#define TERRIFICCODER_ICONE_H
#include "../model/tcsimpleobjects.h"
#include "../library/vector.h"
#include "isegment.h"
#include "iobject.h"

namespace TerrificCoder {

class ICone : public IObject
{
private:
	GLdouble xcoef[2];
	GLdouble ycoef[2];
	GLdouble zcoef[2];
	GLdouble xy;
	GLdouble yz;
	GLdouble zx;
	GLdouble nat;

	
	void setup(Vector tip, Vector center, GLdouble radius);
	
public:
    ICone(TCCone *cone);


	std::list< Vector >* intersect(TerrificCoder::ISegment* segment);
    virtual bool contain(Vector vector);
};

}

#endif // TERRIFICCODER_ICONE_H
