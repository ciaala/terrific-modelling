/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_IBOX_H
#define TERRIFICCODER_IBOX_H
#include <GL/gl.h>

#include "isegment.h"
#include "iplane.h"
#include "../library/vector.h"
#include "../library/matrix.h"
#include "../model/tcsimpleobjects.h"
#include "../scene/selectionbox.h"
#include "iobject.h"

namespace TerrificCoder {
class TCSelectionBox;
class IBox : public IObject
{
private:
	std::list<ISegment*> segments;
	std::list<IPlane*> planes;
	std::list<Vector*> points;
	

	void init( GLdouble width, GLdouble height, GLdouble depth, TerrificCoder::Vector center, TerrificCoder::Matrix* m );
	
public:
	IBox(TCSelectionBox *box );
	IBox(TCBox *box);
	IBox(TCObject *object);
	~IBox();

	std::list<IPlane*>* getPlanes();
	std::list<Vector*>* getPoints();
	std::list<ISegment*>* getSegments();

    virtual std::list< Vector >* intersect(TerrificCoder::ISegment* segment);
    virtual bool contain(Vector vector);
};

};
#endif // IBOX_H

