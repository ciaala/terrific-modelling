/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "iplane.h"

namespace TerrificCoder{


IPlane::IPlane(Vector normal, Vector point) : normal(normal), point(point)
{
	GLdouble d = -(normal.x*point.x + normal.y*point.y + normal.z*point.z);
	this->equation = Vector4( normal.x, normal.y, normal.z, d);
	
}

IPlane* IPlane::ByCorners(Vector corner1, Vector corner2, Vector corner3)
{
	
// 	Vector v1 = pos + ray;
// 	Vector v2 = pos + arw;


	Vector dir1 = corner2 - corner1;
	Vector dir2 = corner3 - corner1;
	Vector n = dir1 * dir2;
	n.normalize();


/*	float d = -(n.x * pos.x + n.y*pos.y + n.z * pos.z);
	Vector4 base_plane = Vector4(n.x, n.y, n.z ,d);*/
	
	IPlane *instance = new IPlane(n, corner1);
	instance->x_max = corner1.x > corner2.x ? corner1.x : corner2.x;
	instance->y_max = corner1.y > corner2.y ? corner1.y : corner2.y;
	instance->z_max = corner1.z > corner2.z ? corner1.z : corner2.z;
	instance->x_min = corner1.x < corner2.x ? corner1.x : corner2.x;
	instance->y_min = corner1.y < corner2.y ? corner1.y : corner2.y;
	instance->z_min = corner1.z < corner2.z ? corner1.z : corner2.z;
	
	return instance;
}

bool IPlane::inPlane(Vector point)
{
	return ( (this->x_max >= point.x ) && (point.x >= this->x_min) ) &&
	( (this->y_max >= point.y ) && (point.y >= this->y_min) ) &&
	( (this->z_max >= point.z ) && (point.z >= this->z_min) );
}


GLdouble IPlane::a()
{
	return equation.v[0];
}
GLdouble IPlane::b()
{
	return equation.v[1];
}
GLdouble IPlane::c()
{
	return equation.v[2];
}
GLdouble IPlane::d()
{
	return equation.v[3];
}

}