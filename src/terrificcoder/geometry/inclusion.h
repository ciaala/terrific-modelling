/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_INCLUSION_H
#define TERRIFICCODER_INCLUSION_H

#include "../patterns/visitor.h"
#include "../model/tcsimpleobjects.h"

#include "../scene/tcscene.h"
#include "../scene/selectionbox.h"
#include "../library/vector.h"
#include "iplane.h"
#include "isegment.h"
#include "ibox.h"
#include "isphere.h"
#include "icone.h"
#include "../scene/objectset.h"

namespace TerrificCoder {
class TCSelectionBox;
class IBox;



class FullInclusion : public TerrificCoder::Visitor
{
protected:
    TCScene *scene;
    TCSelectionBox *box;
	IBox *ibox;
	TCObjectSet* partial;
	TCObjectSet* full;
	bool tested;
	std::list<TCObject*> supports;

/* TESTING OBJECTS */
	GLdouble nearest(Vector p,GLdouble far);
    bool testPoints(std::list<Vector* > *points,TCObject *e);
    bool testSegments(std::list< ISegment* >* segment, TCObject *e);
	bool reverseTestPoints(IObject* io, TCObject* e);
	bool reverseTestSegments( IObject *ic, TCObject *e);
	
	void testIObject(TerrificCoder::IObject* io, TerrificCoder::TCObject* e);
	void testTCObject(TCObject* e);

	TCObject* insertSupport(TerrificCoder::TCObject* o);
/* TESTING OBJECTS */
public:
	FullInclusion( TerrificCoder::TCSelectionBox* box, TerrificCoder::TCScene* scene );
    virtual ~FullInclusion();

	void test();

	TCObjectSet *getFull();
	TCObjectSet *getPartial();
    virtual void visit(TerrificCoder::TCTeapot* e);
    virtual void visit(TerrificCoder::TCObjectSet* e);
    virtual void visit(TerrificCoder::TCPlane* e);
    virtual void visit(TerrificCoder::TCText3D* e);
    virtual void visit(TerrificCoder::TCText2D* e);
    virtual void visit(TerrificCoder::TCArrow* e);
    virtual void visit(TerrificCoder::TCLight* e);
    virtual void visit(TerrificCoder::TCCamera* e);
    virtual void visit(TerrificCoder::TCThorus* e);
    virtual void visit(TerrificCoder::TCSphere* e);
    virtual void visit(TerrificCoder::TCCone* e);
    virtual void visit(TerrificCoder::TCCylinder* e);
    virtual void visit(TerrificCoder::TCBox* e);
    virtual void visit(TerrificCoder::TCObject* e);

    virtual void visit(TerrificCoder::VisitorElement* e);


};

}

#endif // TERRIFICCODER_INCLUSION_H
