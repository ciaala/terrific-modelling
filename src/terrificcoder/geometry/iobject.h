/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_IOBJECT_H
#define TERRIFICCODER_IOBJECT_H
#include "isegment.h"
#include "iplane.h"
#include "../library/vector.h"

namespace TerrificCoder {
class TCObject;

class IObject
{
private:
	IObject();
protected:
	std::list<ISegment*> segments;
	std::list<IPlane*> planes;
	std::list<Vector*> points;
	TCObject* object;
public:
	IObject(TCObject *e);
	virtual ~IObject();
	virtual std::list<IPlane*>* getPlanes();
	virtual std::list<Vector*>* getPoints();
	virtual std::list<ISegment*>* getSegments();
	
	virtual std::list< Vector >* intersect(TerrificCoder::ISegment* segment) = 0;
	virtual bool contain(Vector vector) = 0;
};

}

#endif // TERRIFICCODER_IOBJECT_H
