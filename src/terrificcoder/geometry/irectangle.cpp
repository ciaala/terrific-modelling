/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "irectangle.h"
#include "math.h"
#include <sstream>
namespace TerrificCoder {
IRectangle::IRectangle() {
	this->left = NAN;
	this->right = NAN;
	this->top = NAN;
	this->bottom = NAN;
}

IRectangle::operator std::string() const{
	std::ostringstream result;
	result << "IRect { r:" << this->right << " l:" << this->left << " b:" << this->bottom << " t:" << this->top << "}";
	return result.str();
}

bool IRectangle::operator==(const TerrificCoder::IRectangle& r)
{
	if ( (this->bottom == r.bottom) &&
		(this->right == r.right) &&
		(this->left == r.left) &&
		(this->top == r.top) )
			return true;
	return false;
}
bool IRectangle::isValid()
{
	bool result = (!isnan(bottom)) && (!isnan(left)) && ( !isnan(right)) && (!isnan(top));
	return result;
}
	
std::ostream& operator<<(std::ostream &s, const TCRectangle &r)
{
	s << "Rectangle{r:" << r.right << " l:" << r.left << " b:" << r.bottom << " t:" << r.top << "}";
	return s;
}
};

