/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "ibox.h"
namespace TerrificCoder {


IBox::IBox(TCBox* box) : IObject(box)
{
	Matrix m(box->getRotationMatrix());
	init( 
		box->getWidth()/2.0,
		box->getHeight()/2.0,
		box->getDepth()/2.0,
		box->getPosition(),
		&m
	);
}

IBox::IBox(TCSelectionBox* box) : IObject(box)
{
	Matrix m(box->getRotationMatrix());
	init(
		box->getWidth()/2.0,
		 box->getHeight()/2.0,
		 box->getDepth()/2.0,
		 box->getPosition(),
		 &m
	);
}

IBox::IBox(TCObject* object) : IObject(object)
{
	Matrix m(object->getRotationMatrix());
	init(object->getWidth(),
		 object->getHeight(),
		 object->getDepth(),
		 object->getPosition(),
		 &m);
}

void IBox::init(GLdouble width, GLdouble height, GLdouble depth, Vector center, Matrix* m)
{
		/*
		 * 		3 ----- 4
		 *		|		|
		 *    	|		|
		 * 		2 ----- 1
		 */

		Vector4 p1(width,height,depth,1.0);
		Vector4 p2(-width,height,depth,1.0);
		Vector4 p3(-width,-height,depth,1.0);
		Vector4 p4(width,-height,depth,1.0);
		Vector4 p5(width,height,-depth,1.0);
		Vector4 p6(-width,height,-depth,1.0);
		Vector4 p7(-width,-height,-depth,1.0);
		Vector4 p8(width,-height,-depth,1.0);
		
		p1 *= m; p2*= m; p3 *= m; p4 *= m;
		p5 *= m; p6*= m; p7 *= m; p8 *= m;
		
		//std::list<Vector> *points = new std::list<Vector>();
		
		Vector v[8];
		v[0] = Vector(p1)+center;
		v[1] = Vector(p2)+center;
		v[2] = Vector(p3)+center;
		v[3] = Vector(p4)+center;
		v[4] = Vector(p5)+center;
		v[5] = Vector(p6)+center;
		v[6] = Vector(p7)+center;
		v[7] = Vector(p8)+center;
		
		for ( int i = 0; i< 8; i++ ){
			points.push_back( new Vector(v[i]) );
		}

		int i = 0;
		segments.push_back( new ISegment( v[i],v[i+4]));
		segments.push_back( new ISegment( v[i],v[i+1]));
		i++; // 1
		segments.push_back( new ISegment( v[i],v[i+4]));
		segments.push_back( new ISegment( v[i],v[i+1]));
		i++; // 2
		segments.push_back( new ISegment( v[i],v[i+4]));
		segments.push_back( new ISegment( v[i],v[i+1]));
		i++; // 3
		segments.push_back( new ISegment( v[i],v[i+4]));
		segments.push_back( new ISegment( v[i],v[i-3]));
		
		i++; // 4
		segments.push_back( new ISegment( v[i],v[i+1]));
		i++; // 5
		segments.push_back( new ISegment( v[i],v[i+1]));
		i++; // 6
		segments.push_back( new ISegment( v[i],v[i+1]));
		i++; // 7
		segments.push_back( new ISegment( v[i],v[i-3]));
		
		planes.push_back(IPlane::ByCorners(v[0],v[3],v[1]));
		planes.push_back(IPlane::ByCorners(v[0],v[4],v[3]));
		planes.push_back(IPlane::ByCorners(v[0],v[1],v[4]));

		planes.push_back(IPlane::ByCorners(v[6],v[5],v[2]));
		planes.push_back(IPlane::ByCorners(v[6],v[2],v[7]));
		planes.push_back(IPlane::ByCorners(v[6],v[5],v[5]));
}


IBox::~IBox()
{
	while(! planes.empty() ) {
		IPlane *t = planes.front();
		planes.pop_front();
		delete t;
	}
	while(! points.empty() ) {
		Vector *t = points.front();
		points.pop_front();
		delete t;
	}
	while(! segments.empty() ) {
		ISegment *t = segments.front();
		segments.pop_front();
		delete t;
	}
}



std::list< IPlane* >* IBox::getPlanes()
{
	return &planes;
}
std::list< Vector* >* IBox::getPoints()
{
	return &points;
}
std::list< ISegment* >* IBox::getSegments()
{
	return &segments;
}

std::list< Vector >* IBox::intersect(ISegment* segment)
{
	return NULL;
}


bool IBox::contain(Vector v)
{
// 	Vector c = object->getPosition();
// 	GLdouble w = object->getWidth();
// 	GLdouble h = object->getHeight();
// 	GLdouble d = object->getDepth();

// 	if ((v.x <= c.x +w) && (v.x >= c.x-w) &&
// 		(v.y <= c.y +h) && (v.y >= c.y-h) &&
// 		(v.z <= c.z +d) && (v.z >= c.z-d) ) {
// 		return true;
// 	} else {
// 		return false;
// 	}
	std::list<Vector*>::iterator it = points.begin();

	bool test[8];

	while ( it != points.end()) {
		std::list< IPlane* >::iterator jt = planes.begin();
		while (jt != planes.end())
		{
			IPlane *plane = *jt;
			
			Vector dir = plane->point - v;
			GLdouble value = dir.product_scalar(plane->normal);
			test[8] = value >0 ? true : false;
			jt++;
		}
		it++;
	}
	bool result = true;
	for (int i=1;i<8 && result;i++) {
		result = test[0] == test[i] ? true : false;
	}
	return result;
}

};
