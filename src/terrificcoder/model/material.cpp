/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "material.h"
#include "../library/logging.h"
namespace TerrificCoder {
Material::Material()
{
	ambient.first = GL_AMBIENT;
	diffuse.first = GL_DIFFUSE;
	emission.first = GL_EMISSION;
	specular.first = GL_SPECULAR;
	shininess.first = GL_SHININESS;

	ambient.second = new Vector4f(0.2,0.2,0.2,1.0);
	diffuse.second = new Vector4f(0.8,0.8,0.8,1.0);
	emission.second = new Vector4f(0.0,0.0,0.0,1.0);
	specular.second = new Vector4f(0.0,0.0,0.0,1.0);
	shininess.second = new Vector4f(0.0,0.0,0.0,0.0);
	
	materials.insert(ambient);
	materials.insert(diffuse);
	materials.insert(emission);
	materials.insert(specular);
	materials.insert(shininess);
}
Material::~Material()
{
	delete ambient.second;
	delete diffuse.second;
	delete emission.second;
	delete specular.second;
	delete shininess.second;
}

std::pair< materialIt, materialIt > Material::getMaterials()
{
	std::pair< materialIt,materialIt > result = make_pair(materials.begin(), materials.end());
	return result;
}


void Material::setMaterial ( GLenum type, Vector4f value)
{
	switch (type) {
		case GL_AMBIENT: {
			*(ambient.second) = value;
			materials.insert(ambient);
			break;
		}
		case GL_DIFFUSE: {
			*(diffuse.second) = value;
			materials.insert(diffuse);
			break;
		}
		case GL_SPECULAR: {
			*(specular.second) = value;
			materials.insert(specular);
			break;
		}
		case GL_EMISSION: {
			*(emission.second) = value;
			materials.insert(emission);
			break;
		}
		case GL_SHININESS: {
			*(shininess.second) = value;
			materials.insert(shininess);
			break;
		}
		default: {
			TCERR << "Unknown material type "<< type;
		}
	}
}

Vector4f Material::getMaterial ( GLenum type )
{
	switch (type) {
		case GL_AMBIENT: {
			return *ambient.second;
		}
		case GL_DIFFUSE: {
			return *diffuse.second;
		}
		case GL_SPECULAR: {
			return *specular.second;
		}
		case GL_EMISSION: {
			return *emission.second;
		}
		case GL_SHININESS: {
			return *shininess.second;
		}
		default: {
			TCERR << "Unknown material type "<< type;
			return Vector4f();
		}
	}
}



};

