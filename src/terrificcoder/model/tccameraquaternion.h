/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TCCAMERAQUATERNION_H
#define TCCAMERAQUATERNION_H

#include <GL/gl.h>
#include <GL/glu.h>
//#include "../tccontainer.h"
#include "tccamera.h"
#include "../library/tclibrary.h"
#include "../scene/scenepoint.h"

namespace TerrificCoder {
	
class TCCameraQuaternion : public TCCamera
{

public:
	TCCameraQuaternion();
	
	
	Quaternion ql,qm,qn;
	Quaternion qz_plus, qz_minus;
	Quaternion qx_plus, qx_minus;

	
	GLfloat rotationQuantum(GLfloat arg1);

	void viewportPadding(GLfloat dm, GLfloat dn);
	void viewportPaddingStop();
	void updateTimer();

	virtual void draw();
    virtual void draw_disabled();
	virtual void accept(Visitor* v);
	
};

};
#endif // TCCAMERAQUATERNION_H
