/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef TCCAMERA_H
#define TCCAMERA_H
#include <GL/gl.h>
#include <GL/glu.h>


#include "tcobject.h"
#include "../patterns/observer.h"

namespace TerrificCoder {
	
class TCContainer;
class TCGui;

class TCCamera : public TCObject, public TerrificCoder::Subject
{

/**
 * Camera Update
 * 
 */
public:
	void perspective();
	void lookat();
	void overlayLookat();
protected :
	void rotate(Vector q, GLdouble degree );
	
public:
	TCCamera(TerrificCoder::TCObjectEnum type);
	virtual ~TCCamera();

 	virtual void viewportResize(GLint width, GLint height);
	
	void zoom(GLint arg1);
// 	virtual void set_center(GLfloat x, GLfloat y, GLfloat z);
// 	virtual void set_up(GLfloat x, GLfloat y, GLfloat z);
// 	virtual void setRight(GLfloat x, GLfloat y, GLfloat z);
// 	Vector getCenter( );
// 	Vector getUp();
// 	Vector getRight();

    virtual void draw_disabled();
	virtual void pitch( GLdouble unit );
	virtual void roll( GLdouble unit );
	virtual void yaw( GLdouble unit );
	
	virtual void forward( GLdouble unit );
	virtual void up(GLdouble unit );
	virtual void right(GLdouble unit );
public:
	virtual void setPosition(Vector position);

private:
//	void projection();
	

public:
	/** Implement them !! */
	virtual void viewportPadding(GLfloat dm, GLfloat dn)=0;
	virtual void updatePosition( Vector p1) = 0;
    void setEye(Vector arg1);
    virtual void reposition(Vector position, Vector normal, Vector centre)=0;
protected:

	
	// Viewport
	GLint height;
	GLint width;
	
	// Perspective
	GLfloat fov;
	GLfloat aspect;
	GLfloat zfar;
	GLfloat znear;
	
	// Camera
	//Position eye;
	Position center;
	Vector l, m, n;

	GLfloat angle_phi(Vector v);
	GLfloat angle_theta(Vector v);

	// Spherical Model
	GLfloat phi; 
	GLfloat theta;
	GLfloat rho;

	GLfloat delta_angle;
	virtual void accept(Visitor* v);
};

};
#endif // CAMERA_H
