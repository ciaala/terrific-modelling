/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "texturemanager.h"
#include "GL/gl.h"
#include <utility>
namespace TerrificCoder {
int TextureInfo::counter = 1;

TextureInfo::TextureInfo(std::string &filename, GLsizei width, GLsizei height,unsigned char *bits)
{
	texture_counter = counter++;
	this->bits = bits;
	this->filename = filename;
	glGenTextures( 1, &texture_id );
	glBindTexture( GL_TEXTURE_2D, texture_id );
	glTexImage2D( GL_TEXTURE_2D, 0, 3, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, bits );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

}
TextureManager::TextureManager() { }

void TextureManager::setScene(TCScene* scene)
{
	this->scene = scene;
}

void TextureManager::assignTexture(unsigned int object_id, unsigned int texture_id)
{
	scene->getObject(object_id)->setTextureID(texture_id);
	textures.at(texture_id)->object_ids.push_back(object_id);

}


GLuint TextureManager::registerTexture2D(std::string filename, GLsizei width, GLsizei height, unsigned char* bits)
{
	TextureInfo *ti = new TextureInfo(filename,width, height,bits);
	textures.insert(std::make_pair(ti->texture_id ,ti));
	return ti->texture_id;
}

std::pair<TextureIterator,TextureIterator> TextureManager::getTextureList() {
	std::pair<TextureIterator,TextureIterator > result = std::make_pair(textures.begin(),textures.end());
	return result;
}



};

