/*
 *    <one line to give the program's name and a brief idea of what it does.>
 *    Copyright (C) <year>  <name of author>
 * 
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 * 
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 * 
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TCLIGHT_H
#define TCLIGHT_H
#include "tcsimpleobjects.h"

namespace TerrificCoder {
	
class TCLight : public TCSimpleObject {
private:
	static GLint light_counter;
	static GLint light_enumeration[8];
protected:
	GLint number;
	Vector4f ambient;
	Vector4f diffuse;
	Vector4f specular;
	Vector4f spot;
	Vector4f lightPosition;
	
	Vector4f spotDirection;
	GLfloat spotExponent;
	GLfloat spotCutoff;
public:
	virtual void draw();
    virtual void draw_disabled();
	
	TCLight();
    virtual ~TCLight();
	GLfloat* getLightPosition();
	GLint getNumber();
	GLint shininess();
	GLfloat* getAmbient();
	GLfloat* getDiffuse();
	GLfloat* getSpecular();
	GLfloat* getSpot();
	
	GLfloat* getSpotDirection();
	GLfloat getSpotCutoff();
	GLfloat getSpotExponent();
	void setAmbient(GLint red, GLint green, GLint blue, GLint alpha);
	void setSpot(GLint red, GLint green, GLint blue, GLint alpha);
	void setSpecular(GLint red, GLint green, GLint blue, GLint alpha);
	void setDiffuse(GLint red, GLint green, GLint blue, GLint alpha);
	void setSpotDirection(TerrificCoder::Vector4f direction);
	void setCutoff(GLfloat value);
	void setExponent(GLfloat value);
	virtual void accept(Visitor* v);
	Vector getPosition( );
	void setPosition(Vector v);
};
};

#endif
