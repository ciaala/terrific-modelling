/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TCOBJECT_H
#define TCOBJECT_H
#include <GL/gl.h>
#include <string>

#include "../terrificcoder.h"
#include "../library/tclibrary.h"
#include "../patterns/visitor.h"
#include "../scene/selectable.h"
#include "material.h"
#include "texture.h"

namespace TerrificCoder {

class TCObject : public TerrificCoder::VisitorElement, public TerrificCoder::Selectable, public TerrificCoder::Material, public TerrificCoder::Texture
{
public:
	
	TCObject(TerrificCoder::TCObjectEnum type=Any, std::string name=std::string("Object"));
	virtual ~TCObject();
	unsigned int getId();
	
	virtual std::string getName();
	virtual void setName(std::string name);
	virtual Vector getPosition();
	virtual void setPosition(Vector position);
	virtual Vector getRotation();
	virtual void setRotation(Vector rotation);
	virtual void setRotation(double rotx, double roty, double rotz);

	virtual void setColor(GLint red, GLint green, GLint blue, GLint alpha);
	virtual TCObjectEnum getType();
	virtual GLdouble* getColor();
	virtual GLdouble* getRotationMatrix();
	
	virtual void draw()=0;
	virtual void draw_disabled();
	
	virtual operator std::string() const;
	bool operator==(TCObject &b);


	
	virtual void setWidth(GLdouble width);
	GLdouble getWidth();
	virtual void setHeight(GLdouble height);
	GLdouble getHeight();
	virtual void setDepth(GLdouble depth);
	GLdouble getDepth();
	virtual void setInner(GLdouble inner);
	GLdouble getInner();
protected:
	Matrix rotationMatrix;
	Vector position;
	Vector rotation;
	Vector4 color;
	std::string name;
	std::string name_id;
	TCObjectEnum type;

	GLdouble width;
	GLdouble height;
	GLdouble depth;
	GLdouble inner;
private:

	unsigned id;
	static unsigned int counter;
	
};

};
#endif // TCOBJECT_H
