/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "tctext.h"
#include <GL/freeglut_std.h>
#include <GL/freeglut_ext.h>

namespace TerrificCoder{

TCText3D::TCText3D() : TCObject(Text3D,"Text3D")
{
	font = GLUT_STROKE_ROMAN;
	text = "Hello World!";
}

void TCText3D::draw()
{
	glScalef(0.1,0.1,0.1);
	glutStrokeString(font,(const unsigned char*) text.c_str());
}

void TCText3D::accept(TerrificCoder::Visitor* v)
{
	v->visit(this);
}

TCText2D::TCText2D() : TCObject(Text2D,"Text2D")
{
	font = GLUT_BITMAP_HELVETICA_12;
	text = "Hello World";
}
void TCText2D::accept(Visitor* v)
{
	v->visit(this);
}
void TCText2D::draw()
{
	glutBitmapString(font, (const unsigned char*) text.c_str());
}



};