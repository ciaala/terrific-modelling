/*
 *  <one line to give the program's name and a brief idea of what it does.>
 *  Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "tclastcamera.h"
#include "../container.h"

#define TCLOGGING_LEVEL TCERR_LEVEL
#include "../library/logging.h"

namespace TerrificCoder {
	
TCLastCamera::TCLastCamera() : TCCamera(QuaternionCamera)	
{
	
	
	center = Vector(0.0,0.0,0.0);
	
	position = Vector(1.0,0.0,0.0);
	l = center - position;
	l.normalize();
	n = Vector(0.0,0.0,1.0);
	m = (n*l);
	m.normalize();

#ifdef DEBUG_LASTCAMERA
	apitch.setColor(255,0,128,255);
	aroll.setColor(0,128,255,255);
	ayaw.setColor(128,255,0,255);


	Container::getScene()->insertTemporary(&apitch);
	Container::getScene()->insertTemporary(&aroll);
	Container::getScene()->insertTemporary(&ayaw);
#endif
	
}

void TCLastCamera::viewportPadding(GLfloat dm, GLfloat dn)
{
	GLfloat rad = rho/36;
	dm = dm > 0 ? -rad : (dm < 0 ? rad :0);
	dn = dn > 0 ? rad : (dn < 0 ? -rad :0);
	TCDEBUG << " dm dn " << dm << dn;
	TCDEBUG << " center position " << center << position;
	TCDEBUG << " m n l =  "  << m << n << l ;
	Vector mt = m; mt.scale(dm);
	Vector nt = n; nt.scale(dn);
	TCDEBUG << " nt mt =  "  << nt << mt ;
	Vector p1 = position + nt + mt;
	updatePosition(p1);
}

void TCLastCamera::updatePosition( Vector p1) {

	// Position p1 on the sphere
	Vector l1 = center - p1;
	l1.normalize();l1.scale(rho);
	p1 = center - l1;
	l1.normalize();
	TCDEBUG << " p1 l1 =  "  << p1 << l1 ;
	/**
	 * Vector ptemp = p1 + m
	 * p1 + center + ptemp => plane _|_  n
	 */
// 	Vector4 plane = computePlane(l1,m ,p1);
// 	Vector n1 = Vector( plane.v[0], plane.v[1], plane.v[2] );
// 	n1.normalize();


	/*plane = computePlane( l1, n, p1);
	Vector m1 = Vector( plane.v[0], plane.v[1], plane.v[2] );
	*/
	Vector m1 = n * l1;
	m1.normalize();
	Vector n1 = l1 * m1;
	n1.normalize();
	TCDEBUG << " n1 m1 =  "  << n1 << m1 ;

	Vector lt = m1 * n1;lt.normalize();
	TCDEBUG << " lt =  m1 x n1 "  << lt << " l1" << l1 ;

	Vector nt = l1* m1;nt.normalize();
	TCDEBUG << " nt = l1 x m1 "  << nt << " l1" << n1 ;
	
	Vector mt = n1 * l1;mt.normalize() ;
	TCDEBUG << " mt = n1 x l1 "  << mt << " l1" << m1 ;
	TCDEBUG << std::endl;
	l = l1;
	m = m1;
	n = n1;
	position = p1;
#ifdef DEBUG_LASTCAMERA
	apitch.setup(l,center);
	aroll.setup(n,center);
	ayaw.setup(m,center);
#endif
	notify();
}

void TCLastCamera::draw()
{

}

void TCLastCamera::reposition(Vector position, Vector normal, Vector center)
{
	this->center = center;
	normal.normalize();
	this->n = normal;
	this->rho = (position-center).length();
	updatePosition(position);
}


};