/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "tccameraquaternion.h"
#include "math.h"
#include "../container.h"
#include "GL/freeglut.h"

namespace TerrificCoder{


TCCameraQuaternion::TCCameraQuaternion() : TCCamera::TCCamera(QuaternionCamera)
{

	this->position = Vector(20.0f,20.0f,20.0f);
	this->center = Vector(0.0f,0.0f,0.0f);
	this->n = Vector(0.0f,0.0f,1.0f);
	this->l = center-position;
	rho = l.length();
	l.normalize();
	n.normalize();
	m = (l * n);
	m.normalize();
	qz_plus = Quaternion(delta_angle,Vector(0,0,1));
	qz_minus = Quaternion(-delta_angle,Vector(0,0,1));
	qx_plus = Quaternion(delta_angle,Vector(1,0,0));
	qx_minus = Quaternion(-delta_angle,Vector(1,0,0));


}

/**
 * Handles camera movement
 *
 * dm the X-axis movement of the mouse on the gui
 * dn the Y-axis movement of the mouse on the gui
 */
void TCCameraQuaternion::viewportPadding(GLfloat dm, GLfloat dn)
{
	dm = dm > 0 ? 10 : (dm < 0 ? -10 :0);
	dn = dn > 0 ? 10 : (dn < 0 ? -10 :0);

// 	std::clog << "[CAMERA] {m,n ="<< m << ", "<< n<<"} ";
	GLfloat alpha = (M_PI * fov/180.0)*(dm );
	GLfloat beta = (M_PI * fov/180.0)*(dn );
// 	std::clog << "{width, height = " << width <<", "<<height <<"} ";
// 	std::clog << "{alpha,beta = " <<alpha << ", " << beta << " } ";
	GLfloat dsm = rho * tanf( alpha );
	GLfloat dsn = rho * tanf( beta );	
	
	Vector axis_x(1.0,0.0,0.0);
	Vector axis_y(0.0,1.0,0.0);
	Vector axis_z(0.0,0.0,1.0);
	
	GLfloat axis_xm = axis_x.product_scalar(m);
	GLfloat axis_ym = axis_y.product_scalar(m);
	GLfloat axis_zm = axis_z.product_scalar(m);
// 	std::clog << "{axis_m =[" << axis_xm <<","<< axis_ym<<","<<axis_zm << "], ";
	
	GLfloat axis_xn = axis_x.product_scalar(n);
	GLfloat axis_yn = axis_y.product_scalar(n);
	GLfloat axis_zn = axis_z.product_scalar(n);
// 	std::clog << "{axis_n =[" << axis_xn <<","<< axis_yn<<","<<axis_zn << "], ";
	
	GLfloat dx = dsm*axis_xm + dsn*axis_xn;
	GLfloat dy = dsm*axis_ym + dsn*axis_yn;
	GLfloat dz = dsm*axis_zm + dsn*axis_zn;
	
	Vector delta( dx, dy, dz );

// 	std::clog << "{delta =[" << dx <<","<< dy<<","<<dz << "], ";

	theta = angle_theta(delta);
	phi = angle_phi(delta);
// 	std::clog << " } {theta,phi ="<< theta <<", " << phi <<"}"  << std::endl ;
	
	updateTimer();
	
}
/*
void TCCameraQuaternion::viewportPaddingStop(){
	modeDraw();
	theta = 0;
	phi = 0;
}*/

// GLfloat TCCameraQuaternion::rotationQuantum(GLfloat angle ){
// 	if ( angle*4 > delta_angle ) {
// 		return angle > delta_angle ? delta_angle : angle;
// 	} else {
// 		return 0;
// 	}
// }


void TCCameraQuaternion::updateTimer()
{
	//phi = delta_angle;
	//theta = delta_angle;
	if ( theta > 0){
		position = qz_plus.rotateVector(position);
		n = qz_plus.rotateVector(n);
	//	theta -= delta_angle;
// 		gui->message(TCGui::Debug, "Theta: %f",theta);
	} else if ( theta < 0){
		position = qz_minus.rotateVector(position);
		n = qz_minus.rotateVector(n);
	//	theta += delta_angle;
// 		gui->message(TCGui::Debug, "Theta: %f",theta);
	}
	if ( theta!=0 && fabs(theta) < delta_angle ) {
		theta = 0;
// 		gui->message(TCGui::Debug, "Theta: %f",theta);
	}
	
	
	if ( phi > 0 ) {
		position = qx_plus.rotateVector(position);
		n = qx_plus.rotateVector(n);
	//	phi -= delta_angle;
	} else if ( phi <0 ) {
		position = qx_minus.rotateVector(position);
		n = qx_minus.rotateVector(n);
	//	phi += delta_angle;
	}
	if ( phi!= 0 && fabs(phi) < delta_angle ) {
		phi = 0;
	}
	
	
	this->l = center-position;
	rho = l.length();
	l.normalize();
	n.normalize();
	m = (l * n);
	m.normalize();
	
	//setPosition(position.x,position.y,position.z);
// 	position = 
// 	this->setPosition(Vector(position.x,position.y,position.z));
// 	this->set_up(n.x,n.y,n.z);
// 	this->set_center(center.x,center.y,center.z);

	this->notify();
	
}

void TCCameraQuaternion::draw()
{
	
}
void TCCameraQuaternion::draw_disabled()
{
//		Vector l = this->center - this->position;
	glutSolidCone(5.0,5.0,10,10);
// 		Position a = this->position;
// 		
// 		Position b = this->position + (this->l - this->m);
// 		Position c = this->position + (this->l + this->m);
// 		glBegin(GL_TRIANGLES);
// 			glVertex3fv(a.v);
// 			glVertex3fv(b.v);
// 			glVertex3fv(c.v);
// 		glEnd();

}

void TCCameraQuaternion::accept(TerrificCoder::Visitor* v)
{
	v->visit(this);
}

};
