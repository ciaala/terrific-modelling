/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_MATERIAL_H
#define TERRIFICCODER_MATERIAL_H

#include "../library/vector.h"
#include <list>
#include <utility>
#include <map>

typedef std::map <GLenum, TerrificCoder::Vector4f*>::const_iterator materialIt;

namespace TerrificCoder {

class Material
{
private:
	std::pair<GLenum ,Vector4f*> ambient;
	std::pair<GLenum ,Vector4f*> diffuse;
	std::pair<GLenum ,Vector4f*> specular;
	std::pair<GLenum ,Vector4f*> emission;
	std::pair<GLenum ,Vector4f*> shininess;

	std::map<GLenum,Vector4f*> materials;
public:
	Material();
	~Material();
	std::pair<materialIt,materialIt> getMaterials();
	void setMaterial( GLenum type, Vector4f value);
	Vector4f getMaterial( GLenum type );
};

};

#endif // TERRIFICCODER_MATERIAL_H
