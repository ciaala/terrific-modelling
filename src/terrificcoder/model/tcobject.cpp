/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <yea
    r>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "tcobject.h"
#include <iostream>
#include <sstream>




namespace TerrificCoder
{

unsigned int TCObject::counter = 1;

TCObject::TCObject(TCObjectEnum type, std::string name)
{
	this->type = type;
	this->id = counter++;
	this->setName(name);
	this->position.set(0,0,0);
	this->rotation.set(0,0,0);
	this->setColor(128,128,128,255);
	this->depth = 10.0;
	this->width = 10.0;
	this->height = 10.0;
	this->inner = 5.0;
}
TCObject::~TCObject()
{

}

TCObjectEnum TCObject::getType()
{
	return this->type;
}

std::string TCObject::getName()
{
	
	return this->name_id;
}
void TCObject::setName(std::string name)
{
	std::ostringstream o;
	o << name << " [" << id << "]";
	this->name = name;
	this->name_id = o.str();
}



Vector TCObject::getPosition()
{
	return position;
}
void TCObject::setPosition(Vector position)
{
	this->position = position;
}

Vector TCObject::getRotation()
{
	return rotation;
}

GLdouble* TCObject::getRotationMatrix() {
	return this->rotationMatrix.toArray();
}
/**
 * Requires angles between 0-360
 */
void TCObject::setRotation(double rotx, double roty, double rotz)
{
	if ( rotx != -1 ) {
		this->rotation.x = (M_PI*rotx)/180.0;
	}
	if ( roty != -1 ) {
		this->rotation.y = (M_PI*roty)/180.0;
	}
	if ( rotz != -1 ) {
		this->rotation.z = (M_PI*rotz)/180.0;
	}
	this->rotationMatrix = Matrix(rotation.x,rotation.y,rotation.z);
}

void TCObject::setRotation(Vector rotation)
{
// 	std::clog << "TCObject: " << this->name << " setRotation(" << rotation << ")"<<std::endl;
	this->rotation = rotation;
	this->rotationMatrix = Matrix(rotation.x,rotation.y,rotation.z);
}

TCObject::operator std::string() const {
	
	return name_id;
}

unsigned int TCObject::getId()
{
	return this->id;
}


void TCObject::setColor(GLint red, GLint green, GLint blue, int alpha)
{
// 	std::clog << "TCObject setColor(" << red << "," << green << "," << blue << "," <<alpha<<")"<<std::endl;
	color.v[0] = ((GLfloat) red)/255.0;
	color.v[1] = ((GLfloat) green)/255.0;
	color.v[2] = ((GLfloat) blue)/255.0;
	color.v[3] = ((GLfloat) alpha)/255.0;
}

GLdouble* TCObject::getColor()
{
	return color.v;
}

bool TCObject::operator== ( TCObject& b )
{	
	return this->id == b.getId();
}
void TCObject::draw_disabled()
{

}

GLdouble TCObject::getDepth()
{
	return depth;
}

GLdouble TCObject::getHeight()
{
	return height;
}

GLdouble TCObject::getInner()
{
	return inner;
}
GLdouble TCObject::getWidth()
{
	return width;
}

void TCObject::setDepth(GLdouble depth)
{
	this->depth = depth;
}
void TCObject::setHeight(GLdouble height)
{
	this->height = height;
}
void TCObject::setInner(GLdouble inner)
{
	this->inner = inner;
}
void TCObject::setWidth(GLdouble width)
{
	this->width = width;
}






};