/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_TEXTUREMANAGER_H
#define TERRIFICCODER_TEXTUREMANAGER_H


#include "GL/gl.h"
#include <map>
#include <string>
#include "../scene/tcscene.h"
#include "../patterns/observer.h"

namespace TerrificCoder {
class TCScene;



class TextureInfo {
	static int counter;
public:
	TextureInfo (std::string &filename, GLsizei width, GLsizei height,unsigned char *bits);
	unsigned char * bits;
	std::list<GLuint> object_ids;
	GLuint texture_id;
	GLuint texture_counter;
	std::string filename;

};

typedef std::map <unsigned int, TextureInfo* >::iterator TextureIterator;

class TextureManager : public TerrificCoder::Subject
{
private :
	TCScene *scene;
	std::map<unsigned int, TextureInfo* > textures;
public :
	void setScene(TCScene *scene);
	TextureManager();
	GLuint registerTexture2D(std::string filename, GLsizei width, GLsizei height, unsigned char *bits);
	void assignTexture(unsigned int object_id, unsigned int texture_id);
	std::pair<TextureIterator,TextureIterator> getTextureList();

};




}

#endif // TERRIFICCODER_TEXTUREMANAGER_H
