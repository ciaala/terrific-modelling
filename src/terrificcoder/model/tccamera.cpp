/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include <stdlib.h>
#include <GL/freeglut.h>
#include <math.h>

#include "tccamera.h"
#include "../container.h"
#define TCLOGGING_LEVEL TCERR_LEVEL
#include "../library/logging.h"
namespace TerrificCoder{
	
/**
 * 
 * container the container class
 * gui the gui class_interface that will be used for user feedback
 * width viewport width
 * height viewport height
 * 
 */ 
TCCamera::TCCamera(TerrificCoder::TCObjectEnum type) : TCObject(type,"Camera")
{
	this->fov = 60;
	this->zfar = 200000.0;
	this->znear = 1.0;
	this->aspect = 16.0/9.0;

	this->theta = 0;
	this->phi = 0;
	
	GLfloat fps = 60.0;
	delta_angle = M_PI * (fov /(180.0*fps));
	color = Vector4(.5,.5,.5,.9 );
}

TCCamera::~TCCamera()
{
	
}

void TCCamera::viewportResize(GLint width,GLint height)
{
	aspect = (width*1.0f)/(height*1.0f);
}

void TCCamera::zoom(GLint delta)
{
	GLfloat scale_factor =1+ (GLfloat)delta / 1200.0f;
	Vector diff = position - center;
	diff.scale(scale_factor);
	position = center + diff;
	rho = diff.length();

}
/*

void TCCamera::set_center(GLfloat x, GLfloat y, GLfloat z)
{
	this->center.set(x,y,z);

}

void TCCamera::set_up(GLfloat x, GLfloat y, GLfloat z)
{
	this->n.set(x,y,z);

}
void TCCamera::setRight(GLfloat x, GLfloat y, GLfloat z)
{
	this->m.set(x,y,z);

}

Vector TCCamera::getCenter()
{
	return this->center;
}

Vector TCCamera::getUp()
{
	return this->n;
}
Vector TCCamera::getRight()
{
	return this->m;
}

GLfloat TCCamera::angle_phi(Vector v)
{
	//Vector c(v.x,v.y,0);
	return atan2( v.z, v.y);
}

GLfloat TCCamera::angle_theta(Vector v)
{
	//Vector c(0, v.y, v.z);
// 	std::clog << v ;
	return atan2(v.x,v.y);
}
*/

void TCCamera::accept(TerrificCoder::Visitor* v)
{
	v->visit(this);
}
void TCCamera::lookat()
{
	gluLookAt(position.x, position.y, position.z,
				center.x, center.y,center.z,
				n.x, n.y, n.z);
}
void TCCamera::overlayLookat()
{
	Vector p = position-center;
	
	p.normalize();
	p.scale(2);
	
	gluLookAt(p.x, p.y, p.z,
			  0, 0, 0,
		   n.x, n.y, n.z);
}
void TCCamera::perspective()
{
	gluPerspective( fov, aspect,znear,zfar );
}

void TCCamera::pitch(GLdouble unit)
{
	rotate(m ,M_PI*unit/180.0);
}
void TCCamera::roll(GLdouble unit)
{
	rotate(l ,M_PI*unit/180.0);
}
void TCCamera::yaw(GLdouble unit)
{
	rotate(n ,M_PI*unit/180.0);
}
void TCCamera::rotate( Vector v, GLdouble degree )
{
	
	
	Quaternion q(degree, v);
	
	TCDEBUG << "q x {l,m,n} " << q << "x {" << l << m << n <<"} "<< std::endl;
	this->m = q.rotateVector(m);
	this->l = q.rotateVector(l);
	this->n = q.rotateVector(n);
	this->m.normalize();
	this->l.normalize();
	this->n.normalize();
	TCDEBUG << " = {" << l << m << n <<"} "<< std::endl;
	Vector away = l;
	away.scale(rho);
	this->center = position + away;
}

void TCCamera::forward(GLdouble unit)
{
	Vector movement = l;
	movement.scale(unit);
	position += movement;
	center += movement;
	setPosition(position);
}
void TCCamera::right(GLdouble unit)
{
	Vector movement = m;
	movement.scale(unit);
	position += movement;
	center += movement;
	setPosition(position);
}
void TCCamera::up(GLdouble unit)
{
	Vector movement = n;
	movement.scale(unit);
	position += movement;
	center += movement;
	setPosition(position);
}
void TCCamera::setPosition(Vector position)
{
	this->position = position;
	rho = (center - position).length();
	updatePosition(position);
}

void TCCamera::setEye(Vector point)
{
	Vector diff = position - center;
	this->center = point;
	this->position = point + diff;
	updatePosition(position);
}




void TCCamera::draw_disabled()
{
	if ( Container::getRenderer()->getCamera() != this ) {
		GLdouble w = 2.0;
		GLdouble h = 2.0;
		GLdouble d = 2.0;
		#   define V(a,b,c) glVertex3d( a w, b h, c*d);
		#   define VP(a,b,c) glVertex3d( a*w, b*h, c*d);
		#   define N(a,b,c) glNormal3d( a, b, c );

		glBegin( GL_LINE_LOOP ); N( 0.0, 0.0, -1.0); V(+,+,0); V(-,+,0); V(-,-,0); V(+,-,0); glEnd();

		glBegin( GL_LINE_STRIP ); N(-1.0, 0.0, 0.0); V(-,-,0); VP(0,0,1); V(+,+,0); glEnd();
		glBegin( GL_LINE_STRIP ); N(-1.0, 0.0, 0.0); V(-,+,0); VP(0,0,1); V(+,-,0); glEnd();

	}
}

};
