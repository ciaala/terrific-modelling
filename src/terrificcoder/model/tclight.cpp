/*
 *    <one line to give the program's name and a brief idea of what it does.>
 *    Copyright (C) <year>  <name of author>
 * 
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 * 
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 * 
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "tclight.h"
#include "GL/glut.h"
#include "../library/tclibrary.h"

namespace TerrificCoder {
	GLint TCLight::light_counter = 0;
	GLint TCLight::light_enumeration [8] = {GL_LIGHT0,GL_LIGHT1,GL_LIGHT2,GL_LIGHT3,GL_LIGHT4,GL_LIGHT5,GL_LIGHT6,GL_LIGHT7};


TCLight::TCLight() : TCSimpleObject(Light,"Light")
{
	this->lightPosition = Vector4f(0.0f, 0.0f, 0.0f, 1.0f);
	this->ambient = Vector4f(0.7f,0.7f,0.0f,1.0f );
	this->diffuse = Vector4f(0.7f,0.7f,0.0f,1.0f );
	this->specular = Vector4f(1,1,1,1);
	this->spot = Vector4f(0,0,0,1);
	this->spotDirection = Vector4f(1,0,0,0);
	this->color = Vector4(0.7,0.7,0,0.5);
	this->spotCutoff = 45.0;
	this->spotExponent = 2.0;
	this->number = light_enumeration[light_counter];
	light_counter++;
}

TCLight::~TCLight()
{

}
GLint TCLight::getNumber()
{
	return this->number;
}

void TCLight::draw()
{

}
void TCLight::draw_disabled()
{
	glPushMatrix();
	glScaled(4.0,4.0,4.0);
	glutWireDodecahedron();
	glPopMatrix();
}

GLfloat* TCLight::getLightPosition()
{
	return lightPosition.v;
}

GLfloat* TCLight::getAmbient()
{
	return ambient.v;
}
GLfloat* TCLight::getDiffuse()
{
	return diffuse.v;
}
GLfloat* TCLight::getSpecular()
{
	return specular.v;
}
GLfloat* TCLight::getSpotDirection()
{
	return spotDirection.v;
}
GLfloat TCLight::getSpotCutoff()
{
	return spotCutoff;
}
GLfloat TCLight::getSpotExponent()
{
	return spotExponent;
}
GLfloat* TCLight::getSpot()
{
	return spot.v;
}

GLint TCLight::shininess()
{
	// TODO is it really used and useful ?
	return 0;
}

void TCLight::setAmbient(GLint red, GLint green, GLint blue, GLint alpha)
{
	std::clog << "TCLight setAmbient(" << red << "," << green << "," << blue << "," <<alpha<<")"<<std::endl;
	this->ambient.v[0] = colorFloat(red);
	this->ambient.v[1] = colorFloat(green);
	this->ambient.v[2] = colorFloat(blue);
	this->ambient.v[3] = colorFloat(alpha);
}
void TCLight::setDiffuse(GLint red, GLint green, GLint blue, GLint alpha)
{
	std::clog << "TCLight setDiffuse(" << red << "," << green << "," << blue << "," <<alpha<<")"<<std::endl;
	this->diffuse.v[0] = colorFloat(red);
	this->diffuse.v[1] = colorFloat(green);
	this->diffuse.v[2] = colorFloat(blue);
	this->diffuse.v[3] = colorFloat(alpha);
}
void TCLight::setSpecular(GLint red, GLint green, GLint blue, GLint alpha)
{
	std::clog << "TCLight setSpecular(" << red << "," << green << "," << blue << "," <<alpha<<")"<<std::endl;
	this->specular.v[0] = colorFloat(red);
	this->specular.v[1] = colorFloat(green);
	this->specular.v[2] = colorFloat(blue);
	this->specular.v[3] = colorFloat(alpha);
}
void TCLight::setSpot(GLint red, GLint green, GLint blue, GLint alpha)
{
	std::clog << "TCLight setSpot(" << red << "," << green << "," << blue << "," <<alpha<<")"<<std::endl;
	this->spot.v[0] = colorFloat(red);
	this->spot.v[1] = colorFloat(green);
	this->spot.v[2] = colorFloat(blue);
	this->spot.v[3] = colorFloat(alpha);
}
void TCLight::setCutoff(GLfloat value)
{
	this->spotCutoff = value;
}
void TCLight::setExponent(GLfloat value)
{
	this->spotExponent = value;
}
void TCLight::setSpotDirection(Vector4f direction)
{
	this->spotDirection = direction;
}
void TCLight::accept(TerrificCoder::Visitor* v)
{
	v->visit(this);
}
Vector TCLight::getPosition()
{
	GLfloat *v = lightPosition.v;
	return Vector(v[0],v[1],v[2]);
}
void TCLight::setPosition(Vector v)
{
	lightPosition.v[0] = v.x;
	lightPosition.v[1] = v.y;
	lightPosition.v[2] = v.z;
}

};