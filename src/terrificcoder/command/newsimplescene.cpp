/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "newsimplescene.h"
#include "../container.h"
#include "../model/tccameraquaternion.h"
#include "selectcamera.h"
#include <QTime>
#include "stdlib.h"
using namespace TerrificCoder;

void NewSimpleScene::execute()
{
	TCScene *scene =  Container::getScene();
	TCCamera *camera = (TCCamera *) scene->objectCreate(TerrificCoder::QuaternionCamera);
	camera->setName("Default Camera");
	camera->setPosition(Vector(200,200,200));
	TCLight *light = (TCLight *) scene->objectCreate(TerrificCoder::Light);
	light->setName("Default Light");
	light->setPosition(Vector(-100,0,0));
	
// 	TCBox *box = (TCBox *) scene->objectCreate(TerrificCoder::Box);
// 	box->setName("Default Cube");
	int max = 200;
	int min = -200;
	int range = max - min;
	
	qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
	for ( int i=0; i < 10; i++ ){
		TCSphere *sphere = (TCSphere *) scene->objectCreate(TerrificCoder::Sphere);

		int x = qrand() % range-max;
		int y = qrand() % range-max;
		int z = qrand() % range-max;

		sphere->setPosition(Vector(x,y,z));
		sphere->setColor(128,128,qrand()%256,255);
	}
	for ( int i=0; i < 10; i++ ){
		TCBox *box = (TCBox*) scene->objectCreate(TerrificCoder::Box);

		int x = qrand() % range-max;
		int y = qrand() % range-max;
		int z = qrand() % range-max;

		box->setPosition(Vector(x,y,z));
		box->setColor(128,128,qrand()%256,255);
	}
	scene->objectCreate(Teapot)->setName("Default Teapot");
 	Container::addCommand(new SelectCamera(camera));
	Container::setSelectedObject(NULL);
}

