/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_NEWSELECTIONSCENE_H
#define TERRIFICCODER_NEWSELECTIONSCENE_H

#include "command.h"
#include "../scene/tcscene.h"

namespace TerrificCoder {

class NewSelectionScene : public TerrificCoder::Command
{
private :
	void build(TCScene* scene, TCObjectEnum type, int step, Vector p0);
public:
    virtual void execute();
	
};

}

#endif // TERRIFICCODER_NEWSELECTIONSCENE_H
