/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "selectmultipleobject.h"
#include "../scene/objectset.h"
#include "../container.h"

namespace TerrificCoder
{

void SelectMultipleObject::execute()
{
	TCObject *o = Container::getSelectedObject();
	if ( o != NULL ) {
		if ( o->getType() == ObjectSet) {
			TCObjectSet *set = (TCObjectSet*) o;
			if ( set->getList()->size() == objects->size() ) {
				/**
				 * They are two list of elements of the same length
				 * we check if their elements are the same
				 */
				std::list<TCObject*> *selected = set->getList();
				objects->sort();
				selected->sort();
				if ( ! std::equal( selected->begin(), selected->end(), objects->begin(), std::equal_to<TCObject*>()) ) {
					Container::setSelectedObject(new TCObjectSet(objects));
					return;
				} else {
					Container::setSelectedObject(NULL);
					return;
				}				
			}
		}
	}
	/**
	 * we have a list of elements
	 * and the selected are different by length or type
	 * or even they are not selected
	 */
	Container::setSelectedObject(new TCObjectSet(this->objects));
	
}

SelectMultipleObject::SelectMultipleObject(std::list<TCObject*> *objects)
{
	this->objects = objects;
}

SelectMultipleObject::~SelectMultipleObject() {}

};