/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_NEWOBJECT_H
#define TERRIFICCODER_NEWOBJECT_H

#include "command.h"

#include "../model/tcobject.h"
#include "../terrificcoder.h"


namespace TerrificCoder {

class NewObject : public TerrificCoder::Command
{
public:
	NewObject(TCObjectEnum type);
    NewObject(TCObject *object);
    virtual ~NewObject();
	virtual void execute();
private:
	TCObjectEnum type;
	TCObject *object;
};

};

#endif // TERRIFICCODER_NEWOBJECT_H
