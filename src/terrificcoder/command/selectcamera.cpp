/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "selectcamera.h"
#include "../container.h"

namespace TerrificCoder {

SelectCamera::SelectCamera()
{
	this->camera = NULL;
}
SelectCamera::SelectCamera(TCCamera* camera)
{
	this->camera = camera;
}

SelectCamera::~SelectCamera()
{

}

void SelectCamera::execute()
{
	if ( this->camera != NULL ) {
		Container::getRenderer()->setCamera(this->camera);
	} else {
		TCObject *o =  Container::getSelectedObject();
		if ( o != NULL && o->getType() == QuaternionCamera ){
			this->camera = static_cast<TCCamera*>(o);
		}
	}
}

};

