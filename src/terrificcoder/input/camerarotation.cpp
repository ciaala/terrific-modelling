/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "camerarotation.h"
#include "../renderer/glrenderer.h"
#include "../container.h"
#define TCLOGGING_LEVEL TCERR_LEVEL
#include "../library/logging.h"
#include "../qt/tcglwidget.h"

namespace TerrificCoder {
	
CameraRotation::CameraRotation() : InputState("CameraRotation")
{
	this->tracking = false;
}
void CameraRotation::mousePressEvent(GLint , GLint , MouseButton button)
{
	if ( RightButton == button ) {
		this->tracking = this->tracking ? false : true;
		
		Container::getRenderer()->setMouseAtCentre();
	}
}
void CameraRotation::mouseReleaseEvent(GLint , GLint , TerrificCoder::MouseButton )
{

}
void CameraRotation::mouseTrackEvent(GLint x, GLint y, MouseButton )
{
	if ( tracking == true ) {
		GLRenderer *renderer = Container::getRenderer();
		std::pair<int,int> vp = renderer->getViewportSize();
		x -= vp.first/2;
		y -= vp.second/2;
		
		
		GLint delta_x = abs(x) > 15 ? limit(x,100) : 0;
		GLint delta_y = abs(y) > 15 ? limit(y,100) : 0;
		
		if ( delta_x || delta_y ) {
			
			renderer->viewportPadding(delta_x,-delta_y);
			Container::getRenderer()->setMouseAtCentre();
		}
	}
}

// void CameraRotation::old_track(GLint x, GLint y) {
// 	if ( tracking == true ) {
// 		GLRenderer *renderer = Container::getRenderer();
// // 		viewport_center[0] = renderer->getViewportSize().first;
// // 		viewport_center[1] = renderer->getViewportSize().second;
// 		std::pair<GLint, GLint> dim = renderer->getViewportSize();
// 		x -= dim.first /2;
// 		y = -y+ dim.second/2;
// 
// 		
// 		GLint dx = dim.first/5;
// 		GLint dy = dim.second/5;
// 
// 		
// 		GLint delta_x = limit(x,dx);
// 		GLint delta_y = limit(y,dy);
// 		TCDEBUG << "x,y (" << x <<","<< y << ") dx,dy (" << delta_x <<","<<delta_y<< ")";
// 		if ( delta_x || delta_y ) {
// 			renderer->viewportPadding(delta_x,delta_y);
// 		}
// 		return;
// 	}
// }
void CameraRotation::mouseWheelEvent(GLint delta, Orientation )
{
	Container::getRenderer()->zoom(delta);
}


void CameraRotation::keyPress(Key key)
{
	GLRenderer *renderer = Container::getRenderer();
	TCCamera *camera = renderer->getCamera();
	std::pair<GLint, GLint> dim = renderer->getViewportSize();
	int w = 100;
	int h = -100;
	if ( camera ) {
		switch( key ) {
			case Right: {
				camera->viewportPadding(w,0);
				return;
			}
			case Left: {
				camera->viewportPadding(-w,0);
				return;
			}
			case Up: {
				camera->viewportPadding(0,h);
				return;
			}
			case Down: {
				camera->viewportPadding(0,-h);
				return;
			}
// 			case KEY_Q:{
// 				camera->pitch(1);
// 				return;
// 			}
// 			case KEY_A:{
// 				camera->pitch(-1);
// 				return;
// 			}
// 			case KEY_W: {
// 				camera->roll(1);
// 				return;
// 			}
// 			case KEY_S: {
// 				camera->roll(-1);
// 				return;
// 			}
// 			case KEY_E: {
// 				camera->yaw(1);
// 				return;
// 			}
// 			case KEY_D: {
// 				camera->yaw(-1);
// 				return;
// 			}
// 			case KEY_Y: {
// 				camera->right(1);
// 				return;
// 			}
// 			case KEY_H: {
// 				camera->right(-1);
// 				return;
// 			}
// 			case KEY_U: {
// 				camera->forward(1);
// 				return;
// 			}
// 			case KEY_J: {
// 				camera->forward(-1);
// 				return;
// 			}
// 			case KEY_I: {
// 				camera->up(1);
// 				return;
// 			}
// 			case KEY_K: {
// 				camera->up(-1);
// 				return;
// 			}
			default:
				return;
		}
	}
}
void CameraRotation::keyRelease(Key )
{

}


};

