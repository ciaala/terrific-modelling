/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_INPUTCONTEXT_H
#define TERRIFICCODER_INPUTCONTEXT_H

#include "inputstate.h"
#include "navigationstate.h"
#include "camerarotation.h"
#include "selection.h"
#include "selectionvolume.h"
#include "arrowmovement.h"
#include "camerareset.h"
#include "free.h"

#include "../container.h"
#include "../patterns/observer.h"
#include "../renderer/glrenderer.h"


namespace TerrificCoder {

class ArrowMovement;
class NavigationState;
class SelectionVolume;
class Free;
enum InputHandler{ InputSelect, InputTranslate, InputRotation, InputFree, InputSelectionVolume };

class InputContext : public TerrificCoder::InputState, public TerrificCoder::Observer
{
public:
	
	void mousePressEvent(GLint x, GLint y, MouseButton button);
	void mouseTrackEvent(GLint x, GLint y, MouseButton button);
	void mouseWheelEvent(GLint delta, Orientation orientation);
	void mouseReleaseEvent(int x, int y, MouseButton button);
    void keyPress(TerrificCoder::Key key);
    void keyRelease(TerrificCoder::Key key);
    
    virtual void update(Subject* subject);
public:
	
	InputContext(GLRenderer *renderer, TCScene *scene);
	~InputContext();
    Vector getMouseCoordinate();
    void changeHandler (TerrificCoder::InputState* handler);
	void changeHandler (TerrificCoder::InputHandler handler);
	void reset();
private:
	void printContext();
	void setMouse(int x, int y);
	void setupCurrent(InputState *next);

	
	Vector mouse;
	GLRenderer *renderer;
	InputState *current;
	TerrificCoder::NavigationState *navigation;
	TerrificCoder::CameraRotation *cameraRotation;
	TerrificCoder::Selection *selection;
	TerrificCoder::SelectionVolume *selectionVolume;
	TerrificCoder::ArrowMovement *arrowMovement;
	TerrificCoder::CameraReset *cameraReset;
	TerrificCoder::Free *free;
	int times;
};

}

#endif // TERRIFICCODER_INPUTCONTEXT_H
