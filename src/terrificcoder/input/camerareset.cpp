/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software) you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http)//www.gnu.org/licenses/>.
*/


#include "camerareset.h"
#include "../container.h"
#define TCLOGGING_LEVEL TCERR_LEVEL
#include "../library/logging.h"

namespace TerrificCoder {

CameraReset::CameraReset(TerrificCoder::Selection* selection, TerrificCoder::GLRenderer* renderer) : Clickable()
{
    this->renderer = renderer;
    this->selection = selection;
  //  Renderer *sr = renderer->getSceneRenderer();
    overlay = renderer->getOverlay();
    TCArrowHead ** heads = overlay->getHeads();
    for ( int i =0;i <6;i++ ) {
        selection->registerClickableObject(heads[i],this);
    }
    selection->registerClickableObject(&overlay->box,this);
}

void CameraReset::clicked(TerrificCoder::TCObject* object, int x, int y, TerrificCoder::MouseButton button)
{
    Vector position;
	Vector normal;
    double v = 1000;
    if ( object == &overlay->m_x) {
        position = Vector(-v,0,0);
		normal = Vector(0,0,1);
    } else if ( object == &overlay->p_x ) {
        position = Vector(v,0,0);
		normal = Vector(0,0,1);
    } else if ( object == &overlay->m_y ) {
        position = Vector(0,-v,0);
		normal = Vector(0,0,1);
    } else if ( object == &overlay->p_y ) {
        position = Vector(0,v,0);
		normal = Vector(0,0,1);
    } else if ( object == &overlay->m_z ) {
        position = Vector(0,0,-v);
		normal = Vector(0,1,0);
    } else if ( object == &overlay->p_z ) {
        position = Vector(0,0,v);
		normal = Vector(0,1,0);
	} else if ( object == &overlay->box ) {
		position = Vector(v,v,v);
		normal = Vector(-1,-1,1);
	}  else  return ;
	TCDEBUG << "ResetCamera: " << position << std::endl;
    updateCamera(position,normal);
}

void CameraReset::updateCamera(Vector position, Vector normal)
{
	TCCamera *camera = Container::getRenderer()->getCamera();
	
	if (camera) {
		camera->reposition(position,normal,Vector(0,0,0));
	}
}


};


