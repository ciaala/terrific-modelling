/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_VOLUMECREATION_H
#define TERRIFICCODER_VOLUMECREATION_H

#include <list>
#include "inputstate.h"
#include "../library/vector.h"

namespace TerrificCoder {
class TCScenePoint;
class GLRenderer;


class VolumeCreation : public TerrificCoder::InputState
{
private:
	GLRenderer* renderer;
	Vector4 plane_xy;
	Vector4 plane_xz;
	Vector4 plane_yz;
	Vector4 currentPlane;
	std::list<TCScenePoint*> points;
	TCScenePoint *cursor;
public:
    VolumeCreation(GLRenderer *renderer);
	virtual void disable();
	virtual void enable();

    virtual void keyRelease(TerrificCoder::Key key);
    virtual void keyPress(TerrificCoder::Key key);
    virtual void mouseReleaseEvent(int x, int y, TerrificCoder::MouseButton button);
    virtual void mouseWheelEvent(GLint delta, TerrificCoder::Orientation orientation);
    virtual void mouseTrackEvent(GLint x, GLint y, TerrificCoder::MouseButton button);
    virtual void mousePressEvent(GLint x, GLint y, TerrificCoder::MouseButton button);
	
private:
	TCScenePoint* insertTemporaryPoint(Vector position);
	Vector getPlanePosition(Vector4 plane);
	void updateCurrentPlane();
	

};

}

#endif // TERRIFICCODER_VOLUMECREATION_H
