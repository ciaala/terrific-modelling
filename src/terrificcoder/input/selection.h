/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_SELECTION_H
#define TERRIFICCODER_SELECTION_H

#include "inputstate.h"
#include "clickmanager.h"
#include "../library/tclibrary.h"
#include "../renderer/glrenderer.h"
#include "../scene/tcscene.h"
#include "../patterns/observer.h"

namespace TerrificCoder {

class Selection : public TerrificCoder::InputState, public ClickManager
{
public:
    Selection(GLRenderer * renderer,TCScene *scene);
    virtual ~Selection();
public:
    virtual void keyRelease(TerrificCoder::Key key);
    virtual void keyPress(TerrificCoder::Key key);
    virtual void mouseReleaseEvent(int x, int y, TerrificCoder::MouseButton button);
    virtual void mouseWheelEvent(GLint delta, TerrificCoder::Orientation orientation);
    virtual void mouseTrackEvent(GLint x, GLint y, TerrificCoder::MouseButton button);
    virtual void mousePressEvent(GLint x, GLint y, TerrificCoder::MouseButton button);

    virtual void disable();
    virtual void enable();

	
private:
	std::list<GLuint> ids;
	std::list<GLuint> overs;
    TerrificCoder::TCScene *scene;
	TerrificCoder::GLRenderer *renderer;
	virtual void fillSelectionBuffers(int x, int y, GLsizei sceneSize, GLuint *sceneBuffer,int *scene,GLsizei overlaySize, GLuint* overlayBuffer,int *overlay);
	virtual void processSelectionBuffer(GLint hits, GLuint *selectBuffer, std::list<GLuint>* ids);
	bool handleClickable(GLint x, GLint y, TerrificCoder::MouseButton button);
	std::list<TCObject*>* idsToList();
};

};

#endif // TERRIFICCODER_SELECTION_H
