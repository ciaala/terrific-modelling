/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "volumecreation.h"
#include "../container.h"
#include "../scene/scenepoint.h"
#include "../library/logging.h"
#include "../scene/selectionbox.h"

namespace TerrificCoder {

VolumeCreation::VolumeCreation(GLRenderer* renderer): InputState("Volume Creation")
{
    this->renderer = renderer;
	this->plane_yz = Vector4(1.0,0,0);
	this->plane_xz = Vector4(0,1,0,0);
	this->plane_xy = Vector4(0,0,1,0);
	
}


void VolumeCreation::keyRelease(TerrificCoder::Key )
{

}

void VolumeCreation::keyPress(TerrificCoder::Key )
{

}

void VolumeCreation::mouseReleaseEvent(int , int , TerrificCoder::MouseButton )
{

}

void VolumeCreation::mouseWheelEvent(GLint , TerrificCoder::Orientation )
{

}

void VolumeCreation::mouseTrackEvent(GLint , GLint , TerrificCoder::MouseButton )
{
	if ( cursor )
		Container::getScene()->deleteTemporary(cursor);
	Vector position = this->getPlanePosition(currentPlane);
	cursor = this->insertTemporaryPoint(position);
	
	//std::clog << pnear << pfar;
// 	if ( point.x > -1000 && point.x < 1000) {
	TCDEBUG << "The point " << position;
// 	}
// 	(" << dbx <<", "<< dby << ", " << dbz <<") on the plane is " << point << std::endl;
	
}

void VolumeCreation::mousePressEvent(GLint , GLint , TerrificCoder::MouseButton )
{

	Vector position = this->getPlanePosition(currentPlane);
	TCScenePoint *point = this->insertTemporaryPoint(position);
	this->updateCurrentPlane();
	points.push_back(point);
	
	if ( points.size() == 3 ) {
// 		Container::getScene()->insertTemporary(new TCSelectionBox(&points));
	}
}
void VolumeCreation::enable()
{
	currentPlane = plane_xy;
	cursor = NULL;
	//Container::getScene()->deleteAllTemporary();
}

void VolumeCreation::disable()
{
	currentPlane = plane_xy;
//	Container::getScene()->deleteAllTemporary();
}

TCScenePoint* VolumeCreation::insertTemporaryPoint(Vector position)
{
	TCScenePoint *point = new TCScenePoint();
	point->setPosition(position);
	point->setColor(128,255,255,255);
	Container::getScene()->insertTemporary(point);
	return point;
}

Vector VolumeCreation::getPlanePosition(Vector4 plane)
{
	Vector pnear = renderer->getCamera()->getPosition();
	Vector pfar = renderer->getSceneCoordinate();
	Vector result = planePoint(pnear,pfar,plane);
	result.approx();
	return result;
}

void VolumeCreation::updateCurrentPlane()
{
	if ( currentPlane == plane_xy ) {
		currentPlane = plane_xz;
	} else if (currentPlane == plane_xz ) {
		currentPlane = plane_yz;
	} else if (currentPlane == plane_yz ) {
		currentPlane = plane_xy;	
	}
}

};
