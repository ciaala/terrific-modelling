/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "selection.h"
#include <iostream>
#include <sstream>
#include "../container.h"
#include "../model/tccamera.h"
#include "../command/selectobject.h"
#include "../command/selectmultipleobject.h"
#include "../command/highlightobject.h"
#define TCLOGGING_LEVEL TCERR_LEVEL
#include "../library/logging.h"
#include "time.h"


namespace TerrificCoder {

Selection::Selection(GLRenderer *renderer,TCScene *scene): InputState("Selection")
{
    this->scene = scene;
    this->renderer = renderer;

}
Selection::~Selection()
{

}

void Selection::keyRelease(TerrificCoder::Key )
{

}

void Selection::keyPress(TerrificCoder::Key key)
{
	switch ( key ) {
		case BACKSPACE:
		case CANC:{
			Container::DeleteSelectedObject();
			break;
		}
		case KEY_C: {
			TCObject *obj = Container::getSelectedObject();
			TCCamera * camera = Container::getRenderer()->getCamera();
			if ( camera && obj) {
				camera->setEye(obj->getPosition());
			}
		}
		default :
			break;
	}
}

void Selection::mouseReleaseEvent(GLint , GLint , TerrificCoder::MouseButton )
{

}

void Selection::mouseWheelEvent(GLint , TerrificCoder::Orientation )
{

}

void Selection::mouseTrackEvent(GLint x, GLint y, TerrificCoder::MouseButton )
{
	int sceneCount,overlayCount;
    if ( renderer->getCamera() == NULL )
        return;
    GLuint sceneBuffer[4096] = {(GLuint)-1};
	GLuint overlayBuffer[4096] = {(GLuint)-1};
    fillSelectionBuffers(x,y,4096,sceneBuffer,&sceneCount,4096,overlayBuffer,&overlayCount);
    ids.clear();
	overs.clear();
    scene->resetHightlightBox();
	if (sceneCount > 0) {
		processSelectionBuffer(sceneCount, sceneBuffer,&ids);
        scene->highlightSelected(&ids);
		if ( ids.size() == 1 ) {
			TCObject * o = scene->getObject(ids.front());
			Container::addCommand(new HighlightObject(o));
		} else {
			Container::addCommand(new HighlightObject(idsToList()));
		}

	} else {
        Container::addCommand(new HighlightObject());
    }
    if ( overlayCount >0 ) {
		
		processSelectionBuffer(overlayCount, overlayBuffer,&overs);
		
	}
}

void Selection::mousePressEvent(GLint x, GLint y, TerrificCoder::MouseButton button )
{
    int size = this->ids.size();

    if ( size >= 1 || overs.size() >=1) {
        if ( !handleClickable(x,y,button) ) {
            if ( size == 1)  {
				TCObject *o = scene->getObject(ids.front());
				if ( o ) 
					Container::addCommand(new SelectObject(o));
			} else if (size >1) {
				Container::addCommand(new SelectMultipleObject(idsToList()));
			}
		}
    } else {
        Container::addCommand(new SelectObject());
    }
}
bool Selection::handleClickable(GLint x, GLint y, TerrificCoder::MouseButton button) {
    bool result = false;
	for ( std::list<GLuint>::iterator it=ids.begin(); it != ids.end(); it++) {
        GLuint id = *it;
        result = result || notifyClickable(id,x,y,button);
    }
    for ( std::list<GLuint>::iterator it=overs.begin(); it != overs.end(); it++) {
		GLuint id = *it;
		result = result || notifyClickable(id,x,y,button);
	}
    return result;
}

std::list<TCObject*>* Selection::idsToList()
{
    std::list<TCObject*> *objects = new std::list<TCObject*>();
    for (std::list< GLuint >::iterator it=ids.begin(); it!= ids.end(); it++) {
        TCObject *o = scene->getObject(*it);
        if ( o ) {
            objects->push_back(o);
        }
    }
    return objects;
}

void Selection::fillSelectionBuffers(int x, int y, GLsizei sceneSize, GLuint *sceneBuffer,int *scene,GLsizei overlaySize, GLuint* overlayBuffer,int *overlay)
{
	
	glSelectBuffer(sceneSize, sceneBuffer);
	*scene = renderer->selection_scene(x,y);
//	*scene = 0;
	glSelectBuffer(overlaySize, overlayBuffer);
	*overlay = renderer->selection_overlay(x,y);
// 	*overlay = 0;
}



/*


	*/
void Selection::processSelectionBuffer(GLint hits, GLuint *selectBuffer, std::list<GLuint>* ids)
{
	

	std::ostringstream result;
    GLuint *v = selectBuffer;
    for (int i=0; i < hits; i++)
    {
        GLuint count = *v-1;
        v+=3;
        GLuint id = *v++;
        if (id != (GLuint)-1) {
            ids->push_back(id);
			result <<id << ", ";
        }
        /* USE THIS FOR THE NESTED NAMES WHILE USING NESTED NAME STACK */
        while ( count--) {
            v++;
            if (id != (GLuint)-1) {
                /* NESTED NAMES ACCUMULATE HERE */
            }
        }
    }
    time_t t;
	time(&t);
    TCDEBUG << " Find<"<<t <<"> :"<< result.str();
}

void Selection::disable()
{
//	ids.clear();
	scene->resetHightlightBox();
}

void Selection::enable()
{
	ids.clear();
	scene->resetHightlightBox();
}


};
