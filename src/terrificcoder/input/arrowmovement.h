
/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_MOUSEMOVEMENT_H
#define TERRIFICCODER_MOUSEMOVEMENT_H

#include "clickable.h"
#include "point.h"
#include "selection.h"
namespace TerrificCoder
{

class ArrowMovement : public TerrificCoder::Clickable, public InputState
{
private:
    TCArrow *ax, *ay, *az;
	Vector original_position;
	TCObject *selected;
	Selection *selection;
	Vector4 support_plane;
	TCPlane* myplane;
	GLRenderer *renderer;
	TCScene* scene;
	Vector arrow;
	TCLine *line;
	bool done;
private :
	
// 	Vector4 computePlane(Vector ray, Vector arw, Vector pos);
	Vector4 currentPlane;
    TCPlanePoint* pp;
public:
    virtual void clicked ( TerrificCoder::TCObject* x, int y, int button, TerrificCoder::MouseButton object );
    ArrowMovement(Selection* selection, TerrificCoder::GLRenderer* renderer, TerrificCoder::TCScene* scene);
    virtual ~ArrowMovement();

    virtual void keyPress(Key key);
    virtual void keyRelease(Key key);
    virtual void mousePressEvent(GLint x, GLint y, MouseButton button);
    virtual void mouseReleaseEvent(int x, int y, MouseButton button);
    virtual void mouseTrackEvent(GLint x, GLint y, MouseButton button);
    virtual void mouseWheelEvent(GLint, TerrificCoder::Orientation);
    virtual void disable();
    virtual void enable();

};

}

#endif // TERRIFICCODER_MOUSEMOVEMENT_H
