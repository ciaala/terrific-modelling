/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "point.h"
#include "../container.h"
#include "GL/gl.h"
namespace TerrificCoder {


Point::Point(GLRenderer *renderer, TCScene *scene): InputState("ScenePoint")
{
	this->renderer = renderer;
	this->scene = scene;
	this->plane_xy = Vector4(0,0,1,0);
	cursor = new TCPlanePoint( );
	basePoint = new TCPlanePoint( );
	cursor->setColor(128,255,255,255);
// 	debugplane = new TCPlane(Vector4(0,0,1,0));
}



void Point::mouseTrackEvent(GLint , GLint , TerrificCoder::MouseButton )
{
	Vector position = this->updateCursor();
	this->cursorAt(position);
}

void Point::enable()
{
	this->disable();
	state = plane;
	
}

void Point::disable()
{
	isCursorInserted = false;
	currentPlane = plane_xy;
	scene->removeTemporary(cursor);
	scene->removeTemporary(basePoint);
// 	scene->removeTemporary(debugplane);
	state = none;
}


void Point::mousePressEvent(GLint , GLint , TerrificCoder::MouseButton button)
{
	Vector position = updateCursor();

	if ( state == plane ) {
		currentPlane = pickPlane(position);

		basePoint->setPosition(position);
		scene->insertTemporary(basePoint);
		state = axis;
	} else if ( state == axis ) {
		currentPlane = plane_xy;
		this->newPoint(position);
		state = plane;
// 		scene->removeTemporary(basePoint);
	}

}

Vector Point::updateCursor()
{
	Vector position = this->getPlanePosition(currentPlane);

	if ( state == axis ){
		position = Vector(xy_position.x,xy_position.y, position.z);
	} else if ( state == plane) {
		xy_position = position;
	}
	cursor->setPosition(position);

	showDebugPlane(position);
	if ( !isCursorInserted ) {
		isCursorInserted = true;
		scene->insertTemporary(cursor);
// 		scene->insertTemporary(debugplane);
	}
	
	return position;
}

Vector Point::getPlanePosition(Vector4 plane)
{
	Vector pnear = renderer->getCamera()->getPosition();
	Vector pfar = renderer->getSceneCoordinate();
	Vector result = planePoint(pnear,pfar,plane);
	result.approx();
	return result;
}
Vector4 Point::pickPlane(Vector position)
{
	TCCamera* camera = renderer->getCamera();
	Vector eye = camera->getPosition();
	
	return computePlane(eye ,Vector(0,0,1),position);
	
}
void Point::showDebugPlane(Vector position)
{
	Vector4 result;
	result = pickPlane(position);
// 	debugplane->setup(result);

}

void Point::keyRelease(TerrificCoder::Key ){ }
void Point::keyPress(TerrificCoder::Key ){}
void Point::mouseReleaseEvent(int , int , TerrificCoder::MouseButton ){ }
void Point::mouseWheelEvent(GLint , TerrificCoder::Orientation ){}

};