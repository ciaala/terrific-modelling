/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_CAMERAROTATION_H
#define TERRIFICCODER_CAMERAROTATION_H

#include "inputstate.h"


namespace TerrificCoder {

class CameraRotation : public TerrificCoder::InputState
{
public:
    CameraRotation();
	
    virtual void mousePressEvent(GLint x, GLint y, MouseButton button);
    virtual void mouseReleaseEvent(int x, int y, MouseButton button);
    virtual void mouseTrackEvent(GLint x, GLint y, MouseButton button);
    virtual void mouseWheelEvent(GLint delta, Orientation orientation);
	
    virtual void keyPress(Key key);
    virtual void keyRelease(Key key);
    bool tracking;
private:
    int viewport_center[2];
};

};

#endif // TERRIFICCODER_CAMERAROTATION_H
