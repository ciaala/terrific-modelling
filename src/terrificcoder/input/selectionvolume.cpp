/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "selectionvolume.h"
#include "../container.h"
#include "../scene/selectionbox.h"
#include "../command/newobject.h"

namespace TerrificCoder{

SelectionVolume::SelectionVolume(InputContext *inputContext, TerrificCoder::GLRenderer* renderer, TerrificCoder::TCScene* scene) : Point(renderer,scene) {
	temporaryVolume = NULL;
	this->inputContext = inputContext;
	this->state = none;
}
void SelectionVolume::disable()
{
    TerrificCoder::Point::disable();
	scene->deleteTemporary(temporaryVolume);
	temporaryVolume = NULL;
}

void SelectionVolume::enable()
{
    TerrificCoder::Point::enable();
	state = none;
	temporaryVolume = NULL;
}

void SelectionVolume::newPoint(Vector position)
{
	if ( none == state ) {
		first = position;
		state = firstPoint;
	} else if ( firstPoint == state ) {
		second = position;
		state = secondPoint;
	}
	
	if ( secondPoint == state ) {
		TCSelectionBox *object = new TCSelectionBox(first, second );
		object->fill();
		Container::addCommand(new NewObject( object ));
		if ( temporaryVolume )
			scene->deleteTemporary(temporaryVolume);
		
		inputContext->reset();
	}
}

void SelectionVolume::cursorAt(Vector position)
{
	if ( firstPoint == state ) {
		if ( temporaryVolume != NULL ) {
			temporaryVolume->setOppositePoint(position);		
		} else {
			temporaryVolume = new TCSelectionBox(first, position );
			scene->insertTemporary(temporaryVolume);
		}
	}
}
};