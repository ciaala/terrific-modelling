/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_CLICKMANAGER_H
#define TERRIFICCODER_CLICKMANAGER_H

#include "clickable.h"
#include <map>
#include <list>
#include "../model/tcobject.h"

namespace TerrificCoder {


class ClickManager
{
private:
	std::multimap<TCObject*, Clickable* > handler;
	std::map<GLuint, TCObject*> ids;
public:
	ClickManager();
	TCObject* contains(GLuint id);
	bool notifyClickable(GLuint id, int x, int y, MouseButton button );
	void registerClickableObject(TCObject *object,Clickable* clickable);
	void unregisterClickableObject(TCObject *object,Clickable* clickable);
};

}

#endif // TERRIFICCODER_CLICKMANAGER_H
