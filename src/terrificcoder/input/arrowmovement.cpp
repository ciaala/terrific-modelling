/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "arrowmovement.h"
#include "../container.h"
#include "../library/logging.h"
#include <sstream>
 namespace TerrificCoder {

void ArrowMovement::clicked ( TerrificCoder::TCObject* object, int , int , TerrificCoder::MouseButton  )
{
	std::stringstream ss;

	this->selected = Container::getSelectedObject();
	if (selected != NULL && object != NULL && !done){
		Vector p = selected->getPosition();
		this->original_position = p;
		Vector r = selected->getRotation();
		
		
		if (object == ax) {
			arrow = Vector(1.0,0.0,0.0);

		} else if (object == ay) {
			arrow = Vector(0.0,1.0,0.0);

		} else if (object == az) {
			arrow = Vector(0.0,0.0,1.0);

		}

/*
		Matrix rotation = Matrix( );
		rotation;*/
		Quaternion qr = Quaternion::RotationQuaternion(r);
		arrow = qr.rotateVector(arrow);
		arrow.normalize();
		Vector pcamera = renderer->getCamera()->getPosition();

		this->currentPlane = computePlane(p - pcamera, arrow, p);

//		scene->deleteTemporary(myplane);
//		myplane = new TCPlane(currentPlane);
//		myplane->setColor(255,0,128,255);
//		scene->insertTemporary(myplane);
		line = new TCLine(arrow,p);
		scene->insertTemporary(line);

		this->done = true;
		Container::getInputContext()->changeHandler(this);
	}
}

ArrowMovement::ArrowMovement( Selection *selection, GLRenderer* renderer, TCScene* scene) : Clickable(), InputState("ArrowMovement" )
{
	
	this->renderer = renderer;
	this->selection = selection;
	this->scene = scene;
	
	Renderer *sr = renderer->getSceneRenderer();
	selection->registerClickableObject( &(sr->arrow_x),this );
	selection->registerClickableObject( &(sr->arrow_y),this );
	selection->registerClickableObject( &(sr->arrow_z),this );
	this->ax = &(sr->arrow_x);
	this->ay = &(sr->arrow_y);
	this->az = &(sr->arrow_z);
//	this->myplane = new TCPlane(Vector4(1.0,0.0,0.0,1.0));
	this->done = false;
}

ArrowMovement::~ArrowMovement()
{

}

// Vector4 ArrowMovement::computePlane(Vector ray, Vector arw, Vector pos)
// {
// 
// 	Vector v1 = pos + ray;
// 	Vector v2 = pos + arw;
// 
// 	Vector n = v1 * v2;
// 	n.normalize();
// 
// 	float d = -(n.x * pos.x + n.y*pos.y + n.z * pos.z);
// 	Vector4 base_plane = Vector4(n.x, n.y, n.z ,d);
// 
// 	Vector n2 = n*arw;
// 	float d2 = -(n2.x * pos.x + n2.y*pos.y + n2.z * pos.z);
// 	//TCDEBUG << "ray" <<ray << " arw" << arw << " pos" << pos <<" v1" << v1 << " v2 " << v2 << " n2" << n2 << " d2" << d2;
// 	
// 	return Vector4( n2.x, n2.y, n2.z, d2 );
// 		
// }

void ArrowMovement::keyPress(Key) { } 
void ArrowMovement::keyRelease(Key ) { }
void ArrowMovement::mouseWheelEvent(GLint , Orientation ){ }
void ArrowMovement::mouseReleaseEvent(int , int , MouseButton ) { }

void ArrowMovement::mousePressEvent(GLint , GLint , MouseButton )
{
	original_position = selected->getPosition();
	Container::getInputContext()->changeHandler(selection);
}
void ArrowMovement::mouseTrackEvent(GLint , GLint , MouseButton )
{


	Vector pnear = renderer->getCamera()->getPosition();
	Vector pfar = renderer->getSceneCoordinate();
	Vector pointPlane = planePoint(pnear,pfar,currentPlane);
//	scene->deleteTemporary(pp);
//	pp = new TCPlanePoint();
//	pp->setPosition(pointPlane);
	
	// RESULT IS THE POINT ON THE PLANE INTERSECTING THE CAMERA RAY

	Vector diff = pointPlane - original_position;
	Vector result = arrow;
	
	GLfloat scalar = result.product_scalar(diff);
	result.scale(scalar);
	
	
	selected->setPosition(result+original_position);
}

void ArrowMovement::disable()
{
	done = false;
	scene->deleteTemporary(line);
	selected->setPosition(original_position);

	
}
void ArrowMovement::enable()
{
}

};