/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "navigationstate.h"

namespace TerrificCoder{

NavigationState::NavigationState() : InputState("NavigationState")
{
}

NavigationState::~NavigationState()
{
	
}

void NavigationState::mousePressEvent(GLint , GLint , MouseButton )
{
// 	GLRenderer *renderer = Container::getRenderer();
// 	if ( LeftButton == button ) {
// 		//		handleSelection(x,y);
// 	} else if ( RightButton == button ) {
// 		viewport_center[0] = renderer->getViewportSize().first;
// 		viewport_center[1] = renderer->getViewportSize().second;
// 		
// 		GLint delta_x = x-viewport_center[0];
// 		GLint delta_y = y-viewport_center[1];
// 		if ( delta_x && delta_y ) {
// 			renderer->viewportPadding(delta_x,-delta_y);
// 			
// 		}
// 		return;
// 	}
}
void NavigationState::mouseReleaseEvent(int , int , MouseButton)
{

}
void NavigationState::mouseTrackEvent(GLint , GLint, MouseButton )
{
// 		GLRenderer *renderer = Container::getRenderer();
// 	if ( LeftButton == button ) {
// 		//		handleSelection(x,y);
// 	} else if ( RightButton == button ) {
// 		viewport_center[0] = renderer->getViewportSize().first;
// 		viewport_center[1] = renderer->getViewportSize().second;
// 		
// 		GLint delta_x = x-viewport_center[0];
// 		GLint delta_y = y-viewport_center[1];
// 		if ( delta_x && delta_y ) {
// 			renderer->viewportPadding(delta_x,-delta_y);
// 			
// 		}
// 		return;
// 	}
}
void NavigationState::mouseWheelEvent(GLint , Orientation )
{
	
}
void NavigationState::keyPress(TerrificCoder::Key )
{

}
void NavigationState::keyRelease(TerrificCoder::Key )
{

}

};

