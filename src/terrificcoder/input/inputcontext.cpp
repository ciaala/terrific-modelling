/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "inputcontext.h"
#define TCLOGGING_LEVEL TCERR_LEVEL
#include "../library/logging.h"

namespace TerrificCoder
{

InputContext::InputContext ( GLRenderer *renderer, TCScene *scene ) : InputState ( "InputContext" )
{

    this->times = 0;
    this->navigation = new NavigationState();
    this->cameraRotation = new CameraRotation();
    this->selection = new Selection ( renderer,scene );
    this->selectionVolume = new SelectionVolume(this,renderer, scene);
	this->free = new Free();
    //      this->volumeCreation = new VolumeCreation ( renderer );
//		this->point = new Point(renderer);
    this->current = NULL;

/** Clickers */
	this->arrowMovement = new ArrowMovement(selection,renderer,scene );
	this->cameraReset = new CameraReset(selection,renderer);
	
    this->setupCurrent(selection);
    this->renderer = renderer;

	
	
}
InputContext::~InputContext()
{
    delete navigation;
    delete cameraRotation;
    delete selection;
    delete arrowMovement;
    delete selectionVolume;
}
void InputContext::mousePressEvent ( GLint x, GLint y, MouseButton button )
{
    this->setMouse ( x,y );
    if ( current ) {
        printContext();
        current->mousePressEvent ( x,y,button );
    }
}

void InputContext::mouseReleaseEvent ( int x, int y, MouseButton button )
{
    this->setMouse ( x,y );
    if ( current ) {
        printContext();
        current->mouseReleaseEvent ( x,y,button );
    }
}

void InputContext::mouseTrackEvent ( GLint x, GLint y, MouseButton button )
{
    this->setMouse ( x,y );
    if ( current ) {
        printContext();
        current->mouseTrackEvent ( x,y,button );
    }
}

void InputContext::mouseWheelEvent ( GLint delta, Orientation orientation )
{

    if ( current )  {
        printContext();
        current->mouseWheelEvent ( delta,orientation );
    }
}

void InputContext::printContext()
{

    times++;
    if ( times>200 ) {
        times = 0;
        TCDEBUG << "[InputContext] " << this->current->getName() << std::endl;
    }
}
void InputContext::update ( Subject*  )
{
//         TCObject *o = Container::getSelectedObject();
//         if ( o != NULL ) {
//                 switch ( o->getType() ) {
//                 case TerrificCoder::QuaternionCamera:
// 						this->setupCurrent(this->cameraRotation);
//                         break;
//                 default :
// 						this->setupCurrent(this->selection);
//                         break;
//                 }
//         }
}
void InputContext::keyPress ( Key key )
{
    if ( current )  {
        printContext();
        this->current->keyPress ( key );

        switch ( key ) {

        case ESC: {
            this->setupCurrent(this->selection);
            break;
        }
        case KEY_V: {
            this->setupCurrent(this->selectionVolume);
            break;
        }
        case KEY_R: {
            this->setupCurrent(this->cameraRotation);
            break;
        }
        case KEY_N: {
            this->setupCurrent(this->navigation);
            break;
        }
        case KEY_P: {
// 						this->setupCurrent(this->point);
            break;
        }
        default: {

        }
        }

    } else {

    }
}

void InputContext::setupCurrent ( InputState* next )
{
    if ( next != current ) {
        if ( current )
            current->disable();
        if ( next )
            next->enable();
        current = next;
    }
}

void InputContext::keyRelease ( Key key )
{
    if ( current ) {
        printContext();
        this->current->keyRelease ( key );
    }
}
void InputContext::setMouse ( int x, int y )
{
    this->mouse.x = x;
    this->mouse.y = y;
}

Vector InputContext::getMouseCoordinate()
{
    return mouse;
}

void InputContext::changeHandler ( InputState* handler )
{
    current->disable();
    handler->enable();
    current = handler;
}

void InputContext::changeHandler(InputHandler handler)
{
    InputState * next = NULL;
    switch (handler) {
    case InputRotation:
        next = this->cameraRotation;
        break;
    case InputSelectionVolume:
        next = this->selectionVolume;
        break;
    case InputFree:
		next = this->free;
        break;
    case InputSelect:
        next = this->selection;
        break;
    default :
        return;
    }
    changeHandler(next);
}


void InputContext::reset()
{
    if ( current )
        current->disable();
    current = selection;
    current->enable();
}

};
