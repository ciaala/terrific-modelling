/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "clickmanager.h"

namespace TerrificCoder{

ClickManager::ClickManager()
{

}

TCObject* TerrificCoder::ClickManager::contains ( GLuint id )
{
	return ids[id];
}

bool ClickManager::notifyClickable ( GLuint id, int x, int y, TerrificCoder::MouseButton button )
{
	TCObject *o = ids[id];
	if ( o != NULL ){
		typedef std::multimap< TCObject*, Clickable* >::iterator I;
		std::pair<I,I> its = handler.equal_range(o);
		for ( I it = its.first; it != its.second; it++ ) {
			Clickable* c = it->second;
			c->clicked(o,x,y,button);
		}
		return true;
	}
	return false;
}

void ClickManager::registerClickableObject ( TCObject* object, Clickable* clickable )
{
	handler.insert(std::make_pair(object,clickable));
	ids.insert(std::make_pair(object->getId(),object) );
}

void ClickManager::unregisterClickableObject ( TCObject* object, Clickable* clickable )
{
	std::multimap< TCObject*, Clickable* >::iterator it = handler.find(object);
	while ( it != handler.end() ){
		Clickable* c =  it->second;
		if ( c == clickable ){
			handler.erase(it);
			return;
		}
	}
}

};

