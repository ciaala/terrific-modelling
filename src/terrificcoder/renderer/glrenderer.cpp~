/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "glrenderer.h"
#include "../container.h"
#include <GL/gl.h>
#include <GL/glu.h>
#define TCLOGGING_LEVEL TCERR_LEVEL
#include "../library/logging.h"
#include "../qt/tcglwidget.h"

namespace TerrificCoder {

#define DEBUG_SELECTION 0
#define DEBUG_FEEDBACK 0


GLRenderer::GLRenderer() {
    this->scene = NULL;
    this->camera = NULL;
    this->overlay = new TCOverlay(this);

    this->lightsRenderer = new LightsRenderer(this);


    this->ortho2DRenderer = new Ortho2DRenderer(this);
    this->currentRenderer = lightsRenderer;
    this->worldCoordinate = new WorldCoordinate(this);


    optionMap[AXIS_X] = true;
    optionMap[AXIS_Y] = true;
    optionMap[AXIS_Z] = true;
    optionMap[PLANE_XY] = true;
    optionMap[PLANE_YZ] = true;
    optionMap[PLANE_ZX] = true;
    optionMap[OVERLAY] = true;
}

std::pair< int, int > GLRenderer::getViewportSize()
{
    return std::pair<int,int>(this->width,this->height);
}

GLRenderer::~GLRenderer()
{
    delete worldCoordinate;
    delete ortho2DRenderer;
    delete lightsRenderer;
    delete overlay;
}

void GLRenderer::setScene(TCScene* scene)
{
    this->scene = scene;
}

TCScene* GLRenderer::getScene()
{
    return this->scene;
}

void GLRenderer::viewportPadding(int x, int y)
{
    if (camera)  {
        camera->viewportPadding(x,y);
    }
}

void GLRenderer::resizeGL(int w, int h)
{
    this->width = w;
    this->height = h;
    currentRenderer->resize(w,h);
    overlay->resize(w,h);
    this->notify();
}

void GLRenderer::setCamera(TCCamera* camera)
{
    this->camera = camera;
    this->notify();
}

TCCamera* GLRenderer::getCamera()
{
    return this->camera;
}
Renderer* GLRenderer::getSceneRenderer()
{
    return this->currentRenderer;
}

void GLRenderer::zoom(GLint delta)
{
    if (camera ) {
        camera->zoom(delta);
    }
    this->notify();
}

void GLRenderer::draw(Rendering type)
{
    if ( camera != NULL ) {
        //	currentRenderer->resize(width,height);
        currentRenderer->modelview();
        currentRenderer->draw(type);
        currentRenderer->draw_disabled(type);

    }
}
void GLRenderer::draw_overlay(Rendering type)
{
    if ( (camera != NULL) && (optionMap[OVERLAY]) ) {
        
        overlay->modelview();
        overlay->draw(type);
    }
}

void GLRenderer::paintGL()
{
    TCDEBUG << "Paint GL";
    /**/

    if ( camera != NULL && !DEBUG_SELECTION && !DEBUG_FEEDBACK) {
        this->currentRenderer->enableFeatures();
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        camera->perspective();
        this->draw(RENDERER_PAINT);

        //this->renderer();
        mouse = Container::getInputContext()->getMouseCoordinate();
        sceneUnderMouse = worldCoordinate->worldUnderMouse(mouse.x,mouse.y);
        this->currentRenderer->disableFeatures();
        this->drawPlanes();
        this->drawAxis();
// 		this->currentRenderer->enableFeatures();
// 		this->currentRenderer->draw();
// 		this->currentRenderer->draw();
        ortho2DRenderer->resize(width,height);
        ortho2DRenderer->modelview();
        ortho2DRenderer->draw(RENDERER_PAINT);
		overlay->resize(width,height);
        this->draw_overlay(RENDERER_PAINT);
    }
    
    if (DEBUG_SELECTION) {
		//selection_scene(0,0);
		selection_overlay(0,0);
    } else if (DEBUG_FEEDBACK) {
        feedback();
    }
}


int GLRenderer::selection_scene(int x, int y) {
	if ( camera ) {
        GLint viewport[4] = {0,0,width,height};

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

		if ( !DEBUG_SELECTION ) {
            glRenderMode(GL_SELECT);
			gluPickMatrix(x,height-y,3,3,viewport);
        }
        camera->perspective();
        glInitNames();
        glPushName(-1);

		if (  !DEBUG_SELECTION  ) {
            draw(RENDERER_SELECTION);
            return glRenderMode(GL_RENDER);

        } else {
            this->draw(RENDERER_PAINT);
            return -1;
        }
    } else {
        return -1;
    }
}

int GLRenderer::selection_overlay(int x, int y) {
	if ( camera ) {
		GLint viewport[4] = {0,0,width,height};
		//GLint *viewport = overlay->getViewport();
		
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		
		if ( !DEBUG_SELECTION ) {
			glRenderMode(GL_SELECT);
			//gluPickMatrix(x,height-y,3,3,viewport);
			gluPickMatrix(x,viewport[3]-y,3,3,viewport);
		}
		
		overlay->ortho();

		
		glInitNames();
		glPushName(-1);
				
		if ( !DEBUG_SELECTION ) {
            draw_overlay(RENDERER_SELECTION);
			return glRenderMode(GL_RENDER);
		} else {
            this->draw_overlay(RENDERER_PAINT);
			return -1;
		}
	} else {
		return -1;
	}
}




int GLRenderer::feedback() {
    if (camera !=NULL ) {
        if (!DEBUG_FEEDBACK) {
            (void) glRenderMode(GL_FEEDBACK);
        }
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        camera->perspective();
        if (!DEBUG_FEEDBACK) {
            draw(RENDERER_FEEDBACK);
            //draw_overlay(RENDERER_FEEDBACK);
            return glRenderMode(GL_RENDER);
        } else {
            this->draw(RENDERER_PAINT);
            return -1;
        }
    } else {
        return -1;
    }
}

// void GLRenderer::renderer() {
// 	if ( camera != NULL ) {
//
// 	}
//
// }

void GLRenderer::initializeGL()
{
    currentRenderer->setup();
}



Vector GLRenderer::getSceneCoordinate()
{
    return sceneUnderMouse;
}

void GLRenderer::drawAxis()
{

    GLdouble length = 10000.0f;
    GLdouble zero[3] = {0.0,0.0,0.0};
    GLdouble x[3] = {length,0.0,0.0};
    GLdouble y[3] = {0.0,length,0.0};
    GLdouble z[3] = {0.0,0.0,length};
    glLineWidth(2.0);

    glPushMatrix();

    if ( optionMap[AXIS_X] ) {
        glBegin(GL_LINES);
        glColor4f(1.0f,0.0f,0.0f,1.0);
        glVertex3dv(zero);
        glColor4f(0.0f,0.0f,0.0f,1.0);
        glVertex3dv(x);
        glEnd();
    }
    if ( optionMap[AXIS_Y] ) {
        glBegin(GL_LINES);
        glColor4f(0.0f,1.0f,0.0f,1.0);
        glVertex3dv(zero);
        glColor4f(0.0f,0.0f,0.0f,1.0);
        glVertex3dv(y);
        glEnd();
    }
    if ( optionMap[AXIS_Z] ) {
        glBegin(GL_LINES);
        glColor4f(0.0f,0.0f,1.0f,1.0);
        glVertex3dv(zero);
        glColor4f(0.0f,0.0f,0.0f,1.0);
        glVertex3dv(z);
        glEnd();
    }
    glPopMatrix();

}

void GLRenderer::drawPlanes()
{
    glLineWidth(1.0);

    if ( optionMap[PLANE_XY ] ) {
        glColor4f(0.2f,0.2f,0.0f,0.3);
        drawPlane();
    }
    if ( optionMap[PLANE_YZ] ) {
        glPushMatrix();
        glRotatef(90.0,0.0,1.0,0.0);
        glColor4f(0.0f,0.2f,0.2f,0.3);
        drawPlane();
        glPopMatrix();
    }
    if ( optionMap[PLANE_ZX] ) {
        glPushMatrix();
        glRotatef(90.0,1.0,0.0,0.0);
        glColor4f(0.2f,0.0f,0.2f,0.3);
        drawPlane();
        glPopMatrix();
    }

}

void GLRenderer::drawPlane()
{
    GLint tess = 100;
    GLint size = 100;

    GLdouble p1[3];
    GLdouble p2[3];
    GLdouble inc = tess*size;

    p1[0] = -inc;
    p1[1] = -inc;
    p1[2] = 0;

    p2[0] = inc;
    p2[1] = inc;
    p2[2] = 0;



    for (int i=-tess+1; i <= tess; i++) {
        inc = i*size;
        Vector a(p1[0], inc, 0);
        Vector b(p2[0], inc, 0);
        Vector c(inc, p2[1], 0);
        Vector d(inc, p1[1], 0);

        glBegin(GL_LINE_LOOP);
        glVertex3dv(p1);
        glVertex3dv( a.v);
        glVertex3dv( b.v);
        glVertex3dv(p2);
        glVertex3dv( c.v);
        glVertex3dv( d.v);
        glEnd( );
    }
}

void GLRenderer::switchOption(TerrificCoder::RenderFlag flag)
{
    bool value = optionMap[flag];
    optionMap[flag] =  value ? false : true;
}

TCOverlay* GLRenderer::getOverlay()
{
    return overlay;
}


void GLRenderer::setMouseAt(int x, int y)
{

}
void GLRenderer::setMouseAtCentre()
{
	Container::getWidget()->setMouseAt(width/2,height/2 );
}

};
