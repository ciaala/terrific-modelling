/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "ortho2drenderer.h"
#include "glrenderer.h"

namespace TerrificCoder {
Ortho2DRenderer::Ortho2DRenderer(GLRenderer* renderer): Renderer(renderer)
{
}

void Ortho2DRenderer::setup()
{
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glLineWidth(2.0);
}

void Ortho2DRenderer::resize(int width, int height)
{
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glLineWidth(2.0);
	height = height ? height : 1;
	glViewport(0,0,width,height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	glOrtho(0,width,0,height,1.0, -1.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void Ortho2DRenderer::draw(Rendering ){
// 	std::clog << "[ORTHO] ";
	TCScene* scene = glRenderer->getScene();
	SceneIterator *iterator = scene->iterator();
	while (iterator->hasMore() ) {
		
		TCObject *object = iterator->next();
		if ( object->isSelected() ){
			TCRectangle *rect = &object->getHighlightBox();
			if ( rect->isValid() ) {
				glColor4f(1.0,0.2,1.0,0.85);
				this->drawHighlightBox(rect);
			}
		} else if ( object->isHighlighted()){
			TCRectangle *rect = &object->getHighlightBox();
			if ( rect->isValid() ) {
				glColor4f(1.0,0.6,0.6,0.85);
				this->drawHighlightBox(rect);
			}
		}
	}
	delete iterator;
// 	std::clog << std::endl;
}
void Ortho2DRenderer::modelview(){

	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	this->draw(RENDERER_PAINT);

}

void Ortho2DRenderer::drawHighlightBox(TCRectangle *rect)
{
	
	
//   		std::clog << (*rect);
// 		glRectf(rect->left,rect->bottom,rect->right,rect->top);
	glBegin(GL_LINE_LOOP);
	glVertex2f(rect->left,rect->top);
	glVertex2f(rect->left,rect->bottom);
	glVertex2f(rect->right,rect->bottom);
	glVertex2f(rect->right,rect->top);
	glEnd();
	
}
void Ortho2DRenderer::disableFeatures()
{
	glDisable(GL_LIGHTING);
}

void Ortho2DRenderer::enableFeatures()
{
	glEnable(GL_LIGHTING);
}


// void Ortho2DRenderer::draw(std::list< TerrificCoder::TCRectangle* > *rectangles )
// {
// 	std::list< TCRectangle* >::iterator i = rectangles->begin();
// 	while ( i != rectangles->end() ) {
// 		TCRectangle *rect = (*i);
// 		glDisable(GL_LIGHTING);
// 		glColor3f(1.0,0.5,0.5);
// // 		std::clog << (*rect);
// // 		glRectf(rect->left,rect->bottom,rect->right,rect->top);
// 		glBegin(GL_LINE_LOOP);
// 		glVertex2f(rect->left,rect->top);
// 		glVertex2f(rect->left,rect->bottom);
// 		glVertex2f(rect->right,rect->bottom);
// 		glVertex2f(rect->right,rect->top);
// 		glEnd();
// 		glEnable(GL_LIGHTING);
// 		i++;
// 	}
// }
// void Ortho2DRenderer::render(std::list<TCRectangle*> *rectangles ){
// 	std::pair< int, int > vp = glRenderer->getViewportSize();
// 	this->resize(vp.first,vp.second);
// 	this->draw(rectangles);
// }


};

