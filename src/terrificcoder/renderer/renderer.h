/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_RENDERER_H
#define TERRIFICCODER_RENDERER_H
#include "../scene/selectionbox.h"
#include "../scene/tcscene.h"
#include "../model/tclight.h"

namespace TerrificCoder {
class GLRenderer;

class LightsRenderer;


	typedef enum Rendering {
		RENDERER_SELECTION,
		RENDERER_FEEDBACK,
		RENDERER_PAINT,
	} Rendering;
	

class Renderer {
private:
	Renderer();
protected:
	int width, height;
	GLRenderer *glRenderer;
	void setup_object_matrix(TCObject *object,Rendering type);
	void draw_object(TCObject* object, Rendering type);
public:
	Renderer(GLRenderer *renderer);
	virtual void draw(Rendering type)=0;
	virtual void draw_disabled(Rendering type);
	
	virtual void modelview()=0;
	virtual void resize(int width,int height)=0;
	virtual void setup()=0;
	virtual ~Renderer();
    virtual void disableFeatures()=0;
	virtual void enableFeatures()=0;
	

public:
	TCArrow arrow_x;
	TCArrow arrow_y;
	TCArrow arrow_z;
};
	
// class BasicRenderer : public TerrificCoder::Renderer {
// public:
// 	BasicRenderer(GLRenderer* renderer);
// 	virtual ~BasicRenderer();
// 	virtual void draw();
// 	virtual void render();
// 	virtual void resize(int width,int height);
// 	virtual void setup();
// 	
// };
// 	
// class WireframeRenderer : public TerrificCoder::Renderer { 
// public:
// 	WireframeRenderer(GLRenderer* renderer);
// 	virtual ~WireframeRenderer();
// 	virtual void draw();
// 	virtual void render();
// 	virtual void resize(int width,int height);
// 	virtual void setup();
// };
	
}

#endif // TERRIFICCODER_RENDERER_H
