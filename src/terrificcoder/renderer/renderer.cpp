/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "renderer.h"
#include "glrenderer.h"
#include "../container.h"
#include "../scene/selectionbox.h"
namespace TerrificCoder{
void Renderer::setup_object_matrix(TCObject *object,Rendering type)
{

	Vector p = object->getPosition();
	glTranslated(p.x,p.y,p.z);
	GLdouble* matrix = object->getRotationMatrix();
	glMultMatrixd(matrix);
	glColor4dv(object->getColor());

	if ( type == RENDERER_SELECTION )
		glLoadName(object->getId());
	if ( type == RENDERER_FEEDBACK )
		glPassThrough((GLfloat)object->getId());
}
void Renderer::draw_object(TCObject* object,Rendering type) {

	setup_object_matrix(object,type);
	object->draw();
}

Renderer::Renderer(GLRenderer* renderer)
{
	this->glRenderer = renderer;

	arrow_x.setColor(192,0,0,192);
	arrow_x.setRotation(Vector(0,0,-M_PI/2.0));
	
	arrow_y.setColor(0,192,0,192);
	arrow_y.setRotation(Vector(M_PI,0,0));
	
	arrow_z.setColor(0,0,192,192);
	arrow_z.setRotation(Vector(M_PI/2.0,0,0));
}

Renderer::Renderer()
{

}

Renderer::~Renderer()
{
	
}

void Renderer::draw_disabled(Rendering )
{
	
}
/*
BasicRenderer::BasicRenderer(GLRenderer* renderer): Renderer(renderer)
{
	
}
BasicRenderer::~BasicRenderer()
{
	
}

void BasicRenderer::render()
{
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	TCCamera *camera = glRenderer->getCamera();
	if (camera)
		camera->lookat();
	this->draw();
	//     glClear(GL_COLOR_BUFFER_BIT);
	// 	glColor3f(1.0f,1.0f,0.0f);
	// 	glRectf(-1000,-1000,1000,1000);
	//     TCPlane *p1 = new TCPlane(
	//         Vector(1000,0,1000 ),
	//         Vector(1000,0,-1000),
	//         Vector(-1000,0,-1000),
	//         Vector(-1000,0,1000),
	//         Vector4(1.0,1.0,0.0,1.0)
	//
	//     );
	//     TCPlane *p2 = new TCPlane(
	//         Vector(1000,1000,0),
	//         Vector(1000,-1000,0),
	//         Vector(-1000,-1000,0),
	//         Vector(-1000,1000,0),
	//         Vector4(1.0,0.0,1.0,1.0)
	//
	//     );
	//     TCPlane *p3 = new TCPlane(
	//         Vector(0,1000,1000),
	//         Vector(0,1000,-1000),
	//         Vector(0,-1000,-1000),
	//         Vector(0,-1000,1000),
	//         Vector4(0.0,0.0,1.0,1.0)
	//
	//     );
	//     p1->draw();
	//     p2->draw();
	//     p3->draw();
	//std::clog << "}"<< std::endl;
}

void BasicRenderer::resize(int width, int height)
{
	height = height ? height : 1;
	
	glViewport(0,0,width,height);
	
	//     glMatrixMode(GL_PROJECTION);
	//     glLoadIdentity();
	//
	//     TCCamera *camera = glRenderer->getCamera();
	//     if ( camera )
	//         camera->perspective();
	//     glViewport(0,0,width,height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-width,width,-height,height,1000.0, -1000.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
}

void BasicRenderer::setup()
{
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glLineWidth(2.0);
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	
}
void BasicRenderer::draw()
{
	TCScene* scene = glRenderer->getScene();
	SceneIterator iterator = scene->iterator();
	
	while (iterator.hasMore() ) {
		glPushMatrix();
		TCObject *object = iterator.next();
		draw_object(object);
		glPopMatrix();
	}
}








WireframeRenderer::WireframeRenderer(GLRenderer* renderer): Renderer(renderer)
{
	
}
WireframeRenderer::~WireframeRenderer()
{
	
}

void WireframeRenderer::render()
{
	glClear(GL_COLOR_BUFFER_BIT);
	// 	glColor3f(1.0f,1.0f,0.0f);
	// 	glRectf(-1000,-1000,1000,1000);
	TCPlane *p1 = new TCPlane(
		Vector(1000,0,1000 ),
							  Vector(1000,0,-1000),
							  Vector(-1000,0,-1000),
							  Vector(-1000,0,1000),
							  Vector4(1.0,1.0,0.0,1.0)
							  
	);
	TCPlane *p2 = new TCPlane(
		Vector(1000,1000,0),
							  Vector(1000,-1000,0),
							  Vector(-1000,-1000,0),
							  Vector(-1000,1000,0),
							  Vector4(1.0,0.0,1.0,1.0)
							  
	);
	TCPlane *p3 = new TCPlane(
		Vector(0,1000,1000),
							  Vector(0,1000,-1000),
							  Vector(0,-1000,-1000),
							  Vector(0,-1000,1000),
							  Vector4(0.0,0.0,1.0,1.0)
							  
	);
	p1->draw();
	p2->draw();
	p3->draw();
}
void WireframeRenderer::resize(int width, int height)
{
	glViewport(0,0,width,height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-width,width,-height,height,1000.0, -1000.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
void WireframeRenderer::setup()
{
	
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	glPolygonMode(GL_BACK,GL_FILL);
	
}
void WireframeRenderer::draw()
{

}*/


};