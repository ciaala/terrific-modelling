/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GLRENDERER_H
#define GLRENDERER_H

#include "renderer.h"
#include "feedback.h"
#include "raytracing.h"
#include "lightsrenderer.h"
#include "ortho2drenderer.h"
#include "tcoverlay.h"

#include "../scene/tcscene.h"
#include "../model/tclight.h"
#include "../patterns/observer.h"
#include "../command/renderoption.h"
#include <utility>

namespace TerrificCoder {
class GLFeedback;
class WorldCoordinate;

class GLRenderer : public TerrificCoder::Subject
{
private:
    int width;
    int height;
	float aspect;
	Vector sceneUnderMouse;
	TCOverlay *overlay;
	TCScene *scene;
	TCCamera *camera;
	std::list<TCRectangle*> selection_boxes;
	
	Renderer *currentRenderer;
//	BasicRenderer *basicRenderer;
	LightsRenderer *lightsRenderer;
//	WireframeRenderer *wireframeRenderer;
	Ortho2DRenderer *ortho2DRenderer;

	WorldCoordinate *worldCoordinate;
    Vector mouse;
	std::map<TerrificCoder::RenderFlag,bool> optionMap;
	
private:
	void drawAxis();
	void drawPlanes();
	void drawPlane();
	void draw(Rendering type);
	

	//void renderer();
public:    
	GLRenderer();
    ~GLRenderer();
	
	void setScene(TCScene *scene);
    TCScene* getScene();
	
	void setCamera(TCCamera *camera);
	TCCamera *getCamera();

	
	Vector getSceneCoordinate();
	
	
    std::pair<int,int> getViewportSize();
    void viewportPadding(int x, int y);
    void initializeGL();
	void resizeGL(int w, int h);
	void zoom(GLint delta);
	void paintGL();

	
	int selection_scene(int x, int y);
	int selection_overlay(int x, int y);
	int feedback();

	Renderer* getSceneRenderer();
    void switchOption(TerrificCoder::RenderFlag flag);
	
    TCOverlay* getOverlay();
    void draw_overlay(Rendering type);
    void setMouseAt(int x, int y);
	void setMouseAtCentre();
};

};

#endif // GLRENDER_H
