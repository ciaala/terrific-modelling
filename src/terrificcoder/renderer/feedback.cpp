/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "feedback.h"
#include "../patterns/changemanager.h"
#include "../container.h"
#define TCLOGGING_LEVEL TCERR_LEVEL
#include "../library/logging.h"

namespace TerrificCoder {

GLFeedback::GLFeedback(GLRenderer *renderer) {
	this->feedbackSize = 1638400;
	this->feedbackBuffer = new GLfloat[feedbackSize];
	

    this->renderer = renderer;
	renderer->attach(this);
	
}
GLFeedback::~GLFeedback()
{
	delete feedbackBuffer;
}

void GLFeedback::setScene(TCScene *scene)
{
    this->scene = scene;
    scene->attach(this);
    Container::attachSelectedCamera(this);
    Container::attachSelectedObject(this);

}

void GLFeedback::update(TerrificCoder::Subject* )
{
	//scene->resetHightlightBox();
	feedback2d();
}

void GLFeedback::feedback2d()
{

    if ( renderer->getCamera() != NULL )  {

        std::list<int> ids;
       
		glFeedbackBuffer(feedbackSize, GL_2D, feedbackBuffer);
		int size =  renderer->feedback();
		if ( size > 0 ) {
			identifyObjects(size, feedbackBuffer, &ids);
		//	printIdList(&ids);
		} else if ( size == -1 ){
			delete[] feedbackBuffer;
			feedbackSize *= 2;
			feedbackBuffer = new GLfloat[feedbackSize];
			TCERR << "Feedback Buffer increased to "<< feedbackSize << std::endl;
			feedback2d();
			return;
		}
		glerror(__FILE__,__LINE__);
		TCDEBUG << "[Feedback 2D]";
    }
}


void GLFeedback::identifyObjects(int size,GLfloat *feedbackBuffer, std::list<int> *ids )
{
    int i = 0;
    TCDEBUG << "\n[FEEDBACK] " << size;
    while ( i < size ) {
        if (feedbackBuffer[i] == GL_PASS_THROUGH_TOKEN) {
            i++;
            GLuint id = feedbackBuffer[i];
            TCDEBUG << "id: " << id;
            i = parseFeedbackBuffer(size, ++i, feedbackBuffer, id);
            if ( ids != NULL)
                ids->push_back(id);
        } else {
			TCERR << "Not a pass-trough token ["<<i<<"] = " << feedbackBuffer[i]<<std::endl;
		}
        i++;
		TCDEBUG << std::endl ;
    }
}


// int GLFeedback::fillFeedBackBuffer(GLsizei size, GLfloat *feedbackBuffer, GLenum type) {
// 
// 
// //     glMatrixMode(GL_PROJECTION);
// //     glLoadIdentity();
// //     renderer->getCamera()->perspective();
// //     glMatrixMode(GL_MODELVIEW);
// //     glLoadIdentity();
// //     renderer->getCamera()->lookat();
// //     renderer->draw();
// }

int GLFeedback::parseFeedbackBuffer(int size, int i, GLfloat* feedbackBuffer,GLuint id) {

    TCScene *scene = this->renderer->getScene();
    TCRectangle r;
    TCDEBUG << std::endl << "\t\tVertices";
    while ( i < size && feedbackBuffer[i] != GL_PASS_THROUGH_TOKEN ) {
        if ( feedbackBuffer[i] == GL_POLYGON_TOKEN) {
            i++;
            i = this->feedbackPolygon(i, feedbackBuffer, &r);
            TCObject* object = scene->getObject(id);
            if ( object )
				object->setHighlightBox(r);

        }
        i++;
    }

    return --i;
}


int GLFeedback::feedbackPolygon(int i, GLfloat *feedbackBuffer, TCRectangle *r ) {
    GLint count = (GLint) feedbackBuffer[i];
    i++;

    for (int j = 0; j < count; j++,i++)
    {
        GLfloat value = feedbackBuffer[i];
        TCDEBUG << "("<< value;
        r->right = testGreaterNAN(r->right,value+5);
        r->left = testLesserNAN(r->left, value-5);
        value = feedbackBuffer[++i];
        TCDEBUG <<","<< value <<") ";
        r->bottom = testGreaterNAN( r->bottom, value+5);
        r->top = testLesserNAN(r->top, value-5);

    }
    return i;
}

GLfloat GLFeedback::testGreaterNAN(GLfloat tested, GLfloat value) {
    if ( tested == NAN )
        return value;
    else if ( tested > value )
        return tested;
    else
        return value;
}

GLfloat GLFeedback::testLesserNAN(GLfloat tested, GLfloat value) {
    if ( tested == NAN )
        return value;
    else if ( tested < value )
        return tested;
    else
        return value;
}


void GLFeedback::printIdList(std::list< int >* ids)
{

	TCDEBUG << "\tIDS {";
    for ( std::list< int >::iterator it = ids->begin(); it != ids->end(); it++ ) {
		TCRectangle &rect = scene->getObject((*it))->getHighlightBox();
        TCDEBUG << *it <<" "<< rect <<", ";
		//std::cout << *it <<" "<< rect <<", ";
    }
    TCDEBUG << "}";
}


};
