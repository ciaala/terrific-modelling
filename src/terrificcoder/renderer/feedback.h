/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_FEEDBACK_H
#define TERRIFICCODER_FEEDBACK_H

#include "glrenderer.h"
#include "../patterns/observer.h"
#include "../library/tclibrary.h"

namespace TerrificCoder {

class GLFeedback : public TerrificCoder::Observer
{
	
protected:
	GLRenderer* renderer;
	TCScene *scene;
	virtual int feedbackPolygon( int i, GLfloat *feedbackBuffer, TCRectangle *r);
	virtual int parseFeedbackBuffer(int size, int i, GLfloat* feedbackBuffer, GLuint id );
	int fillFeedBackBuffer(GLsizei size, GLfloat *feedbackBuffer, GLenum type);
	void identifyObjects(int size,GLfloat *feedbackBuffer, std::list<int> *ids );
	void printIdList( std::list<int> *ids );


	
	GLfloat testGreaterNAN(GLfloat tested, GLfloat value );
	GLfloat testLesserNAN(GLfloat tested, GLfloat value);
public:
	GLFeedback(GLRenderer *renderer);
    virtual ~GLFeedback();
    virtual void update(TerrificCoder::Subject* subject);
	virtual void feedback2d();
    virtual void setScene(TCScene *scene);
    GLfloat* feedbackBuffer;
    int feedbackSize;
	
};

};

#endif // TERRIFICCODER_FEEDBACK_H
