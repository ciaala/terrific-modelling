/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_LIGHTSRENDERER_H
#define TERRIFICCODER_LIGHTSRENDERER_H

#include "renderer.h"
#include "glrenderer.h"
#include "../model/tctext.h"


namespace TerrificCoder {

class LightsRenderer : public TerrificCoder::Renderer {


protected:
	virtual void draw_object(TCObject *object,Rendering type);
	virtual void draw_light(TCLight *light,Rendering type);
	virtual void draw_text2D(TCText2D *text,Rendering type);
	//virtual void draw_texture(TCObject* object);
	virtual void draw_material (Material *material);

	
	virtual void drawLights(SceneIterator *iterator,Rendering type);
	virtual void drawObjects(SceneIterator *iterator,Rendering type);
	virtual void drawDisabledObjects(SceneIterator *iterator,Rendering type);
	virtual void drawDisabledSupports(SceneIterator *iterator,Rendering type);
	
public:	
	LightsRenderer(GLRenderer* renderer);
	virtual ~LightsRenderer();
	virtual void draw(Rendering type);
	virtual void draw_disabled(Rendering type);
	virtual void modelview();
	virtual void resize(int width,int height);
	virtual void setup();
    virtual void disableFeatures();
	virtual void enableFeatures();
private:
	void draw_object_placers(Vector position);
// 	void setup_object_matrix(TCObject* object,Rendering type);
    void draw_move_arrow();
    void draw_arrow(TCArrow &arrow);

};

}

#endif // TERRIFICCODER_LIGHTSRENDERER_H
