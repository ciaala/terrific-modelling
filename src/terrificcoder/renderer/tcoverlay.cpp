/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <iostream>
#include <GL/gl.h>
#include <GL/glu.h>
#include "tcoverlay.h"
#include "malloc.h"
#include "../container.h"
#include <GL/freeglut.h>
#include "../input/camerareset.h"

namespace TerrificCoder {

	
TCOverlay::TCOverlay(GLRenderer* renderer) : Renderer(renderer)
{
	box.setWidth(12);
	GLdouble d = 16;

	setup_head(&p_x, Vector4(192,0,0,255),		Vector(0,0,M_PI/2.0),	Vector(0,0,-d) );
	setup_head(&m_x, Vector4(196,192,192,255),	Vector(0,0,-M_PI/2.0),	Vector(0,0,-d) );
	setup_head(&p_y, Vector4(0,192,0,255),		Vector(0,0,0),			Vector(0,0,-d) );
	setup_head(&m_y, Vector4(192,196,192,255),	Vector(-M_PI,0,0),		Vector(0,0,-d) );
	setup_head(&p_z, Vector4(0,0,192,255),		Vector(-M_PI/2.0,0,0),	Vector(0,0,-d) );
	setup_head(&m_z, Vector4(192,192,196,255),	Vector(M_PI/2.0,0,0),	Vector(0,0,-d) );
}
TCArrowHead** TCOverlay::getHeads()
{
	return heads;
}


void TCOverlay::setup_head(TCArrowHead *head,Vector4 color, Vector rotation, Vector position )
{
	static int i=0;
	head->setColor(color.v[0],color.v[1],color.v[2],color.v[3]);
	head->setRotation(rotation);
	head->setPosition(position);
	head->setWidth(4.0);
	head->setHeight(8.0);
	heads[i] = head;
	i++;
}

TCOverlay::~TCOverlay()
{
	
}

void TCOverlay::draw(Rendering type)
{
	for (int i=0; i < 6; i++ ) {
		this->draw_head(heads[i]);
	}
//	draw_xy();
	draw_object(&box,type);
}
void TCOverlay::draw_xy()
{
	GLfloat d = 10.0;
	glColor4f(0.0f,0.0f,0.7f,0.8f);
	glBegin(GL_LINE_LOOP);
	
	glVertex3f(-d,-d,0);
	glVertex3f(d,-d,0);
	glVertex3f(d,d,0);
	glVertex3f(-d,d,0);
	glEnd();

}

void TCOverlay::draw_head(TCArrowHead* head) {
	glPushMatrix();
	GLdouble * m = head->getRotationMatrix();
	glMultMatrixd(m);
	glLoadName(head->getId());
	glColor4dv(head->getColor());
	head->draw();
	glPopMatrix();
}

void TCOverlay::resize(int width, int height){
// 	std::pair< int, int > size = Container::getRenderer()->getViewportSize();
	this->setup();
	this->width = width;
	this->height = height;
	this->aspect = this->width/this->height;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	ortho();
}
void TCOverlay::disableFeatures()
{
	glEnable(GL_LIGHTING);
}
void TCOverlay::enableFeatures()
{
	
	glDisable(GL_LIGHTING);
}
void TCOverlay::setup()
{
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);

	glDepthRange(-2000.0,2000.0);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	
	glShadeModel(GL_SMOOTH);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glPolygonMode(GL_FRONT_AND_BACK ,GL_FILL);
	
	glFrontFace(GL_CCW);
	glEnable(GL_CULL_FACE);
}

void TCOverlay::modelview()
{
	TCCamera *camera = glRenderer->getCamera();
	if (camera){
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		camera->overlayLookat();
	}
}
GLint* TCOverlay::getViewport()
{
	return viewport;
}

void TCOverlay::ortho()
{
	double w = width/4;
	double h = w/aspect;
	double off_w = w / 16;
	double off_h = h / 16;
	glOrtho(-w,2*off_w,-h,2*off_h,100,-100);
	viewport[0] = -w;
	viewport[1] = 2*off_w;
	viewport[2] = -h;
	viewport[3] = 2*off_h;
}


};