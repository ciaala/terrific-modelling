/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_ORTHO2DRENDERER_H
#define TERRIFICCODER_ORTHO2DRENDERER_H

#include "renderer.h"


namespace TerrificCoder {

class Ortho2DRenderer : public TerrificCoder::Renderer
{
	
virtual void drawHighlightBox(TCRectangle* rect);
public:
    Ortho2DRenderer(GLRenderer* renderer);
    virtual void setup();
    virtual void resize(int width, int height);
    virtual void modelview();
    virtual void draw(Rendering type);
    virtual void disableFeatures();
    virtual void enableFeatures();


//	virtual void draw(std::list<TCRectangle*> *rectangles );
//	virtual void render(std::list<TCRectangle*> *rectangles );
	
};

};

#endif // TERRIFICCODER_ORTHO2DRENDERER_H
