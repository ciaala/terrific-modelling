/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "lightsrenderer.h"
#include "../container.h"
#include "../model/material.h"

namespace TerrificCoder{

LightsRenderer::LightsRenderer(GLRenderer* renderer): Renderer(renderer)
{


}
LightsRenderer::~LightsRenderer()
{
	
}


void LightsRenderer::draw_object(TCObject* object,Rendering type)
{
	setup_object_matrix(object,type);
	
	if ( type == RENDERER_PAINT )
		draw_material(object);
	
	if ( object->hasTexture() && type == RENDERER_PAINT) {
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,object->getTextureID());
		object->draw();
		glDisable(GL_TEXTURE_2D);
	} else {
		object->draw();
	}
}

void LightsRenderer::draw_material(Material *material)
{
	std::pair<materialIt,materialIt > iterators = material->getMaterials();
	for ( materialIt it = iterators.first; it != iterators.second; it++ ) {
		GLenum type = (*it).first;
		Vector4f *v = (*it).second;
		glMaterialfv(GL_FRONT, type, v->v);
	}
}

void LightsRenderer::draw_light(TCLight* light,Rendering type)
{

	glLightfv(light->getNumber(), GL_POSITION, light->getLightPosition());
	glLightfv(light->getNumber(), GL_AMBIENT, light->getAmbient());
	glLightfv(light->getNumber(), GL_DIFFUSE, light->getDiffuse());
	glLightfv(light->getNumber(), GL_SPECULAR, light->getSpecular());
	glLightfv(light->getNumber(), GL_SPOT_DIRECTION, light->getSpotDirection());
	glLightf(light->getNumber(), GL_SPOT_CUTOFF, light->getSpotCutoff());
	glLightf(light->getNumber(), GL_SPOT_EXPONENT, light->getSpotExponent());
	
	glEnable(light->getNumber());
}


void LightsRenderer::draw_text2D(TerrificCoder::TCText2D* text, TerrificCoder::Rendering type)
{
	Vector p = text->getPosition();
	glRasterPos3dv(p.v);
	if ( type == RENDERER_SELECTION )
		glLoadName(text->getId());
	if ( type == RENDERER_FEEDBACK )
		glPassThrough((GLfloat)text->getId());
	glColor4dv(text->getColor());
	text->draw();
}


void LightsRenderer::modelview()
{
	TCCamera *camera = glRenderer->getCamera();
	if (camera){

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		camera->lookat();
	}
	
}
/**
 * Update Viewport
 * Reload Projection Matrix
 * Reload the Perspective Transformation
 */
void LightsRenderer::resize(int width, int height)
{
	TCCamera *camera = glRenderer->getCamera();
	if ( camera ) {
		height = height ? height : 1;
		glViewport(0,0,width,height);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		camera->viewportResize(width,height);
		camera->perspective();
	}
}
void LightsRenderer::setup()
{
	glClearColor(0.07f,0.07f,0.07f,0.0f);
	
	glEnable(GL_DEPTH_TEST);
	glDepthRange(-2000.0,2000.0);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	glShadeModel(GL_SMOOTH);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glPolygonMode(GL_FRONT_AND_BACK ,GL_FILL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_LIGHTING);
	
	glFrontFace(GL_CCW);
	glEnable(GL_CULL_FACE);
	
	
	glLineWidth(1.0);
	
}


void LightsRenderer::draw(Rendering type)
{

	TCScene* scene = glRenderer->getScene();
	SceneIterator *it = scene->lights();
	drawLights(it,type);
	delete it;
	it = scene->iterator();
	drawObjects(it,type);
	delete it;
	if ( type == RENDERER_PAINT) {
		it = scene->supports();
		drawObjects(it,type);
		delete it;
	}
	it = scene->temporaries();
	drawObjects(it,type);
	delete it;
}

void LightsRenderer::drawLights(SceneIterator *iterator, Rendering type)
{
	while (iterator->hasMore() ) {
		glPushMatrix();
		TCObject *object = iterator->next();
		this->draw_light(static_cast<TCLight*>(object),type);
		glPopMatrix();
	}
}

void LightsRenderer::drawObjects(SceneIterator *iterator, Rendering type)
{
 	while (iterator->hasMore() ) {
		glPushMatrix();
		TCObject *object = iterator->next();
		switch ( object->getType() ) {
// 			case Light: {
// 				this->draw_light(static_cast<TCLight*>(object),type);
// 				this->draw_object(object,type);
// 				break;
// 			}
			case Text2D: {
				this->draw_text2D((TCText2D*)object,type);
				break;
			}
			default: {
				this->draw_object(object,type);
			}
		}
		glPopMatrix();
	}
}
void LightsRenderer::draw_disabled(Rendering type) {
	disableFeatures();
	TCScene* scene = glRenderer->getScene();
	drawDisabledObjects(scene->iterator(),type);
	drawDisabledObjects(scene->temporaries(),type);
	if ( type == RENDERER_PAINT ) {
		drawDisabledSupports(scene->supports(),type);
	}
}

void LightsRenderer::drawDisabledObjects(SceneIterator *iterator, Rendering type)
{
	while (iterator->hasMore() ) {
		glPushMatrix();
		TCObject *object = iterator->next();
		if ( type == RENDERER_SELECTION )
			glLoadName(-1);
		if ( type == RENDERER_FEEDBACK )
			glPassThrough(-1);
		if ( type == RENDERER_PAINT || type == RENDERER_SELECTION ) {
			Vector p = object->getPosition();
			
			glTranslated(p.x,p.y,p.z);
			
			if ( object->isHighlighted() || object->isSelected() ) {
				draw_object_placers(object->getPosition());
				if ( object->isSelected() ) {

					glMultMatrixd(object->getRotationMatrix());
					draw_move_arrow();
				}
			}
			glPopMatrix();
			glPushMatrix();
		}
		setup_object_matrix(object,type);
		object->draw_disabled();
		glPopMatrix();
	}
}
void LightsRenderer::drawDisabledSupports(SceneIterator *iterator, Rendering type)
{
	while (iterator->hasMore() ) {
		glPushMatrix();
		TCObject *object = iterator->next();
		setup_object_matrix(object,type);
		object->draw_disabled();
		glPopMatrix();
	}
}

// void LightsRenderer::draw_
void LightsRenderer::disableFeatures()
{
	glDisable(GL_LIGHTING);
	
}

void LightsRenderer::enableFeatures()
{
	glEnable(GL_LIGHTING);
}

void LightsRenderer::draw_object_placers(Vector position) {

	glBegin(GL_LINES);
		glColor4d(0.5,0.0,0.0,0.85);
		glVertex3d(0,0,0);
		glVertex3d(-position.v[0],0,0);
		glColor4d(0.0,0.5,0.0,0.85);
		glVertex3d(0,0,0);
		glVertex3d(0,-position.v[1],0);
		glColor4d(0.0,0.0,0.5,0.85);
		glVertex3d(0,0,0);
		glVertex3d(0.0,0.0,-position.v[2]);
	glEnd();
}

void LightsRenderer::draw_move_arrow()
{
	draw_arrow(arrow_x);
	draw_arrow(arrow_y);
	draw_arrow(arrow_z);
}

void LightsRenderer::draw_arrow(TCArrow& arrow)
{
	glPushMatrix();
	GLdouble * m = arrow.getRotationMatrix();
	glMultMatrixd(m);
	glLoadName(arrow.getId());
	glColor4dv(arrow.getColor());
	arrow.draw();
	glPopMatrix();
}


};