/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef TCOVERLAY_H
#define TCOVERLAY_H
#include "renderer.h"

namespace TerrificCoder {
class CameraReset;
	
class TCOverlay : public Renderer
{

	friend class CameraReset;
	CameraReset *cameraReset;
	TCArrowHead *(heads[6]);
	void setup_head(TCArrowHead *head,Vector4 color, Vector rotation, Vector position );
	void draw_head(TCArrowHead* head);
	void draw_xy();
	
	double width;
	double height;
	double aspect;
	GLint viewport[4];
	
	TCBox box;
public:
	TCOverlay(GLRenderer *renderer);
	~TCOverlay();
	
	

    virtual void disableFeatures();
    virtual void draw(Rendering type);
    virtual void enableFeatures();
    virtual void modelview();
    virtual void resize(int width, int height);
    virtual void setup();
    TCArrowHead ** getHeads();
	GLint *getViewport();
	void ortho();
protected:
	TCArrowHead p_x;
	TCArrowHead m_x;
	TCArrowHead p_y;
	TCArrowHead m_y;
	TCArrowHead p_z;
	TCArrowHead m_z;
	
};


};
#endif // TMOVERLAY_H
