/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "quaternion.h"
#include "math.h"

namespace TerrificCoder {
Quaternion::Quaternion()
{
	 Quaternion(0,0,0,0);
}

Quaternion::Quaternion(GLdouble b, GLdouble c, GLdouble d)
{
	 Quaternion(0,b,c,d);
}

Quaternion::Quaternion(GLdouble a, GLdouble b, GLdouble c, GLdouble d)
{
	this->a = a;
	this->b = b;
	this->c = c;
	this->d = d;
}

Quaternion::Quaternion(GLdouble alpha, Vector v)
{
	GLdouble sin_a = sin(alpha/2);
	this->a = cos(alpha/2);
	
	this->b = sin_a*v.x;
	this->c = sin_a*v.y;
	this->d = sin_a*v.z;
}

Quaternion Quaternion::RotationQuaternion(Vector euler)
{
	GLdouble cosz = cos( euler.z/2.0 );
	GLdouble cosy = cos( euler.y/2.0 );
	GLdouble cosx = cos( euler.x/2.0 );
	GLdouble sinz = sin( euler.z/2.0 );
	GLdouble siny = sin( euler.y/2.0 );
	GLdouble sinx = sin( euler.x/2.0 );

	GLdouble coszy = cosz * cosy;
	GLdouble sinzy = sinz * siny;
	GLdouble q0 = coszy * cosx + sinzy * sinx;
	GLdouble q1 = coszy * sinx - sinzy * cosx;
	GLdouble q2 = cosz * siny * cosx + sinz* cosy*sinx;
	GLdouble q3 = sinz * cosy * cosx + cosz* siny*sinx;
	return Quaternion( q0, q1, q2, q3 );
}

Quaternion& Quaternion::operator*=(Quaternion other)
{
	GLdouble result_a = a*other.a - (b * other.b + c * other.c + d * other.d);
	GLdouble result_b = a*other.b + b*other.a + c*other.d - d*other.c;
	GLdouble result_c = a*other.c - b*other.d + c*other.a + d*other.b;
	GLdouble result_d = a*other.d + b*other.c - c*other.b + d*other.a;
	this->a = result_a;
	this->b = result_b;
	this->c = result_c;
	this->d = result_d;
	return *this;
}

Quaternion Quaternion::operator*(Quaternion a)
{
	Quaternion r = *this;
	return (r *= a);
}

Quaternion& Quaternion::operator=(Quaternion a)
{
	this->a = a.a;
	this->b = a.b;
	this->c = a.c;
	this->d = a.d;
	return *this;
}
void Quaternion::set(const GLdouble v[4])
{
	this->a = v[0];
	this->b = v[1];
	this->c = v[2];
	this->d = v[3];
}
Vector Quaternion::rotateVector(Vector v)
{
	
	Quaternion qv(0, v.x, v.y, v.z);
	Quaternion r = ((this->coniugate()) * qv) * (*this);
	return Vector(r.b,r.c,r.d);
}
Quaternion Quaternion::coniugate()
{
	return Quaternion(this->a, -this->b, -this->c, -this->d);
}

std::ostream& operator<<(std::ostream& out, Quaternion& x)
{
	out << "(" << x.a << ", " << x.b << ", " << x.c << ", " << x.d << ")";
	return out;
}

};