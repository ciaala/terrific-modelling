/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "math.h"
#include "matrix.h"
#include <string.h>
namespace TerrificCoder{

Matrix::Matrix( Vector v ) {
	this->setup(v.x, v.y,v.z );
}

Matrix::Matrix(GLdouble* array)
{
	memcpy(matrix,array,16*sizeof(GLdouble));
}


Matrix::Matrix(GLdouble rotx, GLdouble roty, GLdouble rotz)
{
	this->setup(rotx,roty,rotz);
}
void Matrix::setup(GLdouble rotx, GLdouble roty, GLdouble rotz) {
	GLdouble cosO = cos(roty);
	GLdouble cosP = cos(rotx);
	GLdouble cosE = cos(rotz);
	
	GLdouble sinO = sin(roty);
	GLdouble sinP = sin(rotx);
	GLdouble sinE = sin(rotz);
	
	matrix[0] =  cosO*cosE;
	matrix[1] = -cosP*sinE + sinP*sinO*cosE;
	matrix[2] =  sinP*sinE + cosP*sinO*cosE;
	matrix[3] =  0;


	matrix[4]  =  cosO*sinE;
	matrix[5]  =  cosP*cosE + sinP*sinO*sinE;
	matrix[6]  = -sinP*cosE + cosP*sinO*sinE;
	matrix[7]  =  0;
	
	matrix[8]  = -sinO;
	matrix[9]  =  sinP*cosO;
	matrix[10] =  cosP*cosO;
	matrix[11] =  0;

	matrix[12] =  0;
	matrix[13] =  0;
	matrix[14] =  0;
	matrix[15] =  1;
}
GLdouble* Matrix::toArray()
{
	return matrix;
}


GLdouble Matrix::rowColumn(Vector4 *r, int column)
{
	GLdouble result=0;
	result  = r->v[0] * matrix[column];
	result += r->v[1] * matrix[column+4];
	result += r->v[2] * matrix[column+8];
	result += r->v[3] * matrix[column+12];
	return result;
}

};
