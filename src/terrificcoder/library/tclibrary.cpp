/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "tclibrary.h"
#include <iostream>
#include <sstream>
#include <ostream>
#include <utility>
#define GL_DEBUG_MODE 0
#include "math.h"
namespace TerrificCoder {

void glerror(const std::string filename, int line){
	glerror(filename.c_str(),line);
}


void glerror(const char* filename, int line) {
	GLenum error = glGetError();
	
	if (error != GL_NO_ERROR ) {
			while ( error != GL_NO_ERROR ){
				std::cerr << filename << ":"<< line << "> OpenGL error: " << error << std::endl;
				error = glGetError();
			}
	} else {
		if (GL_DEBUG_MODE){
			std::cerr << filename << ":"<< line << "> Ok!" << std::endl;
		}
	}
}

std::pair<Vector, GLdouble> pointOnPlane(Vector direction, Vector base, Vector4 plane)
{
	Vector x0 = base;
	Vector v = direction;
	double a = plane.v[0], b = plane.v[1], c = plane.v[2], d = plane.v[3];
	double t = -( a*x0.x + b*x0.y + c*x0.z + d ) / ( a*v.x + b*v.y + c*v.z );
	Vector result( x0.x + v.x *t, x0.y + v.y*t, x0.z + v.z*t );
	return std::make_pair<Vector,double>(result,t);
}


Vector planePoint(Vector rayP1,Vector rayP2, Vector4 plane)
{
	Vector x0 = rayP1;
	Vector v = rayP1 - rayP2;
	double a = plane.v[0], b = plane.v[1], c = plane.v[2], d = plane.v[3];
	double t = -( a*x0.x + b*x0.y + c*x0.z + d ) / ( a*v.x + b*v.y + c*v.z );
	Vector result( x0.x + v.x *t, x0.y + v.y*t, x0.z + v.z*t);
	return result;
}
/**
 * 	Given the parameters it computes the normal n (v1 * v2) that will be
 * 	the plane normal, and then it calculates the known intersection d
 * 
 *	@param ray direction from the camera-eye to the specified position
 *	@param arw direction on the plane
 *  @param pos the position where the plane will stand
 * 
 */
Vector4 computePlane(Vector ray, Vector arw, Vector pos)
{
	ray.normalize();
	arw.normalize();
	
	Vector v1 = pos + ray;
	Vector v2 = pos + arw;

	Vector n = v1 * v2;
	n.normalize();

	float d = -(n.x * pos.x + n.y*pos.y + n.z * pos.z);
	Vector4 base_plane = Vector4(n.x, n.y, n.z ,d);

	Vector n2 = n*arw;
	GLdouble d2 = -(n2.x * pos.x + n2.y*pos.y + n2.z * pos.z);

	return Vector4( n2.x, n2.y, n2.z, d2 );

}



};