/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef TCLIBRARY_H
#define TCLIBRARY_H

#include <QtGui/QColor>
#include <GL/gl.h>
#include <math.h>
#include <ostream>
#include "vector.h"
#include "matrix.h"
#include "quaternion.h"



namespace TerrificCoder {



void glerror(const char* filename, int line);


static inline void qSetColor( float colorVec[], QColor c)
{
    colorVec[0] = c.redF();
    colorVec[1] = c.greenF();
    colorVec[2] = c.blueF();
    colorVec[3] = c.alphaF();
}

static inline GLfloat colorFloat(GLint value) {
    return ((GLfloat)value)/255.0;
}

static inline void setup_vector( GLfloat vector[3], GLfloat a, GLfloat b, GLfloat c) {
    vector[0]=a;
    vector[1]=b;
    vector[2]=c;
}

static inline void normalize( GLfloat v[3]) {
    GLfloat d = sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
    v[0] /= d;
    v[1] /= d;
    v[2] /= d;
}
/**
 * a = b x c
 *
 */
static inline void cross_product( GLfloat a[3], GLfloat b[3], GLfloat c[3]) {
    a[0] = b[1]*c[2]-b[2]*c[1];
    a[1] = b[2]*c[0]-b[0]*c[2];
    a[2] = b[0]*c[1]-b[1]*c[0];
}

static inline void rotation( GLfloat v[3], GLfloat n[3], GLfloat alpha)
{
    v[0] = cos(alpha)*n[0] - sin(alpha)*n[1];
    v[1] = sin(alpha)*n[0] + cos(alpha)*n[1];
    v[2] = n[2];
}
/**
 * return (a b)
 */
static inline GLfloat scalar_product(GLfloat a[3], GLfloat b[3]) {
    return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
}

/**
 * a = (b c) c
 *
 * c is normalized
 */
static inline void vector_projection(GLfloat a[3], GLfloat b[3], GLfloat c[3]) {
    GLfloat v[3];
    memcpy(v,c,sizeof(GLfloat(3)));
    normalize(v);
    GLfloat d = scalar_product(b,v);
    a[0] = d*v[0];
    a[1] = d*v[1];
    a[2] = d*v[2];
}

/**
 * a= b-c
 */
static inline void vector_subctrat(GLfloat a[3], GLfloat b[3], GLfloat c[3])
{
    a[0] = b[0] - c[0];
    a[1] = b[1] - c[1];
    a[2] = b[2] - c[2];
}
/**
 * d = b-c
 */
static inline GLfloat point_distance(GLfloat b[3], GLfloat c[3])
{
    GLfloat a[3];
    a[0] = b[0] - c[0];
    a[1] = b[1] - c[1];
    a[2] = b[2] - c[2];
    return sqrt( a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
}


Vector planePoint(Vector rayP1,Vector rayP2, Vector4 plane);
std::pair<Vector,GLdouble> pointOnPlane(TerrificCoder::Vector direction, TerrificCoder::Vector base, TerrificCoder::Vector4 plane);
Vector4 computePlane(Vector ray, Vector arw, Vector pos);



inline int signum(int value ) {
	return value > 0 ? 1 : -1;
}
inline int limit(GLint value, GLint step ){
	return value >= step ? step : value <= -step ? -step : value;
}

};


#endif // LIBRARY_H
