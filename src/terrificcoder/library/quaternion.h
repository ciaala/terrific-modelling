/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef QUATERNION_H
#define QUATERNION_H

#include "GL/gl.h"
#include "vector.h"
#include "iostream"

namespace TerrificCoder{
class Quaternion
{
public:
	Quaternion();
	Quaternion(GLdouble a, GLdouble b, GLdouble c, GLdouble d);
	Quaternion(GLdouble b, GLdouble c, GLdouble d);
	Quaternion(GLdouble v[4]);
	
	Quaternion(GLdouble alpha, Vector v );
	
	union {
		struct {
			GLdouble a;
			GLdouble b;
			GLdouble c;
			GLdouble d;
		};
		GLdouble v[4];
	};
	
	// PLUS
	Quaternion& operator+=(Quaternion a);
	Quaternion operator+(Quaternion b);
	// MULTIPLY Quaternion
	Quaternion& operator*=(Quaternion a);
	Quaternion operator*(Quaternion a);
	// MULTIPLY Real
	Quaternion& operator*=(GLdouble a);
	Quaternion operator*(GLdouble a);
	//
	Quaternion& operator=(Quaternion a);

	
	
	void set(const GLdouble v[4]);

	Vector rotateVector( Vector v );
	Quaternion coniugate();

	static Quaternion RotationQuaternion( Vector euler );

	friend std::ostream& operator<<(std::ostream& out, Quaternion& x);
};
std::ostream& operator<<(std::ostream& out, Quaternion& x);

};
#endif // QUATERNION_H
