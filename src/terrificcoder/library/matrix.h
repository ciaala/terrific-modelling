/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MATRIX_H
#define MATRIX_H
#include <GL/gl.h>
#include "vector.h"

namespace TerrificCoder {
class Vector;
class Vector4;
class Matrix
{
public:
	explicit Matrix(Vector v);
	explicit Matrix(GLdouble * array);
	Matrix(GLdouble rotx=0, GLdouble roty=0, GLdouble rotz=0);
	GLdouble* toArray();
	void setup(GLdouble rotx, GLdouble roty, GLdouble rotz);
	GLdouble rowColumn(Vector4 *r, int column );
private:
	GLdouble matrix[16];
};

};

#endif // MATRIX_H
