/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef VECTOR_H
#define VECTOR_H
#include "GL/gl.h"
#include <iostream>
#include <sstream>
#include "matrix.h"
namespace TerrificCoder {
class Matrix;
class Vector4;
class Vector
{
public:
	Vector(GLdouble x, GLdouble y, GLdouble z);
	Vector(Vector4 v4);
	Vector();
	Vector(GLdouble *v);
	union {
		GLdouble v[3];
		struct {
			GLdouble x;
			GLdouble y;
			GLdouble z;
		};
	};
	void set(GLdouble x, GLdouble y, GLdouble z);
	
	GLdouble length();
	GLdouble quadLength();
	
	void normalize();
	void scale(GLdouble scale_factor);
	bool operator==(Vector &other);
	Vector& operator=(Vector other);
	
	Vector& operator-=(Vector other);
	Vector operator-(Vector other);
	Vector operator-();
	
	Vector& operator+=(Vector other);
	Vector operator+(Vector other);

	GLdouble product_scalar(Vector other);
// 	Vector product_vector(Vector other);

	Vector& operator*=(Vector other);
	Vector operator*(Vector other);

	void approx();

	friend std::ostream& operator<<(std::ostream& out, Vector& v);
	friend std::stringstream& operator<<(std::stringstream& out, Vector& v);
};

std::ostream& operator<<(std::ostream& out, Vector& v);

typedef class Vector Position;

class Vector4
{
public:
	Vector4(GLdouble x=0,GLdouble y=0, GLdouble z=0, GLdouble w=1.0);
	GLdouble v[4];

	bool operator==(Vector4 &b);
	Vector4& operator*=( Matrix *matrix );
};
std::ostream& operator<<(std::ostream& out, Vector4 &v);
class Vector4f {
public:
	Vector4f(GLfloat *v);
	Vector4f(GLfloat x=0,GLfloat y=0, GLfloat z=0, GLfloat w=1.0);
	GLfloat v[4];
	bool operator==(Vector4f &b);
};

};
#endif // VECTOR_H
