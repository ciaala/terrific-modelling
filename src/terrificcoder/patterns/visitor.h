/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_VISITOR_H
#define TERRIFICCODER_VISITOR_H

namespace TerrificCoder {
class TCObject;
class TCBox;
class TCCone;
class TCSphere;
class TCCamera;
class TCCylinder;
class TCThorus;
class VisitorElement;
class TCLight;
class TCText2D;
class TCText3D;
class TCPlane;
class TCArrow;
class TCObjectSet;
class TCTeapot;

class Visitor
{
public:
	virtual ~Visitor();
	virtual void visit(VisitorElement* e)=0;
	virtual void visit(TCObject *e)=0;
	virtual void visit(TCBox *e)=0;
	virtual void visit(TCCylinder *e)=0;
	virtual void visit(TCCone *e)=0;
	virtual void visit(TCSphere *e)=0;
	virtual void visit(TCThorus *e)=0;
	virtual void visit(TCCamera *e)=0;
	virtual void visit(TCLight *e)=0;
	virtual void visit(TCArrow *e)=0;
	virtual void visit(TCText2D *e)=0;
	virtual void visit(TCText3D *e)=0;
	virtual void visit(TCPlane *e)=0;
	virtual void visit(TCObjectSet *e)=0;
	virtual void visit(TCTeapot *e)=0;
	
};

class VisitorElement {
public:
	virtual ~VisitorElement();
	virtual void accept(Visitor *v)=0;
};

};

#endif // TERRIFICCODER_VISITOR_H
