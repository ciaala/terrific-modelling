/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <utility>
#include <algorithm>
#include "changemanager.h"
#include "../library/logging.h"




namespace TerrificCoder {

typedef std::multimap<Subject*,Observer*>::iterator I;
	
ChangeManager::~ChangeManager()
{

}

	
SimpleChangeManager *SimpleChangeManager::instance = NULL;

SimpleChangeManager::SimpleChangeManager()
{
	this->enabled = true;
}

void SimpleChangeManager::Notify(Subject* subject, Observer* observer)
{
	instance->notify(subject,observer);
}
void SimpleChangeManager::Subscribe(Subject* subject, Observer* observer)
{ 
	if ( subject && observer ) 
		instance->subscribe(subject,observer);
	else
		std::cerr << "[SIMPLE CHANGE MANAGER] Error subjet or observer are NULL" << std::endl;
}
void SimpleChangeManager::Unsubscribe(Subject* subject, Observer* observer)
{
	instance->unsubscribe(subject,observer);
}



void SimpleChangeManager::notify(Subject* subject, Observer* )
{
	if ( enabled ) { 
		//TODO OBSERVER IS UNUSED
		if ( find(blocked.begin(),blocked.end(),subject) == blocked.end()) {
			//std::multimap< Subject*, Observer* >::iterator it = this->mapping.find(subject);
			
			
			std::pair<I,I> list = this->mapping.equal_range(subject);
			for ( I it = list.first; it != list.second; it++ ) {
				Observer* ob = (*it).second;
				ob->update(subject);
			
			}
		}
	}
}

void SimpleChangeManager::subscribe(Subject* subject, Observer* observer)
{
	if ( subject == NULL || observer == NULL ) {
		TCERR << " subject or observer are null " << subject <<", " << observer;
	}
	this->mapping.insert( std::make_pair<Subject*,Observer*>(subject,observer) );
//	subject->attach(this);
}
void SimpleChangeManager::unsubscribe(Subject* subject, Observer* observer)
{
	std::multimap< Subject*, Observer* >::iterator i = this->mapping.find(subject);
	while ( i != this->mapping.end() ) {

		if (i->second == observer ) {
			
			this->mapping.erase(i);
			return;
		}
		i++;
	}
}
void SimpleChangeManager::StaticInitialization()
{
	instance = new SimpleChangeManager();
}

SimpleChangeManager::~SimpleChangeManager() { }

void SimpleChangeManager::Block(Subject* subject)
{
	instance->block(subject);
}
void SimpleChangeManager::block(Subject* subject)
{
	blocked.push_back(subject);
}

void SimpleChangeManager::Unblock(Subject* subject)
{
	instance->unblock(subject);
}

void SimpleChangeManager::unblock(Subject* subject)
{
	std::list< Subject* >::iterator it = find( blocked.begin(),blocked.end(),subject );
// 	if ( it != blocked.end() ) {
	blocked.erase(it);
// 	}
}
void SimpleChangeManager::GlobalBlock()
{
	instance->globalBlock();
}
void SimpleChangeManager::globalBlock()
{
	this->enabled = false;
}
void SimpleChangeManager::GlobalUnblock()
{
	instance->globalUnblock();
}
void SimpleChangeManager::globalUnblock()
{
	this->enabled = true;
}

};
