/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CHANGEMANAGER_H
#define CHANGEMANAGER_H
#include "observer.h"
#include <map>

namespace TerrificCoder {
	
class ChangeManager
{
protected:
	virtual void subscribe(Subject *subject,Observer *observer)=0;
	virtual void unsubscribe(Subject *subject, Observer *observer)=0;
	virtual void notify(Subject* subject, Observer* observer=NULL)=0;
	virtual void block(Subject *subject) =0;
	virtual void unblock(Subject *subject) =0;
	virtual void globalBlock() =0;
	virtual void globalUnblock() =0;
	
protected:
	static void Notify(Subject* subject, Observer* observer=NULL);
	static void Subscribe(Subject *subject,Observer *observer);
	static void Unsubscribe(Subject *subject, Observer *observer);
	static void Block(Subject *subject);
	static void Unblock(Subject *subject);
	static void GlobalBlock();
	static void GlobalUnblock();
public:
	virtual ~ChangeManager();
    
};


class SimpleChangeManager : public ChangeManager {
	
private:
	static SimpleChangeManager *instance;
	
private:
	std::multimap<Subject*,Observer*> mapping;
	std::list<Subject*> blocked;
	bool enabled;
	SimpleChangeManager();
    virtual ~SimpleChangeManager();
public:

	static void StaticInitialization();
	static void Block(Subject *subject);
	static void Unblock(Subject *subject);
	static void Subscribe(Subject *subject,Observer *observer);
	static void Unsubscribe(Subject *subject, Observer *observer);
	static void Notify(Subject* subject, Observer* observer=NULL);
	static void GlobalBlock();
	static void GlobalUnblock();
	
public :
	virtual void globalBlock();
	virtual void globalUnblock();
	virtual void notify(Subject* subject, Observer* observer = 0);
	virtual void subscribe(Subject* subject, Observer* observer);
	virtual void unsubscribe(Subject* subject, Observer* observer);
	virtual void block(Subject *subject);
	virtual void unblock(Subject *subject);
};

};
#endif // CHANGEMANAGER_H
