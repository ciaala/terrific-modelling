


#ifndef TERRIFICCODER_H
#define TERRIFICCODER_H
#include <string>


namespace TerrificCoder {
	typedef
	enum Key {
		Right,
		Left,
		Up,
		Down,
		ESC,
		CANC,
		BACKSPACE,
		
		KEY_R,
		KEY_V,
		KEY_N,
		KEY_P,

		KEY_Q,
		KEY_A,
		KEY_W,
		KEY_S,
		KEY_E,
		KEY_D,
		
		KEY_Y,
		KEY_H,
		KEY_U,
		KEY_J,
		KEY_I,
		KEY_K,

		KEY_C,
	}
	Key;
	
	typedef 
	enum MouseButton {
		NoneButton,
		RightButton,
		LeftButton,
		MiddleButton
	} 
	MouseButton;
	

	typedef
	enum Orientation {
		NoneOrientation,
		Orizontal,
		Vertical
	} Orientation;

	typedef
	
	enum TCObjectEnum{
		Any,
		
		Box,
		Cone,
		Sphere,
		Thorus,
		Cylinder,
		Text2D,
		Text3D,
		Plane,
		Arrow,
		ArrowHead,
		Teapot,
		
		QuaternionCamera,
		FreeCamera,
		
		Light,
		
		SelectionBox,
		ObjectSet,
		ScenePoint,
		PlanePoint,
		Line,
		
	} TCObjectEnum;

//	static std::vector<TCObjectEnum> TCObjectEnumList;

std::string enumToString(TCObjectEnum type);

};

#endif
