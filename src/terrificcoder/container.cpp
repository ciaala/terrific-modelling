/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdlib.h"
#include "container.h"
#include "patterns/changemanager.h"
namespace TerrificCoder {
Container *Container::instance = new Container();

Container::Container()
{
	SimpleChangeManager::StaticInitialization();
	scene = new TCScene();
	renderer = new GLRenderer();
	feedback = new GLFeedback(renderer);
	inputContext = new InputContext(renderer, scene);
	textureManager = new TextureManager();
	selectedObject = NULL;
	highlightedObject = NULL;
	//selectedCamera = NULL;
}
Container::~Container() {
	if (instance != NULL ) {
		for ( std::list< Command* >::iterator it = instance->pending.begin(); it != pending.end(); ++it ) {
			delete (*it);
			it++;
		}

		for ( std::list< Command* >::iterator it = history.begin(); it != history.end(); ++it ) {
			delete (*it);
			it++;
		}

	}
}
void Container::setup()
{
	instance->renderer->setScene(instance->scene);
	instance->feedback->setScene(instance->scene);
	instance->textureManager->setScene(instance->scene);
}


void Container::free()
{
	delete instance;
}

// Container* Container::getContainer()
// {
// 	return instance;
// }

GLRenderer* Container::getRenderer()
{
	return instance->renderer;
}
TCScene* Container::getScene()
{
	return instance->scene;
}

TerrificCoder::InputContext* Container::getInputContext()
{
	return instance->inputContext;
}

TextureManager* Container::getTextureManager()
{
	return instance->textureManager;
}



void Container::addCommand(Command *command)
{
	instance->pending.push_back(command);
}

TCObject* Container::getSelectedObject()
{
	return instance->selectedObject;
}


void Container::executeCommands()
{
	std::list< Command* >::const_iterator i = pending.begin();
	while ( i != pending.end() ){
		Command *c = (*i);
		c->execute();
		history.push_back(c);
		++i;
	}
	pending.clear();
	//renderer->timerEvent();
}

void Container::setSelectedObject(TCObject* object)
{
	if ( instance->selectedObject && instance->selectedObject != object ){
		instance->selectedObject->setSelected(false);
	}
	if ( object != NULL ) {
		object->setSelected(true);
	}
	instance->selectedObject = object;
	instance->objectSubject.notify();
}

void Container::timerEvent() {
	instance->executeCommands();
}

void Container::attachSelectedObject(Observer* observer)
{
	instance->objectSubject.attach(observer);
}

void Container::detachSelectedObject(Observer* observer)
{
	instance->objectSubject.detach(observer);
}
void Container::notifySelectedUpdate()
{
	instance->objectSubject.notify();
}


void Container::attachSelectedCamera(Observer* observer)
{
	instance->cameraSubject.attach(observer);
}

void Container::detachSelectedCamera(Observer* observer)
{
	instance->cameraSubject.detach(observer);
}

void Container::attachHighlightedObject(Observer* observer)
{
	instance->highlightedSubject.attach(observer);
}
void Container::detachHighlightedObject(Observer* observer)
{
	instance->highlightedSubject.detach(observer);
}
void Container::setHighlightedObject(TCObject* object)
{
	
	instance->highlightedObject = object;
	instance->highlightedSubject.notify();
}
TCObject* Container::getHighlightedObject()
{
	return instance->highlightedObject;
}

void Container::DeleteSelectedObject()
{
	TCObject *o = instance->selectedObject;
	instance->setSelectedObject(NULL);
	instance->scene->object_del(o);
}
void Container::Reset()
{

	instance->renderer->setCamera(NULL);

	instance->renderer->initializeGL();
	Container::setHighlightedObject(NULL);
	Container::setSelectedObject(NULL);
	
	TCScene* scene = Container::getScene();
	
	SimpleChangeManager::GlobalBlock();
	
	scene->reset();
	
	SimpleChangeManager::GlobalUnblock();
	scene->notify();
	std::pair<int,int> p =instance->renderer->getViewportSize();
	instance->renderer->resizeGL(p.first,p.second);
}

TCGLWidget* Container::getWidget() {
	return instance->widget;
}
void Container::setWidget(TerrificCoder::TCGLWidget* widget)
{
	instance->widget = widget;
}

};

