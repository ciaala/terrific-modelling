#include "terrificcoder.h"

namespace TerrificCoder {

std::string enumToString(TCObjectEnum type) {
	switch  (type){
		case Any:
			return "Any";
		case Box:
			return "Box";
		case Cone:
			return "Cone";
		case Cylinder:
			return "Cylinder";
		case Light:
			return "Light";
		case QuaternionCamera:
			return "QuaternionCamera";
		case Sphere :
			return "Sphere";
		case Thorus:
			return "Thorus";
		case Text2D:
			return "Text2D";
		case Text3D:
			return "Text3D";
		case Plane :
			return "Plane";
		case Arrow :
			return "Arrow";
		case ArrowHead :
			return "ArrowHead";
		case FreeCamera :
			return "FreeCamera";
		case SelectionBox :
			return "SelectionBox";
		case ObjectSet :
			return "ObjectSet";
		case ScenePoint:
			return "ScenePoint";
		case Teapot:
			return "Teapot";
		case PlanePoint:
			return "PlanePoint";
		case Line:
			return "Line";
	}
	return "None";
}

	

};