/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_SCENEITERATOR_H
#define TERRIFICCODER_SCENEITERATOR_H
#include "../model/tcobject.h"
namespace TerrificCoder {

    class SceneIterator {
    public:
        virtual ~SceneIterator();
        virtual bool hasMore();
        virtual TCObject* next();
        virtual TCObject* current();
    };

    class MapIterator : public SceneIterator {
    public:
        MapIterator(std::multimap<TCObjectEnum,TCObject*>::iterator begin,
                    std::multimap<TCObjectEnum,TCObject*>::iterator end);
        virtual bool hasMore();
        virtual TCObject* next();
        virtual TCObject* current();


    private:
        std::multimap<TCObjectEnum,TCObject*>::iterator iterator;
        std::multimap<TCObjectEnum,TCObject*>::iterator end;
    };


    class ListIterator : public SceneIterator
    {
    private :
        std::list<TCObject* >* list;
        std::list<TCObject* >::iterator iterator;
        std::list<TCObject* >::iterator end;
    public :
        ListIterator(std::list<TCObject*> *list);

        virtual ~ListIterator();
        virtual TCObject* current();
        virtual bool hasMore();
        virtual TCObject* next();

    };



};

#endif // TERRIFICCODER_SCENEITERATOR_H
