/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_SCENEPOINT_H
#define TERRIFICCODER_SCENEPOINT_H

#include "../model/tcobject.h"
#include "../geometry/isegment.h"

namespace TerrificCoder {

class TCScenePoint : public TerrificCoder::TCObject
{

public:
	TCScenePoint(Vector v = Vector(0,0,0));
    virtual ~TCScenePoint();
    virtual void draw();
    virtual void accept(TerrificCoder::Visitor* v);
    virtual void draw_disabled();
};

class TCPlanePoint : public TerrificCoder::TCObject
{
public:
	TCPlanePoint();
    virtual ~TCPlanePoint();
	virtual void draw();
	virtual void accept(TerrificCoder::Visitor *v);
	virtual void draw_disabled();
};

class TCLine : public TerrificCoder::TCObject
{
private:
	Vector p1;
    Vector p2;
public:
	TCLine();
	TCLine(Vector direction, Vector point);
	virtual void setup(Vector direction, Vector point);
	virtual void draw();
	virtual void accept(TerrificCoder::Visitor *v);
	virtual void draw_disabled();
};

class TCSegment: public TerrificCoder::TCObject {
private:
	Vector p1;
	Vector p2;
public:
	TCSegment();
	TCSegment(Vector point1, Vector point2);
	TCSegment(ISegment *segment);
	virtual void setup(Vector direction, Vector point);
	virtual void draw();
	virtual void accept(TerrificCoder::Visitor *v);
	virtual void draw_disabled();
};
};

#endif // TERRIFICCODER_SCENEPOINT_H
