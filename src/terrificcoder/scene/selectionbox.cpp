/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "selectionbox.h"
#include "GL/freeglut.h"
#include "../library/vector.h"
#include "scenepoint.h"

#include "../container.h"

#include "../library/logging.h"
#include "../patterns/changemanager.h"

namespace TerrificCoder {

TCSelectionBox::TCSelectionBox(Vector first, Vector second): TCSimpleObject(SelectionBox,"Volume")
{
	this->color = Vector4(0.5,0.5,1.0,1.0);
// 	this->size = 15.0;
	this->first = first;
	this->filled = false;
	this->setOppositePoint(second);
}

void TCSelectionBox::setOppositePoint(Vector second)
{
	this->second = second;

	Vector diff = second - first;
	diff.scale(0.5);
// 	Vector sum = second + first;
// 	sum.scale(0.5);
	this->position = first + diff;

	this->width = abs(second.x-first.x);
	this->height = abs(second.y-first.y);
	this->depth = abs(second.z-first.z);
	
}
void TCSelectionBox::update(Subject* subject)
{
//	TCDEBUG << "Updating trough scene modification" ;
//	TCScene *scene = Container::getScene();	
//	scene->detach(this);
	test();
}


void TCSelectionBox::fill()
{


	if ( filled == false ) 	{
		TCScene *scene = Container::getScene();
		scene->attach(this);
		fi = new FullInclusion (this,scene);
		filled = true;
	}
	test();

}

void TCSelectionBox::test()
{
	TCScene *scene = Container::getScene();
	SimpleChangeManager::Block(scene);
	
	fi->test();
	this->full = fi->getFull();
	this->partial = fi->getPartial();
	SimpleChangeManager::Unblock(scene);
}

Vector TCSelectionBox::getFirst()
{
	return first;
}
Vector TCSelectionBox::getSecond()
{
	return second;
}

void TCSelectionBox::draw()
{

}
void TCSelectionBox::draw_disabled()
{
	GLdouble w = width/2.0;
	GLdouble h = height/2.0;
	GLdouble d = depth/2.0;
// 	glutWireCube(size);
	#   define V(a,b,c) glVertex3d( a w, b h, c d);
	#   define N(a,b,c) glNormal3d( a, b, c );

// 	glColor4d(1.0,0.9,0.9,0.1);
// 	glBegin( GL_QUADS ); N( 1.0, 0.0, 0.0); V(+,-,+); V(+,-,-); V(+,+,-); V(+,+,+); glEnd();
// 	glBegin( GL_QUADS ); N( 0.0, 1.0, 0.0); V(+,+,+); V(+,+,-); V(-,+,-); V(-,+,+); glEnd();
// 	glBegin( GL_QUADS ); N( 0.0, 0.0, 1.0); V(+,+,+); V(-,+,+); V(-,-,+); V(+,-,+); glEnd();
// 	glBegin( GL_QUADS ); N(-1.0, 0.0, 0.0); V(-,-,+); V(-,+,+); V(-,+,-); V(-,-,-); glEnd();
// 	glBegin( GL_QUADS ); N( 0.0,-1.0, 0.0); V(-,-,+); V(-,-,-); V(+,-,-); V(+,-,+); glEnd();
// 	glBegin( GL_QUADS ); N( 0.0, 0.0,-1.0); V(-,-,-); V(-,+,-); V(+,+,-); V(+,-,-); glEnd();
	glLineWidth(4.0);
	glColor4d(1.0,0.9,0.9,0.4);
	glBegin( GL_LINE_LOOP ); N( 1.0, 0.0, 0.0); V(+,-,+); V(+,-,-); V(+,+,-); V(+,+,+); glEnd();
	glBegin( GL_LINE_LOOP ); N( 0.0, 1.0, 0.0); V(+,+,+); V(+,+,-); V(-,+,-); V(-,+,+); glEnd();
	glBegin( GL_LINE_LOOP ); N( 0.0, 0.0, 1.0); V(+,+,+); V(-,+,+); V(-,-,+); V(+,-,+); glEnd();
	glBegin( GL_LINE_LOOP ); N(-1.0, 0.0, 0.0); V(-,-,+); V(-,+,+); V(-,+,-); V(-,-,-); glEnd();
	glBegin( GL_LINE_LOOP ); N( 0.0,-1.0, 0.0); V(-,-,+); V(-,-,-); V(+,-,-); V(+,-,+); glEnd();
	glBegin( GL_LINE_LOOP ); N( 0.0, 0.0,-1.0); V(-,-,-); V(-,+,-); V(+,+,-); V(+,-,-); glEnd();
	glLineWidth(1.0);
	#   undef V
	#   undef N
}

void TCSelectionBox::accept(TerrificCoder::Visitor* v)
{
	v->visit(this);
}

std::string TCSelectionBox::getName()
{
	std::string append = "{NULL}";
	if ( full != NULL ) {
		append = "F:";
		append.append(full->getName());
	}
	if ( partial != NULL ) {
		append.append("P:");
		append.append(partial->getName());
	}
    return "SelBox: " + append;
}


TCSelectionBox::~TCSelectionBox()
{
	TCScene *scene = Container::getScene();	
	scene->detach( this );
}

void TCSelectionBox::setHighlighted(bool status)
{
    this->highlighted = status;
    partial->setHighlighted(status);
	full->setHighlighted(status);

}





TCArrow::TCArrow() : TCSimpleObject(Arrow,"Arrow")
{
	this->x = 1.0;
	this->y = 0.0;
	this->z = 0.0;
	this->width = 2.0;
	this->height = 5.0;
	
}

void TCArrow::accept(Visitor* v)
{
	v->visit(this);
}

void TCArrow::draw()
{
	glPushMatrix();
	glRotatef(90.0,x,y,z);
	glutSolidCylinder( 1.0,20.0,20,8);
	glTranslatef(0.0,0.0,20.0);
	glutSolidCone(width,height,20,8);
	glPopMatrix();
}

/////////////////////////////


TCArrowHead::TCArrowHead() : TCSimpleObject(Arrow,"ArrowHead")
{
	this->x = 1.0;
	this->y = 0.0;
	this->z = 0.0;
	this->width = 2.0;
	this->height = 5.0;
}

void TCArrowHead::accept(Visitor* v)
{
	v->visit(this);
}

void TCArrowHead::draw()
{
	glPushMatrix();
	glRotatef(90.0,x,y,z);
	glTranslatef(position.x,position.y,position.z);
	glutSolidCone(width,height,20,8);
	glPopMatrix();
}






/////////////////////////////

TCPlane::TCPlane(Vector4 g): TCSimpleObject(Plane,"Plane")
{
	setup(g);
}

void TCPlane::setup(Vector4 g) {
	Vector (* compute)(Vector4, float, float) ;
	if ( g.v[2] != 0 ){
		compute = computeZ;
	} else if ( g.v[1] !=0) {
		compute = computeY;
	} else if ( g.v[0] != 0) {
		compute = computeX;
	} else {
		return;
	}
	float length = 100;
	v1 = compute( g, length, length );
	v2 = compute( g, length, -length );
	v3 = compute( g, -length, -length );
	v4 = compute( g, -length, length );
}


TCPlane::TCPlane(Vector v1, Vector v2, Vector v3, Vector v4, Vector4 color) : TCSimpleObject(Plane,"Plane")
{
	this->v1 = v1;
	this->v2 = v2;
	this->v3 = v3;
	this->v4 = v4;
	this->color = color;
}
/**
 * find the x given the y and z coordinates
 */
Vector TCPlane::computeX(Vector4 g, float y, float z)
{
	return Vector( ( g.v[1] * y + g.v[2]*z + g.v[3] ) / g.v[0], y, z);
}

Vector TCPlane::computeY(Vector4 g, float x, float z)
{
	return Vector( x, ( g.v[0] * x + g.v[2]*z + g.v[3] ) / g.v[1], z);
}

Vector TCPlane::computeZ(Vector4 g, float x, float y)
{
	return Vector( x, y, ( g.v[0] * x + g.v[1]*y + g.v[3] ) / g.v[2]);
}

void TCPlane::accept(Visitor* v)
{
	v->visit(this);
}

void TCPlane::draw()
{

}
void TCPlane::draw_disabled() {
// // 	glLoadIdentity();
	glDisable(GL_CULL_FACE);
	glBegin(GL_QUADS);
	glVertex3dv(v1.v);
	glVertex3dv(v2.v);
	glVertex3dv(v3.v);
	glVertex3dv(v4.v);
	glEnd();
	glEnable(GL_CULL_FACE);
	
}
};