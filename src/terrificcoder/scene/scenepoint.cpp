/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "scenepoint.h"
#include "GL/glut.h"
#include "math.h"
#include "../geometry/isegment.h"
namespace TerrificCoder {
	
TCScenePoint::TCScenePoint(Vector v) : TCObject(ScenePoint,"TCScenePoint")
{
//	this->setHighlighted(true);
	this->setColor(255,255,255,192);
	this->setPosition(v);
}

TCScenePoint::~TCScenePoint()
{

}

void TCScenePoint::draw_disabled()
{
	//glutSolidSphere(1.0,10,10);
	glPointSize(4.0);
	glBegin(GL_POINTS);
	glVertex3d(0,0,0);
	glEnd();
}


TCPlanePoint::TCPlanePoint() : TCObject(PlanePoint, "TCPlanePoint")
{
	this->setHighlighted(true);
}

void TCPlanePoint::accept(Visitor* v)
{

}
void TCPlanePoint::draw()
{

}
void TCPlanePoint::draw_disabled()
{
// 	glPushMatrix();
// 	glTranslatef(position.x,position.y,position.z);
	
	glColor4f(1.0,0.8,0.8,0.85);
	glBegin(GL_LINE_LOOP);
	for(double alpha = 0.0; alpha < 2* M_PI ;alpha +=0.1) {
		double x = 10*sin( alpha );
		double y = 10*cos( alpha );
		
		glVertex3f(x,y,0.0);
	}
	glEnd();
/*	
	glBegin(GL_LINES);
	glVertex3f(0.0,0.0,-200000);
	glVertex3f(0.0,0.0,200000);
	glEnd();*/
// 	glPopMatrix();
}

/** OLD LINES
 *
 glBegin(GL_LINES);
 i f ( pos*ition.v[2] != 0 ) {
	 glColor4f(1.0,0.0,0.0,0.85);
	 glVertex3f(0,0,0);
	 glVertex3f(-position.v[0],-position.v[1],0);
 }
 if ( position.v[1] != 0 ) {
	 glColor4f(0.0,1.0,0.0,0.85);
	 glVertex3f(0,0,0);
	 glVertex3f(-position.v[0],0,-position.v[2]);
 }
 if ( position.v[0] != 0 ) {
	 glColor4f(0,0,1.0,0.85);
	 glVertex3f(0,0,0);
	 glVertex3f(0,-position.v[1],-position.v[2]);
 }
 glEnd();
 *
 *
 *
 */
//
void TCScenePoint::draw()
{

}

void TCScenePoint::accept(TerrificCoder::Visitor* v)
{
    v->visit(this);
}
TCPlanePoint::~TCPlanePoint()
{

}

TCLine::TCLine() {
	this->setColor(128,128,128,128);
}
TCLine::TCLine(Vector direction, Vector point): TCObject(Line,"Line")
{
	this->setColor(128,128,128,128);
	this->setup(direction,point);
}

void TCLine::setup(Vector direction, Vector point)
{
	direction.normalize();
	direction.scale(5000);

	this->p1 = point - direction;
	this->p2 = point + direction;
}

void TCLine::accept(Visitor* v)
{
	v->visit(this);
}

void TCLine::draw()
{
	
}

void TCLine::draw_disabled()
{
	glBegin(GL_LINES);
	glVertex3dv(p1.v);
	glVertex3dv(p2.v);
	glEnd();
}
TCSegment::TCSegment(): TCObject(Line,"Segment")
{
	this->setColor(255,128,128,255);
}
TCSegment::TCSegment(ISegment* segment): TCObject(Line,"Segment")
{
	this->setup( segment->point1(), segment->point2());
	
}

TCSegment::TCSegment(Vector point1, Vector point2): TCObject(Line,"Segment")
{
	this->setup(point1,point2);
}

void TCSegment::setup(Vector point1, Vector point2)
{
	this->setColor(255,128,128,255);
	this->p1 = point1;
	this->p2 = point2;
}
void TCSegment::draw_disabled()
{
	glDepthFunc(GL_ALWAYS);
	glBegin(GL_LINES);
	glVertex3dv(p1.v);
	glVertex3dv(p2.v);
	glEnd();
	glDepthFunc(GL_LEQUAL);
}

void TCSegment::draw()
{

}
void TCSegment::accept(Visitor* v)
{
	v->visit(this);
}

};
