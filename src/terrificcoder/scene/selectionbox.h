/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_SELECTIONBOX_H
#define TERRIFICCODER_SELECTIONBOX_H

#include "../model/tcsimpleobjects.h"
#include "objectset.h"
#include "../patterns/observer.h"
#include "../geometry/inclusion.h"

namespace TerrificCoder {
class TCScenePoint;
class FullInclusion;
class TCSelectionBox : public TerrificCoder::TCSimpleObject, TerrificCoder::Observer
{
protected:
	TCObjectSet* partial;
	TCObjectSet* full;
	Vector first;
	Vector second;
	bool filled;
	FullInclusion* fi;
public:
	TCSelectionBox(Vector first, Vector second);
    virtual ~TCSelectionBox();
    virtual void draw();
    virtual void draw_disabled();
    virtual void accept(TerrificCoder::Visitor* v);
    virtual void setOppositePoint(Vector second);


    virtual void setHighlighted (bool status);

    virtual void update(Subject* subject);
	virtual Vector getFirst();
	virtual Vector getSecond();
	virtual void fill();
	virtual void test();
    virtual std::string getName();

};

class TCArrow : public TerrificCoder::TCSimpleObject
{
protected:
	GLfloat x,y,z;
	
public:
	TCArrow();
	virtual void draw();
	virtual void accept(TerrificCoder::Visitor* v);
	
};


class TCArrowHead :  public TerrificCoder::TCSimpleObject
{
protected :
	GLfloat x,y,z;
public :
	TCArrowHead();
    virtual void accept(Visitor* v);
    virtual void draw();
};

class TCPlane : public TerrificCoder::TCSimpleObject
{
protected:

	Vector v1;
	Vector v2;
	Vector v3;
	Vector v4;

	static Vector computeX(Vector4 g, float y, float z );
	static Vector computeY(Vector4 g, float x, float z );
	static Vector computeZ(Vector4 g, float x, float y );
	
public:
	TCPlane(Vector v1, Vector v2, Vector v3, Vector v4, Vector4 color );
	TCPlane(Vector4 general);
	void setup(Vector4 g);
	virtual void draw();
    virtual void draw_disabled();
	virtual void accept(TerrificCoder::Visitor* v);
  
	
};

}

#endif // TERRIFICCODER_SELECTIONBOX_H
