/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/



#include <utility>
#include <algorithm>
#include <functional>
// #include "malloc.h"
#include "tcscene.h"
// #include "../scene/tccameraquaternion.h"



// #include "../library/tclibrary.h"

#include "../model/tctext.h"
#include "../model/tcsimpleobjects.h"
#include "../model/teapot.h"
#include "../model/tclastcamera.h"
#include "scenepoint.h"
#include "selectionbox.h"
namespace TerrificCoder {

TCScene::TCScene() : Subject(std::string("Scene"))
{
// 	this->container = container;
// 	this->gui = gui;
}

TCScene::~TCScene()
{
	std::multimap< TCObjectEnum, TCObject* >::iterator i = objects.begin();
	while (i!=objects.end()) {
		delete (i->second);
		++i;
	}
}

void TCScene::object_del(TCObject* object)
{
	std::multimap<TCObjectEnum,TCObject*>::iterator iterator = objects.begin();
	while( iterator != objects.end() ){
		if ( (iterator->second) == object ){
			objects.erase(iterator);
			delete object;
			notify();
			return;
		}
		iterator++;
	}
}

TCObject* TCScene::getObject(unsigned int id)
{
	std::multimap<TCObjectEnum,TCObject*>::iterator iterator = objects.begin();
	while( iterator != objects.end() ){
		if ( (iterator->second)->getId() == id ){
			return (iterator->second);
		}
		iterator++;
	}
	std::list< TCObject* >::const_iterator it = temporary.begin();
	while( it != temporary.end() ) {
		if ( (*it)->getId() == id ) {
			return *it;
		}
		it++;
	}
	return NULL;
}


TCObject* TCScene::objectCreate(TCObjectEnum type)
{
	TCObject* object;
	switch ( type ){
		case TerrificCoder::Box:
			object = new TCBox();
			break;
		case TerrificCoder::Sphere:
			object = new TCSphere();
			break;
		case TerrificCoder::Cone:
			object = new TCCone();
			break;
		case TerrificCoder::Cylinder:
			object = new TCCylinder();
			break;
		case TerrificCoder::Thorus:
			object = new TCThorus();
			break;
		case TerrificCoder::QuaternionCamera :
			object =  new TCLastCamera();
			break;
		case TerrificCoder::FreeCamera :
			object = new TCLastCamera();
			break;
		case TerrificCoder::Light:
			object = new TCLight();
			break;
		case TerrificCoder::Plane:
			object = new TCPlane(Vector4(0,0,1,0));
// 				        Vector(20,20,0),
// 				        Vector(20,-20,0),
// 				        Vector(-20,-20,0),
// 				        Vector(-20,20,0),
// 				        Vector4(1.0,0.0,1.0,1.0)
// 			);
			break;
		case TerrificCoder::Arrow:
			object = new TCArrow();
			break;
		case TerrificCoder::Text2D :
			object = new TCText2D();
			break;
		case TerrificCoder::Text3D:
			object = new TCText3D();
			break;
		case TerrificCoder::ScenePoint:
			object = new TCScenePoint();
			break;
		case TerrificCoder::Teapot:
			object = new TCTeapot();
			break;
		default:
			object = NULL;
			
	}
	if ( object != NULL) {
		insertObject(object);
	}
	return object;
}



int TCScene::countObject(TerrificCoder::TCObjectEnum type)
{
	return objects.count(type);
}

void TCScene::insertObject(TCObject* object)
{
	if (object != NULL ) {
		objects.insert(std::make_pair( object->getType(),object));
		if ( object->getType() == Light ) {
			light.push_back(object);
		}
		notify();
	}
}
void TCScene::insertTemporary(TCObject *object)
{
	temporary.push_front(object);
	notify();

}
void TCScene::insertSupport(TCObject* object)
{
	support.push_front(object);
	notify();
}



void TCScene::deleteTemporary(TCObject *object)
{
	if ( std::find(temporary.begin(),temporary.end(),object) != temporary.end()){
		temporary.remove(object);
		delete object;
	}
}

void TCScene::deleteSupport(TCObject *object)
{
	if ( std::find(support.begin(),support.end(),object) != support.end()){
		support.remove(object);
		delete object;
	}
}

void TCScene::removeObject(TCObject* object)
{
	std::multimap<TCObjectEnum,TCObject*>::iterator iterator = objects.begin();
	while( iterator != objects.end() ){
		if ( (iterator->second) == object ){
			objects.erase(iterator);
			notify();
			return;
		}
		iterator++;
	}
}

void TCScene::removeTemporary(TCObject* object)
{
	if ( std::find(temporary.begin(),temporary.end(),object) != temporary.end()){
		temporary.remove(object);
	}
}

void TCScene::removeSupport(TCObject* object)
{
	if ( std::find(support.begin(),support.end(),object) != support.end()){
		support.remove(object);
	}
}
SceneIterator* TCScene::lights()
{
	return new ListIterator(&light);
}
SceneIterator* TCScene::supports()
{
	return new ListIterator(&support);
}
SceneIterator* TCScene::temporaries()
{
	return new ListIterator(&temporary);
}


SceneIterator *TCScene::iterator()
{
	std::multimap< TCObjectEnum, TCObject* >::iterator begin = objects.begin();
	std::multimap< TCObjectEnum, TCObject* >::iterator end = objects.end();
	MapIterator *i = new MapIterator(begin,end);
	return i;
}

SceneIterator *TCScene::iterator(TerrificCoder::TCObjectEnum type)
{
	std::pair<
		std::_Rb_tree_iterator<
			std::pair< const TerrificCoder::TCObjectEnum, TCObject* > >,
		std::_Rb_tree_iterator<
			std::pair< const TerrificCoder::TCObjectEnum, TCObject* > > >
		pair = objects.equal_range(type);
	
		MapIterator *i = new MapIterator(pair.first,pair.second);
	return i;
}

void TCScene::resetHightlightBox()
{
	std::multimap< TCObjectEnum, TCObject* >::const_iterator i = objects.begin();
	while ( i != objects.end()) {
		i->second->setHighlighted(false);
		i++;
	}
}

void TCScene::highlightSelected(std::list< unsigned int >* ids)
{
	
	//std::cerr << "[SELECTION] ids = [ ";
	std::list<GLuint>::iterator i = ids->begin();
	while ( i != ids->end() ) {
		int id = *i;
		TCObject *o = this->getObject(id);
		if ( o != NULL ) {
			o->setHighlighted(true);
			//	std::cerr << id << ", ";
		} else {
			//	std::cerr << "[SELECTION] It was not possible to retrieve object :" << id << std::endl;
		}
		i++;
	}
	//std::cerr <<"]"<< std::endl;
}


void TCScene::deleteAllSupports(std::list< TerrificCoder::TCObject* >* supports )
{
	while ( !supports->empty() ) {
		TCObject * o = supports->front();
		support.remove(o);
		supports->pop_front();
		delete o;
	}
}

void TCScene::reset()
{
	std::multimap< TCObjectEnum, TCObject* >::iterator i = objects.begin();
	while (i!=objects.end()) {	
		delete (i->second);
		++i;
	}
	objects.clear();
	while ( !support.empty() ) {
		TCObject * o = support.front();
		support.pop_front();
		delete o;
	}
	while ( !temporary.empty() ) {
		TCObject * o = temporary.front();
		temporary.pop_front();
		delete o;
	}
}

};

