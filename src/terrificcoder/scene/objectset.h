/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_OBJECTSET_H
#define TERRIFICCODER_OBJECTSET_H

#include "../model/tcobject.h"


namespace TerrificCoder {

class TCObjectSet : public TerrificCoder::TCObject
{
private:
    std::list<TCObject*> *objects;

public:
    TCObjectSet(std::list<TCObject*> *objects);
    virtual void draw();
    virtual void accept(TerrificCoder::Visitor* v);
    virtual std::list<TCObject*>* getList();
    virtual void setPosition(Vector position);
	virtual void setRotation(Vector rotation);
    virtual void setRotation(double rotx, double roty, double rotz);
    virtual Vector getPosition();
    virtual Vector getRotation();
    virtual bool isHighlighted();
    virtual bool isSelected();
    virtual void setHighlighted(bool status);
    virtual void setSelected(bool status);
	virtual std::string getName();
    GLdouble* getColor();
    virtual void setColor(int red, int green, int blue, int alpha);
    void add(TCObject* o);
	
};

}

#endif // TERRIFICCODER_OBJECTSET_H
