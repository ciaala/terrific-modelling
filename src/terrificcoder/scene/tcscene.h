/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef TCSCENE_H
#define TCSCENE_H

#include <GL/glu.h>
#include <list>
#include <map>
#include <utility>

#include "../terrificcoder.h"
#include "../library/tclibrary.h"
#include "../patterns/observer.h"

#include "../model/tccamera.h"
#include "../model/tcobject.h"
#include "../model/tclight.h"

#include "sceneobserver.h"
#include "sceneiterator.h"

namespace TerrificCoder {




class TCCamera;
class TCContainer;
class TCGui;
class SceneIterator;

class TCScene : public TerrificCoder::Subject
{
/** Observer Pattern */

/** Iterator Pattern */
public:
	SceneIterator* iterator();
	SceneIterator* iterator(TCObjectEnum type);

	SceneIterator* temporaries();
	
	SceneIterator* supports();

	SceneIterator* lights();
/** */

	
public:
	TCScene();
	~TCScene();

	void removeObject(TCObject *object);
	void object_del(TCObject *object);
	TCObject *objectCreate(TerrificCoder::TCObjectEnum type);
	TCObject *getObject(unsigned int id);
	void insertObject(TCObject* object);
	
public:
	int countObject(TCObjectEnum type);
	void reset();
    void resetHightlightBox();
	void highlightSelected(std::list<unsigned int> *ids);

	
	void insertTemporary(TCObject *object);
    void deleteTemporary(TCObject *object);
	void removeTemporary(TCObject *object);

	
	void insertSupport(TCObject *object);
	void deleteSupport(TCObject *object);
	void removeSupport(TCObject *object);
	void deleteAllSupports(std::list<TCObject*> *supports);

// 	std::list<TCObject*> getObjectsByType(TCObjectEnum type);

private:

	std::multimap<TCObjectEnum,TCObject*> objects;
	std::list<TCObject*> temporary;
	std::list<TCObject*> support;
	std::list<TCObject*> light;
};




};
#endif
