/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "objectset.h"
#include <functional>
#include <utility>
#include <algorithm>
#include <sstream>
#define TCLOGGING_LEVEL TCERR_LEVEL
#include "../library/logging.h"

namespace TerrificCoder {

TCObjectSet::TCObjectSet(std::list< TCObject* >* objects) : TCObject(ObjectSet,"ObjectSet" )
{
    this->objects = objects;
	if ( objects == NULL ){
		this->objects = new std::list<TCObject*>();
	}
	TCDEBUG << this->getName() << std::endl;
}
void TCObjectSet::draw()
{
	
}

void TCObjectSet::accept(TerrificCoder::Visitor* v)
{
    v->visit(this);
}
std::list< TCObject* >* TCObjectSet::getList()
{
	return objects;
}



void TCObjectSet::setPosition(Vector position)
{
//     for ( std::list< TCObject* >::const_iterator it = objects->begin(); it != objects->end(); it++ )
// 	{
// 		TCObject *o = (*it);
// 		Vector v = o->getPosition();
// 		o->setPosition(v+position);
// 	}
}
Vector TCObjectSet::getPosition()
{
    return Vector();
}

void TCObjectSet::setRotation(double rotx, double roty, double rotz)
{
    TerrificCoder::TCObject::setRotation(rotx, roty, rotz);
}

void TCObjectSet::setRotation(Vector rotation)
{
// 	for ( std::list< TCObject* >::const_iterator it = objects->begin(); it != objects->end(); it++ )
// 	{
// 		TCObject *o = (*it);
// 		Vector v = o->getRotation();
// 		o->setRotation(v+rotation);
// 	}
}

Vector TCObjectSet::getRotation()
{
    return Vector();
}

void TCObjectSet::setHighlighted(bool status)
{
	std::for_each(this->objects->begin(), this->objects->end(),std::bind2nd(std::mem_fun(&TCObject::setHighlighted),status));
}
bool TCObjectSet::isHighlighted()
{
	bool result = true;
	for ( std::list< TCObject* >::const_iterator it = objects->begin(); it != objects->end(); it++ )
	{
		TCObject *o = (*it);
		result = result && o->isHighlighted();
	}
	return result;
}
void TCObjectSet::setSelected(bool status)
{
	std::for_each(this->objects->begin(), this->objects->end(),std::bind2nd(std::mem_fun(&TCObject::setSelected),status));
}

bool TCObjectSet::isSelected()
{
	bool result = true;
	for ( std::list< TCObject* >::const_iterator it = objects->begin(); it != objects->end(); it++ )
	{
		TCObject *o = (*it);
		result = result && o->isSelected();
	}
	return result;
}
std::string TCObjectSet::getName()
{
	if (objects) {
		std::ostringstream out;
		out << "Set {";
		for ( std::list< TCObject* >::const_iterator it = objects->begin(); it != objects->end(); it++ )
		{
			TCObject *o = (*it);
			if ( o ) {
				unsigned id = o->getId();
				out << o->getName() << ", " ;
			}
		}
		out << "}";
		return out.str();
	}
	return std::string("Set {NULL}");
}

GLdouble* TCObjectSet::getColor()
{
	GLdouble *colors = objects->front()->getColor();
	for ( std::list< TCObject* >::const_iterator it = objects->begin(); it != objects->end(); it++ )
	{
		TCObject *o = (*it);
		GLdouble *c = o->getColor();
		if ( (c[0] != colors[0]) || (c[1] != colors[1]) || (c[2] != colors[2]) || (c[3] != colors[3]) ) {
			return this->color.v;
		}
	}
	return colors;
}

void TCObjectSet::setColor(int red, int green, int blue, int alpha)
{
	for ( std::list< TCObject* >::const_iterator it = objects->begin(); it != objects->end(); it++ )
	{
		TCObject *o = (*it);
		o->setColor(red, green, blue, alpha);
	}
}

void TCObjectSet::add(TCObject* o)
{
	TCINFO << "set.add " << o->getName() ;
	this->objects->push_back(o);
}


};
