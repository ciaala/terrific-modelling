/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "sceneiterator.h"

namespace TerrificCoder {




// SceneIterator::SceneIterator(std::multimap< TCObjectEnum, TCObject* >::iterator begin,std::multimap< TCObjectEnum, TCObject* >::iterator end)
// {
// 	this->iterator = begin;
// 	this->end = end;
// }

SceneIterator::~SceneIterator()
{

}

bool SceneIterator::hasMore() {
    return false;
}
TCObject *SceneIterator::next() {
    return NULL;
}
TCObject *SceneIterator::current() {
    return NULL;
}

MapIterator::MapIterator(std::multimap< TCObjectEnum, TCObject* >::iterator begin, std::multimap< TCObjectEnum, TCObject* >::iterator end)
{
    this->iterator = begin;
    this->end = end;
}

TCObject* MapIterator::current()
{
    if ( iterator != end )
        return iterator->second;
    return NULL;
}

bool MapIterator::hasMore()
{
    if ( iterator != end )
        return true;
    return false;
}

TCObject* MapIterator::next()
{
    if ( iterator != end ) {
        TCObject *o = this->iterator->second;
        iterator++;
        return o;
    }
    return NULL;
}

ListIterator::ListIterator(std::list< TCObject* >* list)
{
    this->list =list;
    this->iterator = list->begin();
    this->end = list->end();

}
TCObject* ListIterator::current()
{
    return *iterator;
}
bool ListIterator::hasMore()
{
    return iterator != end;
}
TCObject* ListIterator::next()
{
	if ( iterator != end ) {
		TCObject *o =  *iterator;
		iterator ++;
		return o;
	}
    return NULL;
}
ListIterator::~ListIterator()
{

}



};

