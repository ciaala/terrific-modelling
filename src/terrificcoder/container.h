/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_CONTAINER_H
#define TERRIFICCODER_CONTAINER_H
#include <list>
#include <set>
#include "model/tcobject.h"
#include "scene/objectset.h"
#include "model/tccamera.h"
#include "renderer/glrenderer.h"
#include "renderer/feedback.h"
#include "input/inputcontext.h"
#include "command/command.h"
#include "patterns/observer.h"
#include "model/texturemanager.h"
#include "qt/tcglwidget.h"


namespace TerrificCoder {
class TCScene;	
class InputContext;
class TCObject;
class GLRenderer;
class Command;
class TextureManager;
class TCGLWidget;

class Container
{

protected:
	
public:
	static TCScene *getScene();
	static GLRenderer *getRenderer();
	static TCObject *getSelectedObject();
	static TCObject *getHighlightedObject();
	static TextureManager *getTextureManager();
	static InputContext *getInputContext();
	static void DeleteSelectedObject();
	
	static void setSelectedObject(TCObject* o);
	static void setHighlightedObject(TerrificCoder::TCObject* object);
	static void timerEvent();
	static void addCommand(Command *command);
	static void notifySelectedUpdate();
	
/** OBSERVER-SUBJECT */
	static void attachSelectedObject( Observer *observer );
	static void detachSelectedObject( Observer *observer );
	static void attachSelectedCamera( Observer *observer );
	static void detachSelectedCamera( Observer *observer );
	static void attachHighlightedObject( Observer *observer );
	static void detachHighlightedObject( Observer *observer );
	static TCGLWidget* getWidget();
	static void setWidget(TCGLWidget*widget);
/*
	static void attachUpdatedObject( Observer *observer );
	static void detachUpdatedObject( Observer *observer );*/
	
	
/** SINGLETON */
public:
//	Container* getContainer();
	static void free();
	static void setup();
	static void Reset();
private:
	Container();
	~Container();
	static Container* instance;
/** END SINGLETON */


private:
	TCScene *scene;
	TCObject *selectedObject;
	TCObject *highlightedObject;
	
	std::set<TCObject*> highlihtedSet;
	std::set<TCObject*> selectedSet;
	
	TCObjectSet *set;
	
	GLRenderer *renderer;
	GLFeedback *feedback;
	InputContext *inputContext;
	TextureManager *textureManager;
	std::list<Command*> history;
	std::list<Command*> pending;


	Subject highlightedSubject;
	Subject objectSubject;
	Subject cameraSubject;
	void executeCommands();



	
	TCGLWidget* widget;
};

}

#endif // TERRIFICCODER_CONTAINER_H
