/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_MATERIALEDITORDOCK_H
#define TERRIFICCODER_MATERIALEDITORDOCK_H

#include "qtdock.h"
#include <QReadWriteLock>
#include "../model/material.h"
#include "../model/texturemanager.h"
#include <QLabel>
#include <QListView>
#include <QStringListModel>
namespace TerrificCoder {

class MaterialEditorDock : public TerrificCoder::QTDock
{
private:
	Q_OBJECT
protected:
	QReadWriteLock lock;
	void setMaterial(GLenum type, QColor color);
	QLabel* previewImage;
	QListView* textureList;
	TextureManager *tm;
	void updateMaterialDetail(Material *material);
	void invalidateMaterials();
public:
    explicit MaterialEditorDock(const QString& title, QWidget* parent = 0, Qt::WindowFlags flags = 0);
    explicit MaterialEditorDock(QWidget* parent = 0, Qt::WindowFlags flags = 0);
    virtual void visit(TerrificCoder::TCLight* e);
    virtual void visit(TerrificCoder::TCText2D* e);
    virtual void visit(TerrificCoder::TCText3D* e);
    virtual void visit(TerrificCoder::TCCamera* e);
    virtual void visit(TerrificCoder::TCThorus* e);
    virtual void visit(TerrificCoder::TCSphere* e);
    virtual void visit(TerrificCoder::TCCone* e);
    virtual void visit(TerrificCoder::TCCylinder* e);
    virtual void visit(TerrificCoder::TCBox* e);
    virtual void visit(TerrificCoder::TCObject* e);
    virtual void visit(TerrificCoder::VisitorElement* e);
	virtual void visit(TerrificCoder::TCPlane* e);
	virtual void visit(TerrificCoder::TCArrow* e);
	virtual void visit(TerrificCoder::TCObjectSet* e);
	virtual void visit(TerrificCoder::TCTeapot* e);
//    virtual void update(Subject* subject);

public slots:
	void material_specular(QColor color);
	void material_shininess(double value);
	void material_diffuse(QColor color);
	void material_emission(QColor color);
	void material_ambient(QColor color);

	void load_texture();
	void unload_texture();
	void assign_texture();
	void revoke_texture();
	
signals:
	void signal_material_ambient(QColor color);
	void signal_material_specular(QColor color);
	void signal_material_shininess(double value);
	void signal_material_diffuse(QColor color);
	void signal_material_emission(QColor color);
	
private:
	void setup();
	void setupTextureList();
	QListView* getTextureList();
	QLabel* getPreviewImage();
	QStringListModel model;
	QStringList list;
};

}

#endif // TERRIFICCODER_MATERIALEDITORDOCK_H
