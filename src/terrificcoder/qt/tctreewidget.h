/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TCTREEWIDGET_H
#define TCTREEWIDGET_H

#define QT_NO_DEBUG_OUTPUT
#include <vector>
#include <qtreeview.h>
#include <QReadWriteLock>
#include "../scene/tcscene.h"

namespace TerrificCoder{
class TCTreeModel;

class TCTreeWidget : public QTreeView
{
	Q_OBJECT
	TCTreeModel* treeModel;
public:
	explicit TCTreeWidget(QWidget* parent = 0);
	virtual ~TCTreeWidget();
	
	bool isExpanded(const QModelIndex &index) const;
	void setExpanded(const QModelIndex &index, bool expand);
public slots:
	void clickedObject(QModelIndex index);
};




typedef enum TCTreeType { Object, Folder, Scene} TCTreeType;


class TCTreeItem
{
public:
	TCTreeItem(TCTreeType itemType);
	virtual QString getName()=0;
	TCTreeItem* getParent();
	TCTreeType getType();
	std::vector<TCTreeItem*> *getChildren();
	void addChild(TCTreeItem* item);
	virtual ~TCTreeItem();
	int getRowByChild(TCTreeItem* item);
	TCTreeItem* getChildByRow(int row);
	virtual int columnCount();
	int rowCounts();
	void clear();
	virtual bool isExpanded() const;
	virtual void setExpanded(bool expand);
protected:
	TCTreeItem* parent;
	bool expanded;
private:
	TCTreeItem();
	TCTreeType itemType;
	std::vector<TCTreeItem*> childreen;
	
};



class TCSceneItem : public TCTreeItem
{
private:
	QString name;
public:
	TCSceneItem(std::string name);
	QString getName();
};


class TCFoldItem : public TCTreeItem
{
private:
	TCObjectEnum type;
public:
	TCFoldItem(TCObjectEnum type = Any, TCSceneItem* parent = 0);
	QString getName();
	TCObjectEnum getEnumType();
};


class TCObjectItem: public TCTreeItem {
private :
	TCObject* object;
public :
	TCObjectItem(TCObject *object,TCFoldItem *parent=0);
	QString getName();
	QString getId();
	TCObject *getObject();
    virtual void setExpanded(bool expand);
	virtual int columnCount();
};




class TCTreeModel : public QAbstractItemModel, public TerrificCoder::Observer {
public :

	explicit TCTreeModel(QObject* parent = 0);
	virtual ~TCTreeModel();
public :
	virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
	virtual QModelIndex parent(const QModelIndex& child) const;
	
	virtual int columnCount(const QModelIndex& parent = QModelIndex()) const;
	virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;
	virtual QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const;
	virtual Qt::ItemFlags flags(const QModelIndex& index) const;
	virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
	TCTreeItem* itemForIndex(const QModelIndex& index) const;

	/*
		void beginResetScene();
		void endResetScene();
		void insertObject(TCObject* arg1);
	*/
	virtual void update(Subject *subject);

    virtual bool hasChildren(const QModelIndex& parent = QModelIndex()) const;
	virtual bool isExpanded(const QModelIndex &index) const;
	virtual void setExpanded(const QModelIndex &index, bool expand);
	
private:
	TCSceneItem* sceneItem;
	TCScene* scene;
	std::list<TCObjectEnum> enumList;
	int rowForFolder(TerrificCoder::TCObjectEnum type) const;
	void loadScene();
	QReadWriteLock lock;
};


};
#endif // TCTREEWIDGET_H
