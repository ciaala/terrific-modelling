/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "cameraeditordock.h"
#include "../container.h"
namespace TerrificCoder {

CameraEditorDock::CameraEditorDock(const QString& title, QWidget* parent, Qt::WindowFlags flags): QTDock(title, parent, flags)
{
// 	Container::attachSelectedObject(this);
}

CameraEditorDock::CameraEditorDock(QWidget* parent, Qt::WindowFlags flags): QTDock(parent, flags)
{
// 	Container::attachSelectedObject(this);
}


void CameraEditorDock::visit(TerrificCoder::TCText3D* ) { this->setVisible(false); } 
void CameraEditorDock::visit(TerrificCoder::TCText2D* ) { this->setVisible(false); }
void CameraEditorDock::visit(TerrificCoder::TCLight* ) { this->setVisible(false); }
void CameraEditorDock::visit(TerrificCoder::TCCamera* e){ this->updateCameraDetail(e); }
void CameraEditorDock::visit(TerrificCoder::TCThorus* ) { this->setVisible(false); }
void CameraEditorDock::visit(TerrificCoder::TCSphere* ) { this->setVisible(false); }
void CameraEditorDock::visit(TerrificCoder::TCCone* )	{ this->setVisible(false); }
void CameraEditorDock::visit(TerrificCoder::TCCylinder* ) { this->setVisible(false); }
void CameraEditorDock::visit(TerrificCoder::TCBox* )	{ this->setVisible(false);}
void CameraEditorDock::visit(TerrificCoder::TCObject* ) { this->setVisible(false);}
void CameraEditorDock::visit(TerrificCoder::VisitorElement* ) { this->setVisible(false); }
void CameraEditorDock::visit(TerrificCoder::TCPlane* ) { this->setVisible(false); }
void CameraEditorDock::visit(TerrificCoder::TCArrow* ) { this->setVisible(false); }
void CameraEditorDock::visit(TerrificCoder::TCObjectSet* ) { this->setVisible(false); }
void CameraEditorDock::visit(TCTeapot* ){ this->setVisible(false);}



void CameraEditorDock::updateCameraDetail(TCCamera* camera)
{
// 	QReadWriteLock(lock);
// 	this->setVisible(true);
// 	Vector center = camera->getCenter();
// 	emit signal_center_x(center.x);
// 	emit signal_center_y(center.y);
// 	emit signal_center_z(center.z);
// 
// 	Vector up = camera->getUp();
// 	emit signal_up_x(up.x);
// 	emit signal_up_y(up.y);
// 	emit signal_up_z(up.z);
// 
// 	Vector right = camera->getRight();
// 	emit signal_right_x(right.x);
// 	emit signal_right_y(right.y);
// 	emit signal_right_z(right.z);

}
/*
void CameraEditorDock::center_x(double value)
{
	TCObject *o = Container::getSelectedObject();
	if ( o->getType() == TerrificCoder::QuaternionCamera ) {
		TCCamera *camera = (TCCamera*) o;
		Vector center = camera->getCenter();
		camera->set_center(value, center.y,center.z);
	}
}
void CameraEditorDock::center_y(double value)
{
	TCObject *o = Container::getSelectedObject();
	if ( o->getType() == TerrificCoder::QuaternionCamera ) {
		TCCamera *camera = (TCCamera*) o;
		Vector center = camera->getCenter();
		camera->set_center(center.x, value, center.y);
	}
}
void CameraEditorDock::center_z(double value)
{
	TCObject *o = Container::getSelectedObject();
	if ( o->getType() == TerrificCoder::QuaternionCamera ) {
		TCCamera *camera = (TCCamera*) o;
		Vector center = camera->getCenter();
		camera->set_center(center.x,center.y,value);
	}
}

void CameraEditorDock::up_x(double value)
{
	TCObject *o = Container::getSelectedObject();
	if ( o->getType() == TerrificCoder::QuaternionCamera ) {
		TCCamera *camera = (TCCamera*) o;
		Vector up = camera->getUp();
		camera->set_up(value, up.y, up.z);
	}
}
void CameraEditorDock::up_y(double value)
{
	TCObject *o = Container::getSelectedObject();
	if ( o->getType() == TerrificCoder::QuaternionCamera ) {
		TCCamera *camera = (TCCamera*) o;
		Vector up = camera->getUp();
		camera->set_up(up.x,value,up.z);
	}
}
void CameraEditorDock::up_z(double value)
{
	TCObject *o = Container::getSelectedObject();
	if ( o->getType() == TerrificCoder::QuaternionCamera ) {
		TCCamera *camera = (TCCamera*) o;
		Vector up = camera->getUp();
		camera->set_up(up.x, up.y,value);
	}
}
void CameraEditorDock::right_x(double value)
{
	TCObject *o = Container::getSelectedObject();
	if ( o->getType() == TerrificCoder::QuaternionCamera ) {
		TCCamera *camera = (TCCamera*) o;
		Vector right = camera->getRight();
		camera->setRight(value, right.y, right.z);
	}
}
void CameraEditorDock::right_y(double value)
{
	TCObject *o = Container::getSelectedObject();
	if ( o->getType() == TerrificCoder::QuaternionCamera ) {
		TCCamera *camera = (TCCamera*) o;
		Vector right = camera->getRight();
		camera->setRight(right.x,value,right.z);
	}
}
void CameraEditorDock::right_z(double value)
{
	TCObject *o = Container::getSelectedObject();
	if ( o->getType() == TerrificCoder::QuaternionCamera ) {
		TCCamera *camera = (TCCamera*) o;
		Vector right = camera->getRight();
		camera->setRight(right.x, right.y,value);
	}
}*/
};
