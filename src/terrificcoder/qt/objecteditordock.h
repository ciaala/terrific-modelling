/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_DOCKWIDGET_H
#define TERRIFICCODER_DOCKWIDGET_H

#include <QDockWidget>
#include <QGridLayout>
#include <QLabel>
#include <QDoubleSpinBox>
#include <QReadWriteLock>

#include "qtdock.h"


namespace TerrificCoder {
// template<class T> class VectorRow;

class ObjectEditorDock : public QTDock
{
private:
	Q_OBJECT
// 	QWidget *dockWidgetContents;
// 	QGridLayout *gridLayout;
// 	void addVector(VectorRow<>* row);
// 	std::list<VectorRow<>*> rows;
protected:
	QReadWriteLock lock;	


public:
	explicit ObjectEditorDock(const QString& title, QWidget* parent = 0, Qt::WindowFlags flags = 0);
	explicit ObjectEditorDock(QWidget* parent = 0, Qt::WindowFlags flags = 0);


	virtual void visit(VisitorElement* e);
	virtual void visit(TCObject* e);
	virtual void visit(TCBox* e);
	virtual void visit(TCCone *e);
	virtual void visit(TCCylinder* e);
	virtual void visit(TCSphere* e);
	virtual void visit(TCThorus* e);
	virtual void visit(TCCamera *e);
	virtual void visit(TCLight* e);
	virtual void visit(TCPlane* e);
	virtual void visit(TCText2D* e);
	virtual void visit(TCText3D* e);
	virtual void visit(TCArrow* e);
	virtual void visit(TCObjectSet *e);
    virtual void visit(TCTeapot* e);
	virtual ~ObjectEditorDock();	

	void updateObjectDetail(TCObject *object);

signals:

	void signal_object_position_x(double value);
	void signal_object_position_y(double value);
	void signal_object_position_z(double value);

	
	void signal_object_dimension_w(double value);
	void signal_object_dimension_h(double value);
	void signal_object_dimension_d(double value);
	
	void signal_object_rotation_x(int value);
	void signal_object_rotation_y(int value);
	void signal_object_rotation_z(int value);

	void signal_object_type(QString type);
	void signal_object_name(QString name);
	void signal_object_color(QColor color);


		
public slots:

	void object_position_x(double value);
	void object_position_y(double value);
	void object_position_z(double value);
	
	void object_dimension_w(double value);
	void object_dimension_h(double value);
	void object_dimension_d(double value);
	
	void object_rotation_x(int value);
	void object_rotation_y(int value);
	void object_rotation_z(int value);

	void object_name(QString name);
	void object_color(QColor color);

	
// 	void empty(TerrificCoder::TCObject* o);
	//void addVector(QString label, std::mem_fun_ref_t< TerrificCoder::Vector, TerrificCoder::TCObject > get, std::mem_fun1_ref_t< void, TerrificCoder::TCObject, TerrificCoder::Vector > set, double min, double max);
	
};

/*
class DoubleObserver {
public:
	virtual ~DoubleObserver();
	virtual void handleValueChanged(double value) = 0;
};


class QHandleChange: public QObject {
	Q_OBJECT
	DoubleObserver *dObs;
public :
	explicit QHandleChange(DoubleObserver *dObs,QObject* parent = 0);
public slots:
	void handleValueChange(double value);
};


template<class T>
class VectorRow : public DoubleObserver {

	//typedef std::mem_fun_ref_t< TerrificCoder::Vector,T > vectorGetter;
	//typedef std::mem_fun1_ref_t< void, T, TerrificCoder::Vector > vectorSetter;
private:

	QLabel label;
	QDoubleSpinBox v[3];
	std::mem_fun_ref_t< TerrificCoder::Vector,T > vectorGetter;
	std::mem_fun1_ref_t< void, T, TerrificCoder::Vector > vectorSetter;
	
	T* obj;
public:
	VectorRow(QWidget* content, QGridLayout* layout, QString label, T* obj,
			  std::mem_fun_ref_t< TerrificCoder::Vector,T> get,
			  std::mem_fun1_ref_t< void, T, TerrificCoder::Vector> set,
			  double min =-999999, double max=999999, double step=1);
	virtual void handleValueChanged(double value);
	
	
};*/



}

#endif // TERRIFICCODER_DOCKWIDGET_H
