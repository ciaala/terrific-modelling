/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_QTDOCK_H
#define TERRIFICCODER_QTDOCK_H

#include "../patterns/visitor.h"
#include "../patterns/observer.h"
#include <QDockWidget>
#include "../model/tcsimpleobjects.h"
#include "../model/tccamera.h"
#include "../model/tclight.h"
#include "../model/tctext.h"
#include "../model/teapot.h"

#include "../scene/selectionbox.h"
#include "../scene/objectset.h"


namespace TerrificCoder
{

class QTDock : public TerrificCoder::Visitor, public TerrificCoder::Observer, public QWidget
{
	
public:
        explicit QTDock ( const QString& title, QWidget* parent = 0, Qt::WindowFlags flags = 0 );
        explicit QTDock ( QWidget* parent = 0, Qt::WindowFlags flags = 0 );
        virtual void update ( Subject* subject );
		virtual void blockChildren(bool status);
};

}

#endif // TERRIFICCODER_QTDOCK_H
