/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_LIGTHEDITORDOCK_H
#define TERRIFICCODER_LIGTHEDITORDOCK_H


#include <QReadWriteLock>

#include "qtdock.h"
#include "../model/tcsimpleobjects.h"
#include "../model/tccamera.h"
#include "../model/tclight.h"


namespace TerrificCoder {
 
class LightEditorDock : public QTDock
{
private:
	Q_OBJECT
	void updateLightDetail(TCLight* light);
protected:
	QReadWriteLock lock;
public:
	explicit LightEditorDock(const QString& title, QWidget* parent = 0, Qt::WindowFlags flags = 0);
	explicit LightEditorDock(QWidget* parent = 0, Qt::WindowFlags flags = 0);
	
    virtual ~LightEditorDock();

public: 
	virtual void visit(VisitorElement* e);
	virtual void visit(TCObject* e);
	virtual void visit(TCBox* e);
	virtual void visit(TCCone *e);
	
	virtual void visit(TCCylinder* e);
	virtual void visit(TCSphere* e);
	virtual void visit(TCThorus* e);
	virtual void visit(TCCamera *e);
	virtual void visit(TCLight* e);
	virtual void visit(TCPlane* e);
	virtual void visit(TCText2D* e);
	virtual void visit(TCText3D* e);
	virtual void visit(TCArrow* e);
    virtual void visit(TCObjectSet* e);
	virtual void visit(TCTeapot *e);
public slots:
	void light_ambient(QColor color);
	void light_diffuse(QColor color);
	void light_specular(QColor color);
	void light_spot(QColor color);
	void light_spot_exponent(double value);
	void light_spot_cutoff(double value);
	void light_spot_direction_x(double value);
	void light_spot_direction_y(double value);
	void light_spot_direction_z(double value);
 
signals:	
	void signal_light_ambient(QColor color);
	void signal_light_diffuse(QColor color);
	void signal_light_specular(QColor color);
	void signal_light_spot(QColor color);
	void signal_light_spot_exponent(double value);
	void signal_light_spot_cutoff(double value);
	void signal_light_spot_direction_x(double value);
	void signal_light_spot_direction_y(double value);
	void signal_light_spot_direction_z(double value);
};

};

#endif // TERRIFICCODER_LIGTHEDITORDOCK_H
