/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <QtGui>
#include <QtOpenGL>
#include <math.h>
#include <stdarg.h>

#include "tcglwidget.h"
#include "../terrificcoder.h"
#include "../container.h"
#define TCLOGGING_LEVEL TCERR_LEVEL
#include "../library/logging.h"
// #include "../library/tclibrary.h"
// #include "../scene/scene.h"
//#include "modeltest/modeltest.h"

//#define MODEL_TEST 1

namespace TerrificCoder {

TCGLWidget::TCGLWidget(QWidget *parent) : QGLWidget(QGLFormat(QGL::DoubleBuffer), parent ) ,Observer("TCGLWidget"), lock(QReadWriteLock::Recursive) 
{
    this->inputContext = Container::getInputContext();
    this->renderer = Container::getRenderer();
	Container::setWidget(this);
    Container::attachSelectedObject(inputContext);
    Container::getScene()->attach(this);

    this->fullScreen = false;
    timer =  this->startTimer(32);
    if ( timer == 0 ) {
        qDebug() << "Timer Error: unable to start a timer object";
    } else {
        qDebug() << "Timer Started";
    }
	this->firstStart = true;
}

TCGLWidget::~TCGLWidget()
{
    /*if(container)
    	delete container;*/
}

void TCGLWidget::timerEvent(QTimerEvent* event) {
	QWriteLocker locker(&this->lock );
    if (event->timerId() == timer ) {
        Container::timerEvent();
        //Container::timerEvent();
        //container->timerEvent();
        //	paintGL();
		this->updateGL();
    }

}
void TCGLWidget::update(Subject *)
{
//	TCDEBUG << "SUBJECT";
    this->updateGL();
}

void TCGLWidget::initializeGL()
{
    renderer->initializeGL();
}

void TCGLWidget::resizeGL(int w, int h)
{
    renderer->resizeGL(w, h);
	this->updateGL();
}

void TCGLWidget::paintGL()
{

    QWriteLocker locker(&this->lock);
	//glFlush();

	if ( firstStart ) {
		renderer->resizeGL(this->width(), this->height());
		firstStart = false;
	}
    Container::getRenderer()->paintGL();
	//glFlush();
//     GLenum error = glGetError();
//     if (error != GL_NO_ERROR ) {
//         while ( error != GL_NO_ERROR ) {
//             std::cerr << "[GL_ERRROR]: " << error << std::endl;
//             error = glGetError();
//         }
//     }
    this->swapBuffers();
}

void TCGLWidget::wheelEvent(QWheelEvent* event)
{
    QWriteLocker locker(&this->lock);
    Orientation orientation = TerrificCoder::NoneOrientation;
    GLint delta = (GLfloat)event->delta();
    if (event->orientation() == Qt::Horizontal) {
        orientation = TerrificCoder::Orizontal;
    } else {
        orientation = TerrificCoder::Vertical;
    }
    this->inputContext->mouseWheelEvent(-delta,orientation);
	this->updateGL();
//    this->paintGL();
    event->accept();
}


void TCGLWidget::mousePressEvent(QMouseEvent* event)
{
    QWriteLocker locker(&this->lock);
    MouseButton button = TerrificCoder::NoneButton;
    if (event->buttons() & Qt::RightButton) {
        button = RightButton;
    }
    if (event->buttons() & Qt::LeftButton) {
        button = LeftButton;
    }
    if (event->buttons() & Qt::MiddleButton) {
        button = MiddleButton;
    }
    inputContext->mousePressEvent(event->x(), event->y(), button);
	this->updateGL();
 //   this->paintGL();
    event->accept();
}

void TCGLWidget::mouseMoveEvent(QMouseEvent* event)
{
    QWriteLocker locker(&this->lock);
    MouseButton button = TerrificCoder::NoneButton;
    if (event->buttons() & Qt::RightButton) {
        button = RightButton;
    } else if (event->buttons() & Qt::LeftButton) {
        button = LeftButton;
    } else if (event->buttons() & Qt::MiddleButton) {
        button = MiddleButton;
    }
    inputContext->mouseTrackEvent(event->x(), event->y(),button);
	this->updateGL();
//    this->paintGL();
    event->accept();
}

void TCGLWidget::mouseReleaseEvent(QMouseEvent* event) {
    QWriteLocker locker(&this->lock);
    MouseButton button = TerrificCoder::NoneButton;
    if (event->buttons() & Qt::RightButton) {
        button = RightButton;
    } else if (event->buttons() & Qt::LeftButton) {
        button = LeftButton;
    } else if (event->buttons() & Qt::MiddleButton) {
        button = MiddleButton;
    }
    inputContext->mouseReleaseEvent(event->x(), event->y(),button);
	this->updateGL();
    event->accept();

}

void TCGLWidget::keyPressEvent(QKeyEvent* event)
{
    QWriteLocker locker(&this->lock);
    switch ( event->key() )  {

    case Qt::Key_Right: {
        inputContext->keyPress(Right);
        event->accept();
        break;
    }
    case Qt::Key_Left: {
        inputContext->keyPress(Left);
        event->accept();
		break;
    }
    case Qt::Key_Up: {
        inputContext->keyPress(Up);
        event->accept();
		break;
    }
    case Qt::Key_Down: {
        inputContext->keyPress(Down);
        event->accept();
		break;
    }
    case Qt::Key_F: {
        this->fullScreen = this->fullScreen ? false: true;
        if ( fullScreen )  {
            this->window()->setWindowState(Qt::WindowFullScreen);
        } else {
            this->window()->setWindowState(Qt::WindowActive);
        }
        event->accept();
		break;
    }
    case Qt::Key_Escape : {
        inputContext->keyPress(ESC);
        event->accept();
		break;
    }
	case Qt::Key_Delete : {
		inputContext->keyPress(CANC);
		event->accept();
		break;
	} case Qt::Key_Backspace : {
		inputContext->keyPress(BACKSPACE);
		event->accept();
		break;
	}  case Qt::Key_R: {
        inputContext->keyPress(KEY_R);
        event->accept();
		break;
    }
    case Qt::Key_N: {
        inputContext->keyPress(KEY_N);
        event->accept();
		break;
    }
    case Qt::Key_V : {
        inputContext->keyPress(KEY_V);
        event->accept();
		break;
    }
	case Qt::Key_P : {
		inputContext->keyPress(KEY_P);
		event->accept();
		break;
	}

	
	case Qt::Key_Q : {
		inputContext->keyPress(KEY_Q);
		event->accept();
		break;
	}
	case Qt::Key_A : {
		inputContext->keyPress(KEY_A);
		event->accept();
		break;
	}
	case Qt::Key_W : {
		inputContext->keyPress(KEY_W);
		event->accept();
		break;
	}
	case Qt::Key_S : {
		inputContext->keyPress(KEY_S);
		event->accept();
		break;
	}
	case Qt::Key_E : {
		inputContext->keyPress(KEY_E);
		event->accept();
		break;
	}
	case Qt::Key_D : {
		inputContext->keyPress(KEY_D);
		event->accept();
		break;
	}
	case Qt::Key_Y : {
		inputContext->keyPress(KEY_Y);
		event->accept();
		break;
	}
	case Qt::Key_H : {
		inputContext->keyPress(KEY_H);
		event->accept();
		break;
	}
	case Qt::Key_U : {
		inputContext->keyPress(KEY_U);
		event->accept();
		break;
	}
	case Qt::Key_J : {
		inputContext->keyPress(KEY_J);
		event->accept();
		break;
	}
	case Qt::Key_I : {
		inputContext->keyPress(KEY_I);
		event->accept();
		break;
	}
	case Qt::Key_K : {
		inputContext->keyPress(KEY_K);
		event->accept();
		break;
	}
	case Qt::Key_C : {
		inputContext->keyPress(KEY_C);
		event->accept();
		break;
	}
    default:
        QWidget::keyReleaseEvent(event);

    }
    this->updateGL();
}

void TCGLWidget::keyReleaseEvent(QKeyEvent* event)
{
    QWriteLocker locker(&this->lock);
    switch ( event->key() )  {
    case Qt::Key_Right: {
        inputContext->keyRelease(Right);
        event->accept();
        return;
    }
    case Qt::Key_Left: {
        inputContext->keyRelease(Left);
        event->accept();
        return;
    }
    case Qt::Key_Up: {
        inputContext->keyRelease(Up);
        event->accept();
        return;
    }
    case Qt::Key_Down: {
        inputContext->keyRelease(Down);
        event->accept();
        return;
    }
	case Qt::Key_R: {
		inputContext->keyRelease(KEY_R);
		event->accept();
		return;
	}
	case Qt::Key_N: {
		inputContext->keyRelease(KEY_N);
		event->accept();
		return;
	}
	case Qt::Key_V : {
		inputContext->keyRelease(KEY_V);
		event->accept();
		return;
	}
    default:
        QWidget::keyReleaseEvent(event);
    }
    this->updateGL();
}

void TCGLWidget::acquireBigLock()
{
	this->bigLocker = new QWriteLocker(&this->lock);	
}
void TCGLWidget::releaseBigLock()
{
	delete this->bigLocker;
}

void TCGLWidget::setMouseAt(int x, int y)
{
	QPoint c = QWidget::mapToGlobal(QPoint(x,y));
	QCursor::setPos(c);
}


};

