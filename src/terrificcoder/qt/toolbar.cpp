/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "toolbar.h"
#include <qaction.h>

#include <QDebug>
#include "../container.h"
#include "../command/newobject.h"
#include "../command/selectcamera.h"
#include "../command/newsimplescene.h"
#include "../command/newselectionscene.h"
#include "../command/clearscene.h"
#include "../command/inputswitch.h"
#include "../command/renderoption.h"

namespace TerrificCoder
{
Toolbar::Toolbar(const QString& title, QWidget* parent): QToolBar(title, parent)
{
	setupActions();
}

Toolbar::Toolbar(QWidget* parent): QToolBar(parent)
{
	setupActions();
}
Toolbar::Toolbar(const QToolBar& ): QToolBar()
{
	setupActions();
}
Toolbar::~Toolbar()
{
	QList<Command*> values =  actionMap.values();
	actionMap.clear();
	while ( !values.isEmpty() ){
		Command *c = values.front();
		values.pop_front();
		delete c;
	}
}

void Toolbar::setupActions()
{
	label = new QLabel(this->windowTitle() , this);
	
	addWidget(label);
	label->show();
	
	actionMap["newBox"] = new NewObject(Box);
	actionMap["newCone"] = new NewObject(Cone);
	actionMap["newCylinder"] = new NewObject(Cylinder);
	actionMap["newSphere"] = new NewObject(Sphere);
	actionMap["newThorus"] = new NewObject(Thorus);
	actionMap["newLight"] = new NewObject(Light);
	actionMap["newQuaternionCamera"] = new NewObject(QuaternionCamera);
	actionMap["newPlane"] = new NewObject(Plane);
	actionMap["newText2D"] = new NewObject(Text2D);
	actionMap["newText3D"] = new NewObject(Text3D);
	actionMap["newArrow"] = new NewObject(Arrow);
	actionMap["newTeapot"] = new NewObject(Teapot);
	
	actionMap["selectCamera"] = new SelectCamera();

	
	actionMap["newSimpleScene"] = new NewSimpleScene();
	actionMap["newSelectionScene"] = new NewSelectionScene();
	actionMap["clearScene"] = new ClearScene();

	
 	actionMap["actionRotate"] = new InputSwitch(InputRotation);
	actionMap["actionSelect"] = new InputSwitch(InputSelect);
	actionMap["actionFree"] = new InputSwitch(InputFree);
	actionMap["actionSelectionVolume"] = new InputSwitch(InputSelectionVolume);
	
 	actionMap["actionAxis_x"] = new RendererOption(AXIS_X);
	actionMap["actionAxis_y"] = new RendererOption(AXIS_Y);
	actionMap["actionAxis_z"] = new RendererOption(AXIS_Z);
	
	actionMap["actionXY"] = new RendererOption(PLANE_XY);
	actionMap["actionYZ"] = new RendererOption(PLANE_YZ);
	actionMap["actionZX"] = new RendererOption(PLANE_ZX);
}


void Toolbar::actionToInput(QAction* action)
{
	Command* command = actionMap[action->objectName()];	
	if ( command != NULL) {
		qDebug() << "Toolbar Handler [VALID]:" << action->objectName();
		Container::addCommand( command );
	} else {
		qDebug() << "Toolbar Handler [INVALID]:" << action->objectName();
	}
}

};

