/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.



*/

#ifndef TCGLWIDGET_H
#define TCGLWIDGET_H

#include <qgl.h>

#include <QGLWidget>
#include <Qt/qplaintextedit.h>
#include <Qt/qreadwritelock.h>
#include <map>
#include <QAbstractItemModel>
#include <qreadwritelock.h>
#include <QStringListModel>

#include "../input/inputcontext.h"




#include "tctreewidget.h"
#include "../renderer/glrenderer.h"

using namespace std;
namespace TerrificCoder {

class InputContext;
class GLRenderer;

class TCGLWidget : public QGLWidget, public Observer
{
	
	Q_OBJECT
    


private:
	InputContext *inputContext;
	GLRenderer *renderer;
	bool fullScreen;
	bool firstStart;


protected:

	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL();
	QReadWriteLock lock;
public:
	
	TCGLWidget(QWidget *parent = 0);
	~TCGLWidget();

	GLint getViewportHeight();
	GLint getViewportWidth();
	GLfloat getViewportAspect();
	void acquireBigLock();
	void releaseBigLock();
    virtual void update(Subject *subject);

	virtual void setMouseAt( int x, int y);
public slots:

protected:
	virtual void keyPressEvent(QKeyEvent* event);
    virtual void keyReleaseEvent(QKeyEvent* event);
	virtual void mousePressEvent(QMouseEvent* event);
	virtual void mouseReleaseEvent(QMouseEvent* event);
	virtual void mouseMoveEvent(QMouseEvent* event);
	virtual void wheelEvent(QWheelEvent* event);
	virtual void timerEvent(QTimerEvent* event);

private:

	int timer;
    TCScene* scene;
    QWriteLocker* bigLocker;

};
};
#endif
