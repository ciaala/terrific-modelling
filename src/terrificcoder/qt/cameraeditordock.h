/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICCODER_CAMERAEDITORDOCK_H
#define TERRIFICCODER_CAMERAEDITORDOCK_H

#include "qtdock.h"
#include <QReadWriteLock>

#include "../model/tcsimpleobjects.h"
#include "../model/tccamera.h"
#include "../model/tclight.h"

namespace TerrificCoder {

class CameraEditorDock: public QTDock
{
private:
	Q_OBJECT  
	void updateCameraDetail(TCCamera* camera);

protected:
	QReadWriteLock lock;
public:
    explicit CameraEditorDock(const QString& title, QWidget* parent = 0, Qt::WindowFlags flags = 0);
    explicit CameraEditorDock(QWidget* parent = 0, Qt::WindowFlags flags = 0);
	
public:
    virtual void visit(TerrificCoder::TCText3D* e);
    virtual void visit(TerrificCoder::TCText2D* e);
    virtual void visit(TerrificCoder::TCLight* e);
    virtual void visit(TerrificCoder::TCCamera* e);
    virtual void visit(TerrificCoder::TCThorus* e);
    virtual void visit(TerrificCoder::TCSphere* e);
    virtual void visit(TerrificCoder::TCCone* e);
    virtual void visit(TerrificCoder::TCCylinder* e);
    virtual void visit(TerrificCoder::TCBox* e);
    virtual void visit(TerrificCoder::TCObject* e);
    virtual void visit(TerrificCoder::VisitorElement* e);
    virtual void visit(TCPlane* e);
    virtual void visit(TCArrow* e);
    virtual void visit(TCObjectSet* e);
	virtual void visit(TCTeapot *e);

// public slots:
// 	void center_x(double value);
// 	void center_y(double value);
// 	void center_z(double value);
// 	void up_x(double value);
// 	void up_y(double value);
// 	void up_z(double value);
// 	void right_x(double value);
// 	void right_y(double value);
// 	void right_z(double value);
// signals:
// 	void signal_center_x(double value);
// 	void signal_center_y(double value);
// 	void signal_center_z(double value);
// 	void signal_up_x(double value);
// 	void signal_up_y(double value);
// 	void signal_up_z(double value);
// 	void signal_right_x(double value);
// 	void signal_right_y(double value);
// 	void signal_right_z(double value);
};

}

#endif // TERRIFICCODER_CAMERAEDITORDOCK_H
