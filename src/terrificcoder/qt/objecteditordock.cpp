/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <functional>
#include <utility>
#include "objecteditordock.h"
#include "../container.h"
#include <QDoubleSpinBox>
#define TCLOGGING_LEVEL TCERR_LEVEL
#include "../library/logging.h"
namespace TerrificCoder {

ObjectEditorDock::ObjectEditorDock(const QString& title, QWidget* parent, Qt::WindowFlags flags): QTDock(title, parent, flags) {
    Container::attachSelectedObject(this);
}
ObjectEditorDock::ObjectEditorDock(QWidget* parent, Qt::WindowFlags flags): QTDock(parent, flags) {
    Container::attachSelectedObject(this);
}
ObjectEditorDock::~ObjectEditorDock() {}


void ObjectEditorDock::visit(TCObject* e)
{
//    TCERR << "Visitor TCObject *" << e->getName() << std::endl;
}

void ObjectEditorDock::visit(TCBox* e)   { this->updateObjectDetail(e); }
void ObjectEditorDock::visit(TCCone* e)  { this->updateObjectDetail(e); }
void ObjectEditorDock::visit(TCCamera* e)  { this->updateObjectDetail(e); }
void ObjectEditorDock::visit(TCCylinder* e) { this->updateObjectDetail(e); }
void ObjectEditorDock::visit(TCSphere* e)  { this->updateObjectDetail(e); }
void ObjectEditorDock::visit(TCThorus* e)  { this->updateObjectDetail(e); }
void ObjectEditorDock::visit(TCLight* e)  { this->updateObjectDetail(e); }
void ObjectEditorDock::visit(TCPlane* e)  { this->updateObjectDetail(e); }
void ObjectEditorDock::visit(TCText2D* e)  { this->updateObjectDetail(e); }
void ObjectEditorDock::visit(TCText3D* e)  { this->updateObjectDetail(e); }
void ObjectEditorDock::visit(TCArrow* e)   { this->updateObjectDetail(e); }
void ObjectEditorDock::visit(TCObjectSet* e)   { this->updateObjectDetail(e); }
void ObjectEditorDock::visit(TCTeapot *e) { this->updateObjectDetail(e); }
void ObjectEditorDock::visit(VisitorElement* )
{
    TCERR << "Visitor Element " << std::endl;
}


void ObjectEditorDock::updateObjectDetail(TCObject *object)
{
    QReadWriteLock(lock);
    if ( object != NULL) {
        this->blockChildren(true);
		
        // POSITION
        Vector p = object->getPosition();
        emit signal_object_position_x(p.x);
        emit signal_object_position_y(p.y);
        emit signal_object_position_z(p.z);

        Vector r = object->getRotation();
		emit signal_object_rotation_x( r.x*(180.0/M_PI));
		emit signal_object_rotation_y( r.y*(180.0/M_PI));
		emit signal_object_rotation_z( r.z*(180.0/M_PI));

		emit signal_object_dimension_w(object->getWidth());
		emit signal_object_dimension_h(object->getHeight());
		emit signal_object_dimension_d(object->getDepth());
		
        GLdouble *color = object->getColor();

        QColor c;
        c.setRgbF(color[0],color[1],color[2],color[3]);
        emit signal_object_color(c);

        emit signal_object_name(QString().fromStdString(object->getName()));
        emit signal_object_type(QString().fromStdString(enumToString(object->getType())));
        this->blockChildren(false);
    }
}

void ObjectEditorDock::object_color(QColor color)
{
    if (Container::getSelectedObject() ) {
        Container::getSelectedObject()->setColor(color.red(),color.green(),color.blue(),color.alpha());
        Container::notifySelectedUpdate();
    }
}
void ObjectEditorDock::object_name(QString name)
{
    if (Container::getSelectedObject() ) {
        Container::getSelectedObject()->setName(name.toStdString());
        Container::notifySelectedUpdate();
    }
}
void ObjectEditorDock::object_position_x(double value)
{
    if (Container::getSelectedObject() ) {
        Vector v = Container::getSelectedObject()->getPosition();
        v.x = value;
        Container::getSelectedObject()->setPosition(v);
        Container::notifySelectedUpdate();
    }
}
void ObjectEditorDock::object_position_y(double value)
{
    if (Container::getSelectedObject() ) {
        Vector v = Container::getSelectedObject()->getPosition();
        v.y = value;
        Container::getSelectedObject()->setPosition(v);
        Container::notifySelectedUpdate();
    }
}

void ObjectEditorDock::object_position_z(double value)
{
    if (Container::getSelectedObject() ) {
        Vector v = Container::getSelectedObject()->getPosition();
        v.z = value;
        Container::getSelectedObject()->setPosition(v);
        Container::notifySelectedUpdate();
    }
}


void ObjectEditorDock::object_dimension_w(double value)
{
	if (Container::getSelectedObject() ) {
		Container::getSelectedObject()->setWidth(value);
		Container::notifySelectedUpdate();
	}
}
void ObjectEditorDock::object_dimension_h(double value)
{
	if (Container::getSelectedObject() ) {
		Container::getSelectedObject()->setHeight(value);
		Container::notifySelectedUpdate();
	}
}

void ObjectEditorDock::object_dimension_d(double value)
{
	if (Container::getSelectedObject() ) {
		Container::getSelectedObject()->setDepth(value);
		Container::notifySelectedUpdate();
	}
}

void ObjectEditorDock::object_rotation_x(int value)
{
    if (Container::getSelectedObject() ) {
//         Vector r = Container::getSelectedObject()->getRotation();
//         r.x = value;
        Container::getSelectedObject()->setRotation(value,-1,-1);
        Container::notifySelectedUpdate();
    }
}
void ObjectEditorDock::object_rotation_y(int value)
{
    if (Container::getSelectedObject() ) {
//         Vector r = Container::getSelectedObject()->getRotation();
//         r.y = value;
        Container::getSelectedObject()->setRotation(-1,value,-1);
        Container::notifySelectedUpdate();
    }
}

void ObjectEditorDock::object_rotation_z(int value)
{
    if (Container::getSelectedObject() ) {
//         Vector r = Container::getSelectedObject()->getRotation();
//         r.z = value;
        Container::getSelectedObject()->setRotation(-1,-1,value);
        Container::notifySelectedUpdate();
    }
}

// void ObjectEditorDock::empty(TCObject *o)
// {
// 	this->children().empty();
// 	this->dockWidgetContents = new QWidget();
// 	this->gridLayout = new QGridLayout(this->dockWidgetContents);
// 	this->gridLayout->setSpacing(0);
// 	this->gridLayout->setObjectName(QString("editorGridLayout"));
// 	this->setWindowTitle(QString().fromStdString(o->getName()));
// // 	std::mem_fun_ref_t<Vector,TCObject> get = std::mem_fun_ref(&TCObject::getPosition);
// // 	std::mem_fun1_ref_t<void,TCObject,Vector > set = std::mem_fun_ref(&TCObject::setPosition);
// // 	this->addVector("Position", get, set);
// // 	get = std::mem_fun(&TCObject::getRotation);
// // 	set = std::mem_fun(&TCObject::setRotation);
// // 	this->addVector("Rotation",o,o->getRotation,o->setRotation);
// // 	this->addVector("Color",o,o->getColor,o->setColor);
//
//
//
// 	VectorRow* v = new VectorRow(dockWidgetContents,gridLayout,
// 								 "Position", o, &TCObject::getRotation, &TCObject::setRotation);
// 	addVector(v);
// }
//






/*



VectorRow::VectorRow(QWidget *content, QGridLayout *layout, QString label, T *obj,
					 std::mem_fun_ref_t< TerrificCoder::Vector,T> get,
					 std::mem_fun1_ref_t< void, T, TerrificCoder::Vector> set,
					 double min, double max)
{
	int row = gridLayout->rowCount();
	t = make_pair(set,get);
	QLabel *l = new QLabel(label,dockWidgetContents);
	this->gridLayout->addWidget(l,row,0,1,1);
	QDoubleSpinBox *d = new QDoubleSpinBox(dockWidgetContents);
	d->setMinimum(min);
	d->setMaximum(max);
	connect(d,"valueChanged",this,"vectorHandler");
	handler_map->(d,t);
	this->gridLayout->addWidget(d,row,1,1,1);

	this->gridLayout->addWidget(
}


void VectorRow::handleValueChanged(double value)
{
	Vector v( this->v[0].value(),this->v[1].value(),this->v[2].value());
	set(object,v);
}






QHandleChange::QHandleChange(DoubleObserver* dObs, QObject* parent): QObject(dObs, parent) {
	this->dObs = dObs;
}
DoubleObserver::~DoubleObserver(){ }

void QHandleChange::handleValueChange(double value) {
	this->dObs->handleValueChanged(value);
}*/

};
