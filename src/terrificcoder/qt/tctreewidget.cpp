/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#define QT_NO_DEBUG_OUTPUT
#include <QtCore/QtCore>
#include <QtGlobal>

#include "tctreewidget.h"
#include "../container.h"
#include "../command/selectobject.h"


namespace TerrificCoder{

/**
 * TREE WIDGET
 *
 */
TCTreeWidget::TCTreeWidget(QWidget* parent): QTreeView(parent)
{
	this->treeModel = new TCTreeModel(this);
	this->setModel(treeModel);
}
void TCTreeWidget::clickedObject(QModelIndex index)
{
	TCTreeItem* item = treeModel->itemForIndex( index );
	if ( item->getType() == Object ) {
		TCObjectItem *oitem = static_cast<TCObjectItem*>(item);
		TCObject *object = oitem->getObject();
		Container::addCommand(new SelectObject(object));
	}

}

TCTreeWidget::~TCTreeWidget()
{
	if ( treeModel != NULL )
		delete treeModel;
}
bool TCTreeWidget::isExpanded(const QModelIndex& index) const
{
	return this->treeModel->isExpanded(index);
}

void TCTreeWidget::setExpanded(const QModelIndex& index, bool expand)
{
	this->treeModel->setExpanded(index,expand);
}

/**
 *	TREE MODEL
 *
 */

TCTreeModel::TCTreeModel(QObject* parent): QAbstractItemModel(parent)
{
	this->scene = Container::getScene();
	this->scene->attach(this);
	this->sceneItem = new TCSceneItem("Root");
// 	this->enumList.push_back(Any);
// 	this->enumList.push_back(Box);
// 	this->enumList.push_back(Cone);
// 	this->enumList.push_back(Cylinder);
// 	this->enumList.push_back(Sphere);
// 	this->enumList.push_back(Thorus);
// 	this->enumList.push_back(Light);
// 	this->enumList.push_back(QuaternionCamera);

	sceneItem->addChild( new TCFoldItem(Box,sceneItem));
	sceneItem->addChild( new TCFoldItem(Cone,sceneItem));
	sceneItem->addChild( new TCFoldItem(Cylinder,sceneItem));
	sceneItem->addChild( new TCFoldItem(Sphere,sceneItem));
	sceneItem->addChild( new TCFoldItem(Thorus,sceneItem));
	sceneItem->addChild( new TCFoldItem(Light,sceneItem));
	sceneItem->addChild( new TCFoldItem(Plane,sceneItem));
	sceneItem->addChild( new TCFoldItem(Text2D,sceneItem));
	sceneItem->addChild( new TCFoldItem(Text3D,sceneItem));
	sceneItem->addChild( new TCFoldItem(Arrow,sceneItem));
	sceneItem->addChild( new TCFoldItem(Teapot,sceneItem));
	sceneItem->addChild( new TCFoldItem(QuaternionCamera,sceneItem));
	this->loadScene();
}
TCTreeModel::~TCTreeModel()
{
	
}
void TCTreeModel::update(Subject *subject)
{
	QWriteLocker locker(&lock);
	beginResetModel();
	loadScene();
	endResetModel();
	
}
QVariant TCTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role != Qt::DisplayRole ){
		return QVariant();
	}
	if( orientation == Qt::Horizontal ) {
		switch(section){
			case 0: return QString("Type");
			case 1: return QString("ID");
			case 2: return QString("Pippo");
			default: Q_ASSERT(false);
		}
	}
	return section + 1;
}

void TCTreeModel::loadScene() {
	
//	qDebug() << "Loading Scene";

	int i = 0;
	for( i=0; i<sceneItem->rowCounts(); i++){
		TCFoldItem* fold = static_cast<TCFoldItem*>( sceneItem->getChildByRow(i));
	
		fold->clear();
		SceneIterator *j = scene->iterator(fold->getEnumType());
//		qDebug() << "Listing type " << fold->getEnumType() << " counted: " << scene->countObject(fold->getEnumType());
		while(j->current() != NULL){
			TCTreeItem *item = new TCObjectItem (j->current(),fold);
//			qDebug() << "-item " << QString().fromStdString(j->current()->getName()) << " " << j->current()->getId();
			fold->addChild(item);
			j->next();
		}
		delete j;
	}

}

int TCTreeModel::columnCount(const QModelIndex& index) const
{
	if ( index.isValid() && index.column() != 0) {
//		qDebug() << "Column Count [INVALID]: ("<<index.row() << "," <<index.column() <<")";
		return 0;
	}
	TCTreeItem* item = itemForIndex(index);
	if (item != NULL){
		int max = 1;
		
		std::vector< TCTreeItem* >* _v = item->getChildren();
		std::vector< TCTreeItem* >::iterator i = _v->begin();
		while (i != _v->end() ) {
			int count = (*i)->columnCount();
			max = count > max ? count : max;
			++i;
		}
//		qDebug() << "Column Count [VALID]:"<<item->getName() << "("<<index.row() << "," <<index.column() << ")" << max;
		return max;
	}
	return 2;
}
QVariant TCTreeModel::data(const QModelIndex& index, int role) const
{
	
	if (index.isValid()) {
		TCTreeItem* item = itemForIndex(index);
		if (item != NULL && role == Qt::DisplayRole){
//			qDebug() << "Data Request [ROLE DISPLAY]:" << index.row() << "," << index.column() << item->getName();
			if ( index.column() == 0) {
				return item->getName();
			}
			if ( index.column() == 1 && item->getType() == Object) {
				return static_cast<TCObjectItem*>(item)->getId();
			}
		} else {
//			qDebug() << "Data Request [ROLE INVALID]:" << index.row() << "," <<index.column() << item->getName();;
			return QVariant();
		}
		
	}
//	qDebug() << "Data Request [ROLE INVALID]: "<< index.row() << "," <<index.column();
	return QVariant();
}
bool TCTreeModel::hasChildren(const QModelIndex& parent) const
{
	if ( parent.isValid() ) {
		TCTreeItem* item = itemForIndex(parent);
		if ( item->getType() == Scene)
			true;
		else if ( item->getType() == Folder )
			return item->rowCounts() > 0 ? true : false;
		return false;
	}
	return true;
}
QModelIndex TCTreeModel::index(int row, int column, const QModelIndex& parent) const
{
	
	if ( row < 0 || column < 0 || column > 2 || (parent.isValid() && parent.column() != 0)) {
//		qDebug() << "Index [INVALID]:" << row << "," << column;
		return QModelIndex();
	}
	TCTreeItem* parentItem = itemForIndex(parent);
	if (parentItem != NULL && row < parentItem->rowCounts()){
//		qDebug() << "Index [VALID]:" << parentItem->getName() << row << "," << column;
		return createIndex(row, column, (void*)parentItem->getChildByRow(row));
	}
//	qDebug() << "Index [Not Found]:" << row << "," << column;
	return QModelIndex();
}

Qt::ItemFlags TCTreeModel::flags(const QModelIndex& index) const
{
	Qt::ItemFlags theFlags = QAbstractItemModel::flags(index);
	if ( index.isValid() ){
		theFlags |= Qt::ItemIsSelectable | Qt::ItemIsEnabled;
	}
	return theFlags;
}

QModelIndex TCTreeModel::parent(const QModelIndex& child) const
{
	
	TCTreeItem* childItem = itemForIndex(child);
	if ( childItem ) {
		TCTreeItem* parent = childItem->getParent();
		if (parent == sceneItem || parent == NULL){
			return QModelIndex();
		}
		TCTreeItem* grandParent = parent->getParent();
		int row = grandParent->getRowByChild(parent);
		return createIndex(row, 0, parent);
	}
	return QModelIndex();
}


int TCTreeModel::rowCount(const QModelIndex& parent) const
{

	if (parent.isValid() && parent.column() != 0) {
//		qDebug() << "Counting Row [INVALID]:" << 0;
		return 0;
	}
	TCTreeItem *item = itemForIndex(parent);
	if ( item != NULL ){
		int count = item ? item->getChildren()->size() : 0;
//		qDebug() << "Counting Row [VALID]:" << item->getName() << count;
		return count;
	}
//	qDebug() << "Counting Row [INVALID]:" << 0;
	return 0;
}

TCTreeItem* TCTreeModel::itemForIndex(const QModelIndex &index) const
{
	if(index.isValid()){
		if (TCTreeItem* item = static_cast<TCTreeItem*> (index.internalPointer()) )
			return item;
	}
	
	return sceneItem;
}

int TCTreeModel::rowForFolder(TCObjectEnum type ) const {
	int j = 0;
	std::list< TCObjectEnum >::const_iterator i = this->enumList.begin();
	while ( i != this->enumList.end() ){
		if ( *i == type )
			return j;
		j++;
		++i;
	}
	return -1;
}

/**
 *	SCENE
 */
TCSceneItem::TCSceneItem(std::string name) : TCTreeItem(Scene)
{
	this->parent = NULL;
	this->name = QString(name.c_str());
}
QString TCSceneItem::getName()
{
	return this->name;
}
/**
 *	FOLDER
 */
TCFoldItem::TCFoldItem(TCObjectEnum type, TCSceneItem* parent) : TCTreeItem(Folder)
{
	this->type = type;
	this->parent = parent;
}

QString TCFoldItem::getName()
{
	return QString(enumToString(this->type).c_str());
}
TCObjectEnum TCFoldItem::getEnumType()
{
	return this->type;
}

/**
 *	OBJECT
 */

TCObjectItem::TCObjectItem(TCObject* object, TCFoldItem* parent) : TCTreeItem(Object)
{
	this->parent = parent;
	this->object = object;
	this->expanded = false;
}

QString TCObjectItem::getName()
{
	return QString(object->getName().c_str());
}
QString TCObjectItem::getId()
{
	return QString().setNum(object->getId());
}
TCObject* TCObjectItem::getObject()
{
	return object;
}

int TCObjectItem::columnCount()
{
	return 2;
}
void TCObjectItem::setExpanded(bool expand)
{
	
}




/**
 * TREE ITEM
 */
TCTreeItem* TCTreeItem::getParent()
{
	return parent;
}
int TCTreeItem::columnCount()
{
	return 2;
}

TCTreeItem::~TCTreeItem()
{
// 	std::vector< TCTreeItem* >::const_iterator i = childreen.begin();
// 	while (i!= childreen.end() ){
// 		delete (*i);
// 		++i;
// 	}
	if ( childreen.size() >= 1 ) {
		TCTreeItem* item = childreen.back();
		while ( item ) {
			delete item;
			childreen.pop_back();
			item = childreen.back();
		}
	}
}
TCTreeItem::TCTreeItem(TCTreeType itemType) 
{
	this->itemType = itemType;
	this->expanded = true;
}

TCTreeType TCTreeItem::getType()
{
	return this->itemType;
}

TCTreeItem::TCTreeItem()
{
	this->parent = NULL;
}

void TCTreeItem::addChild(TCTreeItem* item)
{
	this->childreen.push_back(item);
// 	size_t length = this->childreen.size() - this->childreen.capacity();
// 	if ( length >= this->childreen.capacity() ) {
// 		this->childreen.resize(length * 1.5 + 1);
// 	}
// 	this->childreen[length] = item;
}
std::vector<TCTreeItem*>* TCTreeItem::getChildren()
{
	return &(this->childreen);
}

int TCTreeItem::getRowByChild(TCTreeItem* item)
{
	int j = 0;
	std::vector<TCTreeItem*>::iterator i = childreen.begin();
	while ( i != childreen.end() ){
		if ( *i == item ){
			return j;
		}
		++j;
		++i;
	}
	return -1;
}
TCTreeItem* TCTreeItem::getChildByRow(int row)
{
	int size = childreen.size();
	if (row >= 0 && row < size) {
		return this->childreen[row];
	}
	return NULL;
}
int TCTreeItem::rowCounts()
{
	return childreen.size();
}
void TCTreeItem::clear()
{
	while ( !childreen.empty() ) {
		TCTreeItem*& o = childreen.back();
		delete o;
		childreen.pop_back();
	}
	//childreen.clear();
// 	if ( childreen.size() >= 1 ) {
// 		TCTreeItem* item = childreen.back();
// 		while ( item ) {
// 			delete item;
// 			childreen.pop_back();
// 			item = childreen.back();
// 		}
// 	}
}


bool TCTreeModel::isExpanded(const QModelIndex& index) const
{
	if ( index.isValid() ) {
		TCTreeItem *item = itemForIndex(index);
		if ( item ) {
			return item->isExpanded();
		}
	}
	return false;
}

void TCTreeModel::setExpanded(const QModelIndex& index, bool expand)
{
	if ( index.isValid() ) {
		TCTreeItem *item = itemForIndex(index);
		if ( item ) {
			item->setExpanded(expand);
		}
	}
}

bool TCTreeItem::isExpanded() const
{
	return this->expanded;
}
void TCTreeItem::setExpanded(bool expand)
{
	this->expanded = expand;
}

};
