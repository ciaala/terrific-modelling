/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "materialeditordock.h"
#include "../container.h"
#include "../scene/selectionbox.h"
#include "qt_library.h"
#include <qfiledialog.h>
#include "../library/logging.h"

#include <qgl.h>
#include "../model/texturemanager.h"
#include <qlistview.h>
#include "../library/logging.h"

namespace TerrificCoder {

MaterialEditorDock::MaterialEditorDock(const QString& title, QWidget* parent, Qt::WindowFlags flags): QTDock(title, parent, flags)
{
    previewImage = NULL;
    textureList  = NULL;
    Container::attachSelectedObject(this);
    tm = Container::getTextureManager();
    tm->attach(this);
}

MaterialEditorDock::MaterialEditorDock(QWidget* parent, Qt::WindowFlags flags) : QTDock("", parent, flags)
{
    previewImage = NULL;
    textureList  = NULL;
    Container::attachSelectedObject(this);
    tm = Container::getTextureManager();
    tm->attach(this);
}

void MaterialEditorDock::visit(TerrificCoder::TCText3D* e) {
    updateMaterialDetail(e);
}
void MaterialEditorDock::visit(TerrificCoder::TCText2D* e) {
    updateMaterialDetail(e);
}

void MaterialEditorDock::visit(TerrificCoder::TCLight* e) {
    updateMaterialDetail(e);
}
void MaterialEditorDock::visit(TerrificCoder::TCCamera* e) {
    updateMaterialDetail(e);
}

void MaterialEditorDock::visit(TerrificCoder::TCThorus* e) {
    updateMaterialDetail(e);
}

void MaterialEditorDock::visit(TerrificCoder::TCSphere* e) {
    updateMaterialDetail(e);
}

void MaterialEditorDock::visit(TerrificCoder::TCCone* e) {
    updateMaterialDetail(e);
}

void MaterialEditorDock::visit(TerrificCoder::TCCylinder* e) {
    updateMaterialDetail(e);
}

void MaterialEditorDock::visit(TerrificCoder::TCBox* e) {
    updateMaterialDetail(e);
}

void MaterialEditorDock::visit(TerrificCoder::TCObject* e) {
    updateMaterialDetail(e);
}

void MaterialEditorDock::visit(TerrificCoder::VisitorElement* ) {  }
void MaterialEditorDock::visit(TCPlane* e) { updateMaterialDetail(e); }
void MaterialEditorDock::visit(TCArrow* e) { updateMaterialDetail(e); }
void MaterialEditorDock::visit(TCObjectSet* e) { updateMaterialDetail(e); }
void MaterialEditorDock::visit(TCTeapot* e) { updateMaterialDetail(e); }

// void MaterialEditorDock::update(Subject* subject)
// {
//     if ( subject == tm ) {
// 		setupTextureList();
//         return;
//     }
//     TCObject* o =  Container::getSelectedObject();
//     if ( o != NULL ) {
// 		if ( this->isVisible() == false )
// 			this->setVisible(true);
//         o->accept(this);
// 	} else if ( this->isVisible() == true ) {
// 			this->setVisible(false);
//     }
// }

void MaterialEditorDock::updateMaterialDetail ( Material *material )
{
    QReadWriteLock(lock);
    this->invalidateMaterials();

    if ( material != NULL) {
        this->blockChildren(true);
        std::pair<materialIt,materialIt> iterators = material->getMaterials();
        for ( materialIt it = iterators.first; it != iterators.second; it++ ) {
            GLenum type = (*it).first;
            GLfloat *v = (*it).second->v;
            switch ( type ) {
            case GL_AMBIENT: {
                emit signal_material_ambient( qColor( v));
                break;
            }
            case GL_DIFFUSE : {
                emit signal_material_diffuse( qColor( v));
                break;
            }
            case GL_EMISSION: {
                emit signal_material_emission( qColor( v));
                break;
            }
            case GL_SPECULAR: {
                emit signal_material_specular( qColor( v));
                break;
            }
            case GL_SHININESS: {
                emit signal_material_shininess( v[0]);
                break;
            }
            }
        }
        this->blockChildren(false);
    }
}

void MaterialEditorDock::material_ambient ( QColor color )
{
    this->setMaterial(GL_AMBIENT, color);
}
void MaterialEditorDock::material_diffuse ( QColor color )
{
    this->setMaterial(GL_DIFFUSE, color);
}

void MaterialEditorDock::material_emission ( QColor color )
{
    this->setMaterial(GL_EMISSION, color);
}

void MaterialEditorDock::material_shininess ( double value )
{
    TCObject *object =Container::getSelectedObject();
    if ( object ) {
        Vector4f shin(value,0.0,0.0,0.0);
        object->setMaterial(GL_SHININESS, shin);
        Container::notifySelectedUpdate();
    }
}

void MaterialEditorDock::material_specular ( QColor color )
{
    this->setMaterial(GL_SPECULAR,color);
}


void MaterialEditorDock::setMaterial ( GLenum type, QColor color )
{
    TCObject *object = Container::getSelectedObject();
    if ( object ) {
        Vector4f value(color.redF(),color.greenF(), color.blueF(),color.alphaF());
        object->setMaterial(type,value);
        Container::notifySelectedUpdate();
    }
}

void MaterialEditorDock::invalidateMaterials()
{
    emit signal_material_ambient(QColor());
    emit signal_material_diffuse(QColor());
    emit signal_material_emission(QColor());
    emit signal_material_shininess(NAN);
    emit signal_material_specular(QColor());
}
void MaterialEditorDock::loadTexture() {
    QString filename = QFileDialog::getOpenFileName(this,
                       tr("Open Texture"),"~",
                       tr("Texture Files (*.png *.jpg *.bmp)"));
    TCDEBUG << filename.toStdString();
    QImage img = QImage(filename);
    QImage pix = img.copy();
    QPixmap pixmap = QPixmap::fromImage(pix);
    getPreviewImage()->setPixmap(pixmap);
    img = QGLWidget::convertToGLFormat(img);
    GLuint text = tm->registerTexture2D(filename.toStdString(), img.width(), img.height(),img.bits());
	tm->assignTexture(Container::getSelectedObject()->getId(),text);
}
void MaterialEditorDock::setupTextureList()
{
    model.blockSignals(true);

    std::pair<TextureIterator,TextureIterator> textures = tm->getTextureList();
    for ( ;textures.first != textures.second; textures.first++) {
        std::pair<unsigned int, TextureInfo*> e = *(textures.first);
        list.push_back(QString().fromStdString(e.second->filename));
        TCDEBUG << "Textures Element: " << e.second->filename;
    }
    model.blockSignals(false);
    model.setStringList(list);
}

void MaterialEditorDock::assign_texture()
{

}
void MaterialEditorDock::revoke_texture()
{

}
void MaterialEditorDock::unload_texture()
{

}

QLabel* MaterialEditorDock::getPreviewImage()
{
    if ( previewImage == NULL ) {
        previewImage = this->findChild<QLabel*>("preview_texture");
    }
    return previewImage;
}
QListView* MaterialEditorDock::getTextureList()
{
    if ( textureList == NULL ) {
        textureList = textureList->findChild<QListView*>("textureList");
        QItemSelectionModel *m = textureList->selectionModel();
        textureList->setModel(&model);
        delete m;
    }
    return textureList;
}

/*
void MaterialEditorDock::setup ( ) {
    if ( previewImage == NULL || textureList == NULL ) {
        QObjectList list = children().front()->children();
        for ( QList< QObject* >::iterator it = list.begin(); it != list.end(); it++ ) {
            QObject* o = *it;
            if ( o->objectName() == QString("previewImage")) {
                previewImage = (QLabel*) o;
            } else if ( o->objectName() == QString("textureList") ) {
				textureList = (QListView*) o;
            }
        }
    }
}*/
};

