/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Francesco Fiduccia <francesco.fiduccia@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "lighteditordock.h"
#include "../container.h"
#include "../qt/qt_library.h"

namespace TerrificCoder{
LightEditorDock::LightEditorDock(const QString& title, QWidget* parent, Qt::WindowFlags flags): QTDock(title, parent, flags)
{
	Container::attachSelectedObject(this);
}
LightEditorDock::LightEditorDock(QWidget* parent, Qt::WindowFlags flags): QTDock(parent, flags)
{
	Container::attachSelectedObject(this);
}

LightEditorDock::~LightEditorDock() {}

void LightEditorDock::visit(VisitorElement* ) { }
void LightEditorDock::visit(TCObject* ) { }
void LightEditorDock::visit(TCBox* ) { }
void LightEditorDock::visit(TCCone* ) { }
void LightEditorDock::visit(TCCamera* ) { }
void LightEditorDock::visit(TCCylinder* ) { }
void LightEditorDock::visit(TCSphere* ) { }
void LightEditorDock::visit(TCThorus* ) { }
void LightEditorDock::visit(TCPlane* ) {  }
void LightEditorDock::visit(TCArrow* ) {  }
void LightEditorDock::visit(TCObjectSet*) {  }
void LightEditorDock::visit(TCText2D* ) { }
void LightEditorDock::visit(TCText3D* ) { }
void LightEditorDock::visit(TCLight* e) { this->updateLightDetail(e); }
void LightEditorDock::visit(TCTeapot* ) {  }



void LightEditorDock::updateLightDetail(TCLight* light)
{
	QReadWriteLock(lock);
//	this->setVisible(true);
	emit signal_light_ambient(qColor( light->getAmbient() ));
	emit signal_light_diffuse(qColor( light->getDiffuse() ));
	emit signal_light_specular(qColor( light->getSpecular() ));
	emit signal_light_spot(qColor( light->getSpot() ));
	emit signal_light_spot_cutoff(light->getSpotCutoff() );
	emit signal_light_spot_exponent(light->getSpotExponent() );
	GLfloat* d = light->getSpotDirection();
	emit signal_light_spot_direction_x( d[0]);
	emit signal_light_spot_direction_y( d[1]);
	emit signal_light_spot_direction_z( d[2]);
}

void LightEditorDock::light_ambient(QColor color)
{
	TCObject *o = Container::getSelectedObject();
	if ( o->getType() == TerrificCoder::Light ) {
		TCLight *light = (TCLight*) o;
		light->setAmbient(color.red(),color.green(),color.blue(),color.alpha());
	}
}
void LightEditorDock::light_diffuse(QColor color)
{
	TCObject *o = Container::getSelectedObject();
	if ( o->getType() == TerrificCoder::Light ) {
		TCLight *light = (TCLight*) o;
		light->setDiffuse(color.red(),color.green(),color.blue(),color.alpha());
	}
}
void LightEditorDock::light_specular(QColor color)
{
	TCObject *o = Container::getSelectedObject();
	if ( o->getType() == TerrificCoder::Light ) {
		TCLight *light = (TCLight*) o;
		light->setSpecular(color.red(),color.green(),color.blue(),color.alpha());
	}
}
void LightEditorDock::light_spot(QColor color)
{
	TCObject *o = Container::getSelectedObject();
	if ( o->getType() == TerrificCoder::Light ) {
		TCLight *light = (TCLight*) o;
		light->setSpot(color.red(),color.green(),color.blue(),color.alpha());
	}
}
void LightEditorDock::light_spot_cutoff(double value)
{
	TCObject *o = Container::getSelectedObject();
	if (o && o->getType() == TerrificCoder::Light ) {
		TCLight *light = (TCLight*) o;
		light->setCutoff(value);
	}
}
void LightEditorDock::light_spot_direction_x(double value)
{
	TCObject *o = Container::getSelectedObject();
	if ( o && o->getType() == TerrificCoder::Light ) {
		TCLight *light = (TCLight*) o;
		Vector4f v(light->getSpotDirection());
		v.v[0] = value;
		light->setSpotDirection(Vector4f(v));
	}
}
void LightEditorDock::light_spot_direction_y(double value)
{
	TCObject *o = Container::getSelectedObject();
	if ( o && o->getType() == TerrificCoder::Light ) {
		TCLight *light = (TCLight*) o;
		Vector4f v(light->getSpotDirection());
		v.v[1] = value;
		light->setSpotDirection(Vector4f(v));
	}
}
void LightEditorDock::light_spot_direction_z(double value)
{
	TCObject *o = Container::getSelectedObject();
	if ( o && o->getType() == TerrificCoder::Light ) {
		TCLight *light = (TCLight*) o;
		Vector4f v(light->getSpotDirection());
		v.v[2] = value;
		light->setSpotDirection(Vector4f(v));
	}
}
void LightEditorDock::light_spot_exponent(double value)
{
	TCObject *o = Container::getSelectedObject();
	if (o && o->getType() == TerrificCoder::Light ) {
		TCLight *light = (TCLight*) o;
		light->setExponent(value);
	}
}

};
