#include "terrificmodellingwindow.h"
#include <KDE/KApplication>
#include <KDE/KAboutData>
#include <KDE/KCmdLineArgs>
#include <KDE/KLocale>
#include <GL/freeglut.h>
#include "container.h"

static const char description[] =
    I18N_NOOP("Terrific Modelling: A KDE 4 Application");

static const char version[] = "%{VERSION}";

int main(int argc, char **argv)
{
	glutInit(&argc,argv);
	
    KAboutData about("terrificmodelling", 0, ki18n("TerrificModelling"), version, ki18n(description),
                     KAboutData::License_GPL, ki18n("(C) 2010 %{AUTHOR}"), KLocalizedString(), 0, "%{EMAIL}");
    about.addAuthor( ki18n("%{AUTHOR}"), KLocalizedString(), "%{EMAIL}" );
    KCmdLineArgs::init(argc, argv, &about);

    KCmdLineOptions options;
    options.add("+[URL]", ki18n( "Document to open" ));
    KCmdLineArgs::addCmdLineOptions(options);
    KApplication app;

    TerrificCoder::TerrificModellingWindow *window = new TerrificCoder::TerrificModellingWindow();
	TerrificCoder::Container::setup();
    // see if we are starting with session management
    if (app.isSessionRestored())
    {
        RESTORE(TerrificCoder::TerrificModellingWindow);
    }
    else
    {
        // no session.. just start up normally
        KCmdLineArgs *args = KCmdLineArgs::parsedArgs();
        if (args->count() == 0)
        {           
            window->show();
        }
        else
        {
            int i = 0;
            for (; i < args->count(); i++)
            {
                window->show();
            }
        }
        args->clear();
    }

    int r = app.exec();
	glutExit();
	return r;
}
