/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRIFICMODELLINGWINDOW_H
#define TERRIFICMODELLINGWINDOW_H
#include <kxmlguiwindow.h>
#include <QtGui/QWidget>
#include "ui_terrificmodelling_window.h"

namespace TerrificCoder {

class TerrificModellingWindow : public KXmlGuiWindow, public Ui::terrificmodelling_window
{
	Q_OBJECT
public:
	TerrificModellingWindow(QWidget* = 0);
	virtual ~TerrificModellingWindow();
	
private:
	Ui::terrificmodelling_window ui_terrificmodelling_window;
};

};
#endif // TERRIFICMODELLINGWINDOW_H
