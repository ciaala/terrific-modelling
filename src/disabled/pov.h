/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include <QObject>
#include <GL/glu.h>
#include "glwidget.h"
#ifndef POV_H
#define POV_H

class Patch;
struct Geometry;
class TMGLWidget;
class POV: public QObject
{
public:
	POV(QObject *parent,TMGLWidget *widget);
	~POV();
	void draw();
	void process_click(GLuint *buffer);
	void click_notify_console( int value );
	
	void zoom(GLfloat degrees);
	

	void initialise_camera();
	void setup_viewport(GLint w, GLint h);
	void pad(GLfloat dx, GLfloat dy);
	
	void draw_scene();
	
	
protected:
	void draw_arrow(GLfloat x, GLfloat y, GLfloat z);
	void draw_axis();
	void draw_plane(int size, int tess, GLfloat x, GLfloat y, GLfloat z, GLfloat k);
	
	void update_camera_from_spherical();	
	void update_spherical_from_cartesian();
	
	
	
private:
	QList<Patch *> parts;
	Geometry *geom;

	
	TMGLWidget *glwidget;
	GLUquadric* obj;
	
	
	// Viewport
	GLint height;
	GLint width;
	
	// Perspective
	GLfloat fov;
	GLfloat aspect;
	GLfloat zfar;
	GLfloat znear;
	
	// Camera
	GLfloat eye[3];
	GLfloat center[3];
	GLfloat n[3];
	GLfloat m[3];
	GLfloat l[3];
	// Spherical Model
	GLfloat phi; 
	GLfloat theta;
	GLfloat rho;
	
//	void approx_angles(GLfloat dr,GLfloat ds, GLfloat dt);
	
	void valid_sphere_point(GLfloat dr, GLfloat ds, GLfloat dt);
	void debug(char *format, ... );
};

#endif // POV_H
