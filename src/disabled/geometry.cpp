/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "geometry.h"

void Geometry::loadArrays() const
{
    glVertexPointer(3, GL_FLOAT, 0, vertices.constData());
    glNormalPointer(GL_FLOAT, 0, normals.constData());
}

void Geometry::finalize()
{
    for( int i=0; i<normals.count(); ++i)
	normals[i].normalize();
}

void Geometry::appendSmooth(const QVector3D& a, const QVector3D& n, int from)
{
    // Smooth normals are achieved by averaging the normals for faces meeting
    // at a point.  First find the point in geometry already generated
    // (working backwards, since most often the points shared are between faces
    // recently added).
    int v = vertices.count() -1;
    for(; v>= from; --v)
	if ( qFuzzyCompare(vertices[v], a))
	    break;
    if (v <from){
      v = vertices.count();
      vertices.append(a);
      normals.append(n);
    }
    else {
	// The vert was not found so add it as a new one, and initialize
        // its corresponding normal
      normals[v] += n;
    }
    faces.append(v);
}

void Geometry::appendFaceted(const QVector3D& a, const QVector3D& n)
{    
    // Faceted normals are achieved by duplicating the vert for every
    // normal, so that faces meeting at a vert get a sharp edge.
    int v = vertices.count();
    vertices.append(a);
    normals.append(n);
    faces.append(v);
}