/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef TCGUI_H
#define TCGUI_H
#include "GL/gl.h"
#include <string>
#include <list>
#include "scene/tcobject.h"
#include "scene/tccamera.h"

namespace TerrificCoder{
class TCCamera;

class TCGui
{
	
public:
	enum MessageType { Verbose, Serious, Debug};
	enum CameraDetail { CameraCenter, CameraEye, CameraUp };
	
// 	virtual void message( MessageType type, const char* format, ... )=0;
// 	virtual void camera_detail( CameraDetail type, const char* format, ... )=0;
	
// 	virtual void verbose(char* format, ... )=0;
// 	virtual void debug(char* format, ... )=0;
// 	virtual void serious(char* format, ... )=0;
// 
// 	virtual void camera_eye(char* format, ... )=0;
// 	virtual void camera_center(char* format, ... )=0;
// 	virtual void camera_up(char* format, ... )=0;
	
	virtual GLint getViewportWidth()=0;
	virtual GLint getViewportHeight()=0;
	virtual GLfloat getViewportAspect()=0;
	virtual ~TCGui(){ };
	//virtual void setCameraList(std::list<TCCamera*>* cameras)=0;
	//virtual void setObjectList(std::list<TCObject*>* objects)=0;
// 	virtual void setObjectSelected(TCObject *object)=0;
// 	virtual void resetObjectList()=0;
// 	virtual void beginResetModel()=0;
// 	virtual void endResetModel()=0;
// 	virtual void insertObject(TCObject* object)=0;
};
};
#endif // TCGUI_H
