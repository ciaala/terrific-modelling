/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include <GL/glut.h>
#include "tccontainer.h"
#include "stdarg.h"


#ifndef GL_MULTISAMPLE
	#define GL_MULTISAMPLE  0x809D
#endif



namespace TerrificCoder{


	
TCContainer::TCContainer(TCGui *gui)
{
	this->gui = gui;
	this->overlay = new TCOverlay(this, gui);
	this->scene = NULL;
	this->selected_object = NULL;

	this->mouse_position[0]=((GLfloat)gui->getViewportWidth())/2;
	this->mouse_position[1]=((GLfloat)gui->getViewportHeight())/2;
}

TCContainer::~TCContainer()
{
	if (scene)
		delete scene;
	if (overlay)
		delete overlay;
}



void TCContainer::handleDraw()
{

}



void TCContainer::resizeGL()
{
	this->handleDraw();
}
/*
void TCContainer::mousePressEvent(GLint x, GLint y,MouseButton button)
{	

	if ( LeftButton == button ) {
//		handleSelection(x,y);
	} else if ( RightButton == button ) {
		GLint delta_x = x-mouse_position[0];
		GLint delta_y = y-mouse_position[1];
		if ( delta_x && delta_y ) {
//			scene->default_camera->viewportPadding(delta_x,-delta_y);
		}
		return;
	}
}*/

// void TCContainer::mouseReleaseEvent(int x, int y, MouseButton button)
// {
// // 	if ( scene != NULL && scene->default_camera != NULL) {
// // //		scene->default_camera->viewportPaddingStop();
// // 		return;
// // 	}
// }
// 
// 
// void TCContainer::mouseTrackEvent(GLint x, GLint y, MouseButton button)
// {
// 	mousePressEvent(x,y,button);
// }
// 
// void TCContainer::mouseWheelEvent(GLint delta, Orientation orientation)
// {
// 	if ( scene != NULL ){
// //		scene->default_camera->zoom(delta);
// 	}
// }

void TCContainer::timerEvent()
{
	if ( scene != NULL ) {
		//scene->updateTimer();
	}
}
void TCContainer::selectCamera(unsigned int id)
{
	if ( scene != NULL ) {
//		scene->selectCamera(id);
	}
}

// void TCContainer::create_object(TCObjectEnum type)
// {
// 	scene->create_object(type);
// }
TCObject* TCContainer::selectObject(unsigned int id)
{
	this->selected_object = this->scene->getObject(id);
//	this->selection_box = this->handleFeedback(id);
	return this->selected_object;
}

void TCContainer::delete_selected_object()
{
	if ( selected_object != NULL) {
		this->scene->object_del(selected_object);
		this->selection_box = TCRectangle();
	}
}
TCScene* TCContainer::getScene()
{
	return scene;
}

void TCContainer::setRenderCamera(TCObject* camera)
{
	if ( camera->getType() ==  QuaternionCamera ) {
//		scene->default_camera = static_cast<TCCamera*>(camera);
	}
}


};