/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "pov.h"
#include <GL/glu.h>
#include "glwidget.h"
#include "library.h"

POV::POV(QObject* parent, TMGLWidget* widget): QObject(parent)
{
	this->glwidget = widget;
	this->obj = gluNewQuadric();
	gluQuadricDrawStyle(obj, GLU_FILL);
	gluQuadricNormals(obj, GLU_SMOOTH);
	gluQuadricOrientation(obj,GLU_OUTSIDE);
	

	/**
	 * Camera initialization
	 */
	this->fov = 20.0f;
	this->zfar = -300.0f;
	this->znear = 300.0f;
	setup_vector(eye, 400.0, 0.0, 0.0);
	setup_vector(center, 0.0, 0.0, 0.0);
	setup_vector(n, 0.0, 1.0, 0.0 );
	update_spherical_from_cartesian();
	update_camera_from_spherical();
/*	this->phi = 0;
	this->theta = M_PI/2;
	this->rho = 400;
	*/

}

void POV::draw_plane(int size, int tess, GLfloat x, GLfloat y, GLfloat z, GLfloat k) {
	GLfloat vertex[tess+1][tess+1][3];
	GLfloat x0=-size, y0=-size;
	GLfloat inc = size;
	inc *= 2;
	inc /= tess;
	for(int i=0; i <= tess; i++ ){
		for (int j=0; j<= tess; j++){
			vertex[i][j][0] = i*inc + x0;
			vertex[i][j][1] = j*inc + y0;
			vertex[i][j][2] = (x*i + y*j -k)/z;
		}
	}
	glPushMatrix();
	glColor3f(0.7,0.7,0.7);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glBegin(GL_QUAD_STRIP);
	for(int i=0; i < tess; i++ ){
		for (int j=0; j<= tess; j++){
			glVertex3fv(vertex[i][j]);
			glVertex3fv(vertex[i+1][j]);
		}
	}
	glEnd();
	glPolygonMode(GL_FRONT, GL_FILL);
	glPopMatrix();
}

void POV::draw_axis(){
	
	
	
	GLfloat zero[3] = {0.0,0.0,0.0};
	GLfloat x[3] = {100.0,0.0,0.0};
	GLfloat y[3] = {0.0,100.0,0.0};
	GLfloat z[3] = {0.0,0.0,100.0};
	glPushMatrix();
	glLineWidth(2.0);
	glBegin(GL_LINES);
		glColor4f(1.0f,0.0f,0.0f,1.0);
		glVertex3fv(zero);
		glVertex3fv(x);
	glEnd();
	glBegin(GL_LINES);
		glColor4f(0.0f,1.0f,0.0f,1.0);
		glVertex3fv(zero);
		glVertex3fv(y);
	glEnd();
	glBegin(GL_LINES);
		glColor4f(0.0f,0.0f,1.0f,1.0);
		glVertex3fv(zero);
		glVertex3fv(z);
	glEnd();
	
	
	glPopMatrix();
}


void POV::draw_arrow(GLfloat x, GLfloat y, GLfloat z) {
	glPushMatrix();
		glRotatef(90.0,x,y,z);
		gluCylinder(obj, 1.0,1.0,10.0,20,2);
	glPopMatrix();
	glPushMatrix();
		glRotatef(90.0,x,y,z);		
		glTranslatef(0.0,0.0,10.0);
		gluCylinder(obj, 2.0,0.0,5.0,20,2);
	glPopMatrix();
}


void POV::draw()

{	
	
		draw_plane(100.0, 10.0, 1.0,1.0,1.0,0.0);
		draw_axis();

		//glTranslatef(0.0,0.0,0.0);
		glLoadName(1000);
		glColor4f(1.0f,0.0f,0.0f,1.0);
		this->draw_arrow(1.0,0.0,0.0);
		
	//	glPopName();

		
		glLoadName(1001);
		glColor4f(0.0f,1.0f,0.0f,1.0);
		this->draw_arrow(0.0,1.0,0.0);
	//	glPopName();
		
		glLoadName(1002);
		glColor4f(0.0f,0.0f,1.0f,1.0);
		this->draw_arrow(0.0,0.0,1.0);
	//	glPopName();
	//
	draw_scene();
}
POV::~POV()
{

}
void POV::draw_scene(){
// 	glPushMatrix();
// 		glColor3f(1.0,0.0,0.0);
// 		glTranslatef(20.0,0.0,0.0);
// 		gluSphere(obj,5.0,10.0,10.0);
// 	glPopMatrix();
// 	glPushMatrix();
// 		glColor3f(0.0,1.0,0.0);
// 		glTranslatef(0.0,20.0,0.0);
// 		gluSphere(obj,5.0,10.0,10.0);
// 	glPopMatrix();
}


void POV::click_notify_console( int value ){
	if (value == 1000) {
		this->glwidget->console( "Selected: 1000");
	} else if (value == 1001) {
		this->glwidget->console( "Selected: 1001");
	} else if (value == 1002) {
		this->glwidget->console( "Selected: 1002");
	} else if (value == 0 ){
		this->glwidget->console( "Empty Space");
	}
}

void POV::process_click(GLuint *buffer)
{
	int count =  buffer[0];
	int value = buffer[3];
	click_notify_console(value);
	
	int i=1;
	while ( count > 1 ) {
		click_notify_console(buffer[3]+i);
		i++;
		count--;
	}

}

void POV::setup_viewport(GLint w, GLint h)
{
	this->width = w;
	this->height = h;
	this->aspect = ((GLfloat)w /( (GLfloat) h));
}

void POV::initialise_camera() {
	glViewport(0,0,width,height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective( fov, aspect, zfar, znear );
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(eye[0], eye[1], eye[2],
			  center[0], center[1],center[2],
			  n[0], n[1], n[2]);
	
	
	/*	gluLookAt(this->camera_position[0],this->camera_position[1],this->camera_position[2],
				this->camera_direction[0],this->camera_direction[1],this->camera_direction[2],
				this->camera_up[0], this->camera_up[1], this->camera_up[2]);*/


}


void POV::pad(GLfloat dm, GLfloat dn){
	
	GLfloat r[3] = {1.0,0.0,0.0};
	GLfloat s[3] = {0.0,1.0,0.0};
	GLfloat t[3] = {0.0,0.0,1.0};
	
	GLfloat dr = dm*scalar_product(m,r) + dn*scalar_product(n,r);
	GLfloat ds = dm*scalar_product(m,s) + dn*scalar_product(n,s);
	GLfloat dt = dm*scalar_product(m,t) + dn*scalar_product(n,t);
	
	glwidget->console(QString().sprintf("dm,dn - dr,ds,dt = {%f, %f} {%f,%f,%f}",dm, dn, dr, ds, dt));
	
	valid_sphere_point(dr,ds,dt);
// 	
/*	GLfloat dr = dm;
	GLfloat ds = dn;*/
	/*
	GLfloat alpha = atan(dr/rho);
	if ( alpha > 0 ){
		alpha = alpha > (M_PI/180)? alpha : M_PI/180;
	}
	else if (alpha <0){
		alpha = alpha < (-M_PI/180)? alpha : -M_PI/180;
	}
	GLfloat beta = atan(ds/rho);
	
	phi += alpha;
	theta += beta;
	phi = fmod(phi,M_PI);
	theta = fmod(theta,2 *M_PI);
	*/
//	theta += atan(ds/dr);
//	phi += acos(dt/rho);
	
	
/*	GLfloat alpha = atan(dn/ rho);
	GLfloat temp[3];
	rotation(temp,n,alpha);
	memcpy(n,temp,sizeof(GLfloat)*3);*/
	
	glwidget->console(QString().sprintf("\tphi,theta,rho = {%f, %f, %f}",phi*180/M_PI, theta*180/M_PI, rho));

	update_camera_from_spherical();
}


/**
 * 	eye,center - > rho, theta, phi.
 */
void POV::update_spherical_from_cartesian(){
	rho = point_distance(eye,center);
	phi = acos( (center[2]-eye[2])/rho);
	theta = atan2((center[1]-eye[1]),(center[0]-eye[0]));
	
	glwidget->console(QString().sprintf("\tphi,theta,rho = {%f, %f, %f}",phi*180/M_PI, theta*180/M_PI, rho));
	
}
/**
 * rho, theta, phi, center, n -> eye, l, m
 */ 
void POV::update_camera_from_spherical(){
	
	l[0] = -rho * sin(phi) * cos(theta);
	l[1] = -rho * sin(phi) * sin(theta);
	l[2] = -rho * cos(phi);
	
	eye[0] = center[0] - l[0];
	eye[1] = center[1] - l[1];
	eye[2] = center[2] - l[2];
	
	normalize(l);
	normalize(n);
	cross_product(m,l,n);
	normalize(m);
	
	
	glwidget->console(QString().sprintf("\tl = {%f, %f, %f}",l[0],l[1],l[2]));
	glwidget->console(QString().sprintf("\tm = {%f, %f, %f}",m[0],m[1],m[2]));
	glwidget->console(QString().sprintf("\tn = {%f, %f, %f}",n[0],n[1],n[2]));
	glwidget->console(QString().sprintf("eye = { %f, %f, %f }",eye[0],eye[1],eye[2]));

}	
/*
void POV::pad(GLfloat dx, GLfloat dy)
{
	glwidget->console(QString().sprintf("dx dy = { %g, %g }",dx, dy));

	GLfloat rx,ry,rz;
	rx = camera_position[0] - camera_direction[0];
	ry = camera_position[1] - camera_direction[1];
	rz = camera_position[2] - camera_direction[2];
	
	GLint old_eye_z = camera_position[2] > 0 ? 1 : -1;
	
	
	GLfloat d = sqrt(rx*rx+ry*ry+rz*rz);
	
	GLfloat alpha = atan(dx/d);
	GLfloat beta = atan(dy/d);
	
	this->camera_position[0] += d*sin(alpha);
	this->camera_position[1] += d*sin(beta);
	this->camera_position[2] += d*(cos(alpha) + cos(beta) - 2);
	
	GLint new_eye_z = camera_position[2] > 0 ? 1: -1;
	if ( old_eye_z != new_eye_z) {
		GLfloat temp = zfar;
		zfar = znear;
		znear = temp;
	}
}
*/

void POV::zoom(GLfloat degrees){
	//this->camera_position[2] += degrees;
	this->rho += degrees/2;
	

	update_camera_from_spherical();
	
}
/*
void POV::approx_angles(GLfloat dr,GLfloat ds, GLfloat dt){
	theta += atan(ds/dr);
	phi += acos(dt/rho);
}
*/



void POV::valid_sphere_point(GLfloat dr, GLfloat ds, GLfloat dt){
	eye[0] += dr;
	eye[1] += ds;
	eye[2] += dt;
	GLfloat a[3];
	
	vector_subctrat(a,center,eye);
	
	GLfloat wrong_rho = point_distance(eye,center);
	theta = acos( a[2]/wrong_rho);
	phi = atan2f(a[1],a[0]);
	
	update_camera_from_spherical();
	
}

void POV::debug(char *format, ... ){
	va_list args;
	va_start (args, format);
	glwidget->console(QString().vsprintf(format,args));
	va_end(args);
}
