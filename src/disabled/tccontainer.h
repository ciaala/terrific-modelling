/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef TCCONTAINER_H
#define TCCONTAINER_H
#include <list>
#include "terrificcoder.h"
#include "tcgui.h"
#include "scene/tcscene.h"
#include "scene/tcoverlay.h"
#include "scene/tccamera.h"
#include "scene/tclight.h"
#include "library/tclibrary.h"


namespace TerrificCoder{ 

class TCScene;
class TCOverlay;
class TCCamera;

class TCContainer
{
public:

	TCContainer(TCGui *gui);
	~TCContainer();
	void initializeGL();
	void handleDraw();
	void resizeGL();
private:
	void handleSelection(GLdouble x, GLdouble y);
	TCRectangle handleFeedback(unsigned int selection_id);
	
public:
// 	void mousePressEvent(GLint x, GLint y, MouseButton button);
// 	void mouseTrackEvent(GLint x, GLint y, MouseButton button);
// 	void mouseWheelEvent(GLint delta, Orientation orientation);
	void timerEvent();
// 	void mouseReleaseEvent(int x, int y, MouseButton button);
	void selectCamera(unsigned int id);
	TCObject* selectObject(unsigned int id);
	void create_object(TCObjectEnum type);

	
	TCRectangle feedbackPolygon(TCRectangle r, int i, GLfloat* feedbackBuffer);
	
	void delete_selected_object();
	TCScene* getScene();
	TCScene* create_scene();
	void setRenderCamera(TCObject* arg1);

	TCRectangle selection_box;

public:
	TCLight* selected_light;
	TCObject* selected_object;
	
	
	
private:
	// Mouse
	GLint mouse_position[2]; 
	
	TCScene *scene;
	TCOverlay *overlay;
	TCGui* gui;
};

};
#endif // TCCONTAINER_H
