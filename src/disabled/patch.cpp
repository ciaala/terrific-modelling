/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "patch.h"
#include "library.h"
/**
 * Transform a QColor to a float vector
 */

Patch::Patch(Geometry* g) 
  : start(g->faces.count()),
  count(0),
  initv(g->vertices.count()),
  sm(Patch::Smooth),
  geom(g)
{
	qSetColor(faceColor,QColor(Qt::darkGray));
}

void Patch::rotate(qreal deg, QVector3D axis)
{
	mat.rotate(deg,axis);
}

void Patch::translate(const QVector3D& t)
{
	mat.translate(t);
}

static inline void qMultMatrix(const QMatrix4x4 &mat)
{
	if(sizeof(qreal) ==sizeof(GLfloat))
		glMultMatrixf((GLfloat*)mat.constData());
#ifndef QT_OPENGL_ES
	else if (sizeof(qreal) == sizeof(GLdouble))
		glMultMatrixd((GLdouble*)mat.constData());
#endif
	else {
		GLfloat fmat[16];
		qreal const *r = mat.constData();
		for (int i=0; i < 16; ++i)
			fmat[i] = r[i];
		glMultMatrixf(fmat);
	}
}
void Patch::draw() const
{
	glPushMatrix();
	qMultMatrix(mat);
	glDisable(GL_COLOR_MATERIAL);
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, faceColor);
	const GLushort *indices = geom->faces.constData();
	glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_SHORT, indices+start);
	glEnable(GL_COLOR_MATERIAL);
	glPopMatrix();
}

void Patch::addTri(const QVector3D& a, const QVector3D& b, const QVector3D& c, const QVector3D& n)
{
	QVector3D norm = n.isNull() ? QVector3D::normal(a,b,c) : n;
	if (sm == Smooth){
		geom->appendSmooth(a, norm, initv);
		geom->appendSmooth(b, norm, initv);
		geom->appendSmooth(c, norm, initv);
	} else {
		geom->appendFaceted(a, norm);
		geom->appendFaceted(b, norm);
		geom->appendFaceted(c, norm);
	}
	count += 3;
}
void Patch::addQuad(const QVector3D& a, const QVector3D& b, const QVector3D& c, const QVector3D& d)
{
	QVector3D norm = QVector3D::normal(a,b,c);
	if ( sm == Smooth){
		addTri(a,b,c,norm);
		addTri(a,c,d,norm);
	} else {
		addTri(a,b,c,norm);
		int k = geom->vertices.count();
		geom->appendSmooth(a, norm, k);
		geom->appendSmooth(c, norm, k);
		geom->appendFaceted(d, norm);
		count += 3;
	}
}
