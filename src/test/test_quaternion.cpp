#include "../terrificcoder/library/quaternion.h"
#include "math.h"
#include <iostream>

using namespace std;
using namespace TerrificCoder;

int main(int , char** ){

	Vector x(1,0,0);
	Vector y(0,1,0);
	Vector z(0,0,1);

	Quaternion rot_z(M_PI/6.0f,Vector(0,0,1));
	Quaternion rot_x(M_PI/6.0f,Vector(1,0,0));
	cout << "x: " << x << " y: " << y << " z: " << z << std::endl << "rot_z :" << rot_z << std::endl ;
	for(int i=0; i < 12; i++){
		x = rot_z.rotateVector(x);
		y = rot_z.rotateVector(y);
		z = rot_z.rotateVector(z);
		cout << "--> x: " << x << " y: " << y << " z: " << z << std::endl;
	}
	x = Vector(1,0,0);
	y = Vector(0,1,0);
	z = Vector(0,0,1);
	cout << "x: " << x << " y: " << y << " z: " << z << std::endl << "rot_x :" << rot_x << std::endl;
	for(int i=0; i < 12; i++){
		x = rot_x.rotateVector(x);
		y = rot_x.rotateVector(y);
		z = rot_x.rotateVector(z);
		cout << "--> x: " << x << " y: " << y << " z: " << z << std::endl;
	}
	return 0;
}