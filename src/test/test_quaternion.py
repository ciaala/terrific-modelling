#/usr/bin/python2.6
from cgkit.cgtypes import *
from math import pi
x = vec3(1,0,0)
y = vec3(0,1,0)
z = vec3(0,0,1)
rot_z = quat().fromAngleAxis(pi/6, vec3(0,0,1));
print "x: ", x, "y: ", y, "z: ", z
print "rot_z:", rot_z
for i in range(12):
    x = rot_z.rotateVec(x)
    y = rot_z.rotateVec(x)
    z = rot_z.rotateVec(x)
    print "--> x: ", x, "y: ", y, "z: ", z

x = vec3(1,0,0)
y = vec3(0,1,0)
z = vec3(0,0,1)
rot_x = quat().fromAngleAxis(pi/6, vec3(1,0,0));    
print "x: ", x, "y: ", y, "z: ", z
for i in range(12):
    x = rot_z.rotateVec(x)
    y = rot_z.rotateVec(x)
    z = rot_z.rotateVec(x)
    print "--> x: ", x, "y: ", y, "z: ", z