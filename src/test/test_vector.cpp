#include <vector>


int main() {
	std::vector<int> v;
	if (v.size() != 0) {
		return -1;
	}
	v.push_back(5);
	if (v.size() != 1){
		return -1;
	}
	v.push_back(3);
	if (v.size() != 2){
		return -1;
	}
	return 0;
}